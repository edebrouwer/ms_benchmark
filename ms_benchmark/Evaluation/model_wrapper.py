import yaml
import wandb
import torch

import pandas as pd

import pytorch_lightning as pl
from pytorch_lightning import Trainer


import tensorflow as tf

GPU_NUM = 0


import numpy as np

from ms_benchmark.models.bayesian_nn.model import make_model

from ms_benchmark import predict_utils, calibration_utils

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

class MSModel:
    def load_model_and_data(self, fold):
        """
        Load in the model and data for the given fold. No specific requirements, other than they should probably be saved on the object
        
        fold: fold number (int)
        return: None
        """
        raise NotImplementedError()
        
    def predict_on_test(self):
        """
        Make predictions on the test set and return both labels and unnormalized (so logits) predictions
        
        return: y_test_true; shape: (N,), y_test_pred; shape: (N,)
        """
        raise NotImplementedError()

    def predict_on_val(self):
        """
        Make predictions on the validation set and return both labels and unnormalized (so logits) predictions
        
        return: y_val_true; shape: (N,), y_val_pred; shape: (N,)
        """
        raise NotImplementedError()

    def get_df(self):
        """
        Return the unnormalized test dataframe for current fold
        
        return: Processed test dataframe containing EDSS information etc.
        """
        raise NotImplementedError()


class BaselineMSModel(MSModel):
    def __init__(self, api_key, model_cls, data_cls):
        api = wandb.Api()
        self.api = api
        self.model_cls = model_cls
        self.data_cls = data_cls
        if isinstance(api_key,list):
            sweep = [api.sweep(f"ms_prognosis_2021/msbase_21/{api_k}") for api_k in api_key]
            sweep_runs = []
            for s in sweep:
                sweep_runs += s.runs
        else:
            sweep = api.sweep(f"ms_prognosis_2021/msbase_21/{api_key}")
            sweep_runs = sweep.runs
        
        self.best_runs = []
        for fold in range(5):
            runs_fold = [r for r in sweep_runs if r.config.get("fold")==fold]
            runs_fold_sorted = sorted(runs_fold,key = lambda run: run.summary.get("best_val_auc"), reverse = True)
            self.best_runs.append(runs_fold_sorted[0])

    def load_model_and_data(self, fold):
        run = self.best_runs[fold]
        run_name = run.id
        self.model, self.dataset = predict_utils.load_run(run_name,self.model_cls,self.data_cls)
        self.trainer = Trainer(logger = False, gpus=1)

    def predict(self, X):
        out = self.trainer.predict(self.model, X)
        y_true = torch.cat([o["y"] for o in out]).cpu()
        #y_pred = torch.cat([o["y_pred"] for o in out]).cpu()[:,1]
        y_pred = torch.softmax(torch.cat([o["y_pred"] for o in out]).cpu(),-1)[:,1]

        return y_true, y_pred
        
    def predict_on_test(self, shuffle_pos = -1):
        if shuffle_pos>=0:
            return self.predict(self.dataset.test_dataloader_shuffle(shuffle_pos))
        return self.predict(self.dataset.test_dataloader())

    def predict_on_val(self, shuffle_pos = -1):
        if shuffle_pos >=0:
            return self.predict(self.dataset.val_dataloader_shuffle(shuffle_pos))
        return self.predict(self.dataset.val_dataloader())  
    
    def get_df(self):
        #import ipdb; ipdb.set_trace()
        return self.dataset.get_df()[-1]

class DynamicMSModel(BaselineMSModel):
    def __init__(self, api_key):
        from ms_benchmark.models.baseline_dynamic import models as dynamic_models
        from ms_benchmark.models.baseline_dynamic import data_utils as dynamic_data_utils
        super().__init__(api_key, dynamic_models.PointWiseModel, dynamic_data_utils.PointWiseDataset)

class StaticMSModel(BaselineMSModel):
    def __init__(self, api_key):
        from ms_benchmark.models.baseline_static import models as static_models
        from ms_benchmark.models.baseline_static import data_utils as static_data_utils
        super().__init__(api_key, static_models.PointWiseModel, static_data_utils.PointWiseDataset)
        
class RNNMSModel(BaselineMSModel):
    def __init__(self, api_key):
        from ms_benchmark.models.RNN import models as RNN_models
        from ms_benchmark.models.RNN import data_utils as RNN_data_utils
        super().__init__(api_key, RNN_models.RNNModel, RNN_data_utils.LongitudinalDataset)

class AttentionMSModel(BaselineMSModel):
    def __init__(self, api_key): 
        from ms_benchmark.models.attention import models_new as attention_models
        from ms_benchmark.models.attention import data_utils as attention_data_utils
        super().__init__(api_key, attention_models.AttentionModel, attention_data_utils.LongitudinalDataset)
    
class BNNMSModel(MSModel):
    def __init__(self, dataset, num_visits, gpu_num=1):
        self.dataset = dataset
        # Important for SUMO-* servers
        gpus = tf.config.experimental.list_physical_devices('GPU')
        print(gpus)
        if gpus:
            try:
                g = gpus[GPU_NUM]
                tf.config.experimental.set_visible_devices(g, 'GPU')
                tf.config.experimental.set_memory_growth(g, True)
                logical_gpus = tf.config.experimental.list_logical_devices('GPU')
                print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
            except RuntimeError as e:
                # Visible devices must be set before GPUs have been initialized
                print(e)
        # Load all parameters as tracked by dvc
        with open("../models/bayesian_nn/params.yaml", 'r') as fd:
            PARAMS = yaml.safe_load(fd)

        data_train = tf.data.experimental.load(f'../models/bayesian_nn/artifacts/{num_visits}_visits/{self.dataset}/train_dataset_0')

        n_features = None
        for X, y in data_train.as_numpy_iterator():
            n_features = X.shape[-1]
    
        self.model = make_model(n_features, **PARAMS['training']['model'])
        self.num_visits = num_visits

    def load_model_and_data(self, fold):
        self.fold = fold
        self.model.load_weights(f'../models/bayesian_nn/artifacts/{self.num_visits}_visits/{self.dataset}/saved_model_{fold}/model')
    
    def predict(self, X, logits=True):
        y_pred = np.mean(np.stack([self.model(X)['outp_mu_sigma'][:,:1].numpy() for _ in range(20)], axis=0), axis=0)
        
        if not logits:
            y_pred = sigmoid(y_pred)
        
        return y_pred[:,0]
    
    def predict_on_test(self):
        data_test = tf.data.experimental.load(f'../models/bayesian_nn/artifacts/{self.num_visits}_visits/{self.dataset}/test_dataset_{self.fold}')
        self.data_test = data_test
        X_test, y_test = next(data_test.batch(data_test.cardinality()).as_numpy_iterator())
        return y_test, self.predict(X_test)

    def predict_on_val(self):
        data_val = tf.data.experimental.load(f'../models/bayesian_nn/artifacts/{self.num_visits}_visits/{self.dataset}/val_dataset_{self.fold}')
        self.data_val = data_val
        X_val, y_val = next(data_val.batch(data_val.cardinality()).as_numpy_iterator())
        return y_val, self.predict(X_val)

    def get_df(self):
        return pd.read_pickle(f'../models/bayesian_nn/artifacts/{self.num_visits}_visits/{self.dataset}/test_df_{self.fold}')

    
class MultitaskMSModel(MSModel):
    def __init__(self, api_key, model_cls, data_cls):
        api = wandb.Api()
        self.api = api
        self.model_cls = model_cls
        self.data_cls = data_cls
        sweep = api.sweep(f"ms_prognosis_2021/msbase_21/{api_key}")

        self.best_runs = []
        for fold in range(5):
            runs_fold = [r for r in sweep.runs if r.config.get("fold")==fold]
            runs_fold_sorted = sorted(runs_fold,key = lambda run: run.summary.get("best_val_auc"), reverse = True)
            self.best_runs.append(runs_fold_sorted[0])

    def load_model_and_data(self, fold):
        run = self.best_runs[fold]
        run_name = run.id
        self.model, self.dataset = predict_utils.load_run(run_name,self.model_cls,self.data_cls)
        self.trainer = Trainer(logger = False, gpus=1)

    def predict(self, X):
        out = self.trainer.predict(self.model, X)
        y_true = torch.cat([o["y"] for o in out]).cpu().numpy().flatten()
        #y_pred = torch.cat([o["y_pred"] for o in out]).cpu()[:,1]
        #y_pred = torch.softmax(torch.cat([o["y_pred"] for o in out]).cpu(),-1)[:,1]
        y_pred = torch.cat([o["y_pred"] for o in out]).cpu().numpy().flatten()

        return y_true, y_pred
        
    def predict_on_test(self, shuffle_pos = -1):
        if shuffle_pos>=0:
            return self.predict(self.dataset.test_dataloader_shuffle(shuffle_pos))
        return self.predict(self.dataset.test_dataloader())

    def predict_on_val(self, shuffle_pos = -1):
        if shuffle_pos >=0:
            return self.predict(self.dataset.val_dataloader_shuffle(shuffle_pos))
        return self.predict(self.dataset.val_dataloader())  
    
    def get_df(self):
        return self.dataset.get_df()[-1]

class DeepMTPModel(MultitaskMSModel):
    def __init__(self, api_key):
        from ms_benchmark.models.AutoMTP_light import model as deepMTP_models
        from ms_benchmark.models.AutoMTP_light import data_utils as deepMTP_data_utils
        super().__init__(api_key, deepMTP_models.DeepMTPModel, deepMTP_data_utils.MultiTargetDataset)
        
import pickle
import scipy
import sys
sys.path.append(f'../models/FMachines/')
from ms_benchmark.models.FMachines.factorizationmachine_utils import FM
from ms_benchmark.models.FMachines.factorizationmachine_utils import CustomDataset

class FactorizationMachine(MSModel):
    def __init__(self, api_key, dataset_version, num_visits):
        self.num_visits = num_visits
        api = wandb.Api()
        self.api = api
        #sweep = api.sweep(f"padwulf/FM/"+api_key)
        sweep = api.sweep(f"ms_prognosis_2021/msbase_21/{api_key}")


        self.best_runs = []
        for fold in range(5):
            runs_fold = [r for r in sweep.runs if (r.config.get("fold")==fold) and (r.config.get("dataset_version")==dataset_version) and (r.config.get("num_visits")==num_visits)]
            runs_fold_sorted = sorted(runs_fold,key = lambda run: run.summary.get("best_val_auc"), reverse = True)
            self.best_runs.append(runs_fold_sorted[0])
    
    def load_model_and_data(self, fold):
        run = self.best_runs[fold]
        run_name = run.id
        
        with open(f'../models/FMachines/SavedModels/'+run.id, 'rb') as f:
            self.model = pickle.load(f) 
        
        ds = CustomDataset(datatype=run.config.get('dataset'), fold=run.config.get('fold'), no_dmt=run.config.get('no_dmt'), data_version=run.config.get('dataset_version'))#, **kwargs)
        df_train, df_val, df_test = ds.prepare_data(num_visits=run.config.get('num_visits'), no_dmt=run.config.get('no_dmt'))
        self.dataset = df_train, df_val, df_test
        
        _, _, df_test_basic = ds.prepare_data(num_visits=run.config.get('num_visits'), no_dmt=run.config.get('no_dmt'), return_only_basic_info=True)
        self.df_test_basic = df_test_basic

        
    def predict_on_test(self):
        df_train, df_val, df_test = self.dataset        
        y_test_true = np.array(df_test.pop('label'))
        Xtest = scipy.sparse.csr_matrix(np.array(df_test,float))
        y_test_pred = self.model.predict(Xtest)
        return y_test_true, y_test_pred


    def predict_on_val(self):
        df_train, df_val, df_test = self.dataset        
        y_val_true = np.array(df_val.pop('label'))
        Xval = scipy.sparse.csr_matrix(np.array(df_val, float))
        y_val_pred = self.model.predict(Xval)
        return y_val_true, y_val_pred

    def get_df(self):
        return self.df_test_basic
