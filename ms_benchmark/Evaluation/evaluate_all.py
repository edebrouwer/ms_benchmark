from ms_benchmark import predict_utils, calibration_utils

from model_wrapper import BNNMSModel, DynamicMSModel, StaticMSModel, RNNMSModel, AttentionMSModel, DeepMTPModel, FactorizationMachine

from sklearn.metrics import roc_auc_score, auc, precision_recall_curve, brier_score_loss
from sklearn.calibration import calibration_curve
import copy

import torch
import pandas
import os

dynamic_baseline_sweeps = {3:"8eaupe5h", 6: "uacwrb8t"}
static_baseline_sweeps = {3:"wrzuwh58", 6: "4j8wvj9h"}
rnn_sweeps = {3:"pfij910u", 6: "r4w6gvb5"}
attention_sweeps = {3:"a9zakq0n", 6: ["a0lvcrwz","jdfg6963"]}
static_mtp_sweeps = {3:"kby8gec0", 6: "qyi0bv6p"}
dynamic_mtp_sweeps = {3:"9tu4eend", 6: "2iqt8pj4"}
fm_machine_sweeps = {3:"6e3i37i2", 6:"zn282ioz"}
static_logistic_sweeps = {3:["7ir17i2n","q8ey0ujm"], 6:"0lsosgmb"}
dynamic_logistic_sweeps = {3:"kooho3ih",6:"sbqu8i8w"}

NUM_VISITS = 6
config = {
    #"Dynamic Logistic" : DynamicMSModel(dynamic_logistic_sweeps[NUM_VISITS]),
    #"Static Logistic" : StaticMSModel(static_logistic_sweeps[NUM_VISITS]),
    #"Dynamic Baseline" : DynamicMSModel(dynamic_baseline_sweeps[NUM_VISITS]),
    #"Static Baseline" : StaticMSModel(static_baseline_sweeps[NUM_VISITS]),
    #"Dynamic Bayesian NN": BNNMSModel("dynamic", num_visits=NUM_VISITS),
    #"Static Bayesian NN": BNNMSModel("static", num_visits = NUM_VISITS),
    #"RNN": RNNMSModel(rnn_sweeps[NUM_VISITS]),
    "Attention": AttentionMSModel(attention_sweeps[NUM_VISITS]),
    #"Static DeepMTP": DeepMTPModel(static_mtp_sweeps[NUM_VISITS]),
    #"Dynamic DeepMTP": DeepMTPModel(dynamic_mtp_sweeps[NUM_VISITS]),
    #"Static FactorizationMachine": FactorizationMachine(api_key=fm_machine_sweeps[NUM_VISITS], dataset_version='static', num_visits=NUM_VISITS),
    #"Dynamic FactorizationMachine": FactorizationMachine(api_key=fm_machine_sweeps[NUM_VISITS], dataset_version='dynamic', num_visits=NUM_VISITS)
}


results = {}
for key, ms_model in config.items():
    print(f"Evaluating model {key} ...")
    metrics = {"ROC-AUC":[],"AUC-PR":[],"Brier":[],"ECE":[]}
    mscourse_dict = {'MSCOURSE_AT_VISIT_PP':copy.deepcopy(metrics), 'MSCOURSE_AT_VISIT_RR':copy.deepcopy(metrics), 'MSCOURSE_AT_VISIT_SP':copy.deepcopy(metrics)}
    
    for fold in range(5):
        ms_model.load_model_and_data(fold)
        df_test = ms_model.get_df()

        y_true_test, y_pred_test = ms_model.predict_on_test()
        y_true_val, y_pred_val = ms_model.predict_on_val()
        calibration_dict = calibration_utils.calibration(y_pred_val, y_true_val, y_pred_test, y_true_test)
        
        y_true_c,_,y_calibrated = calibration_dict["platt"]

        one_word_name = "_".join(key.split(" "))
        os.makedirs(f"./preds_vectors/{one_word_name}/{NUM_VISITS}/fold_{fold}", exist_ok = True )
        torch.save(y_true_c,f"./preds_vectors/{one_word_name}/{NUM_VISITS}/fold_{fold}/y_true.pt")
        torch.save(y_calibrated,f"./preds_vectors/{one_word_name}/{NUM_VISITS}/fold_{fold}/y_calibrated.pt")
        df_test.to_csv(f"./preds_vectors/{one_word_name}/{NUM_VISITS}/fold_{fold}/df_test.csv",index = False)
