from ms_benchmark import DATA_DIR
import torch
import pytorch_lightning as pl
import pandas as pd
import numpy as np
from ms_benchmark.utils import str2bool
import pickle
from torch.utils.data import DataLoader
import pickle

DATATYPE_DICT = ("EP","MSBase2020")

def isolate_country(x):
	return str(x['PATIENT_ID']).split('-')[0]

def isolate_clinic(x):
	return '-'.join(str(x['PATIENT_ID']).split('-')[0:2])

def normalize_df(df_train, df_val, df_test, cols):
    df_train = df_train.copy()
    df_val = df_val.copy()
    df_test = df_test.copy()

    means = df_train[cols].mean()
    stds  = df_train[cols].std()
    
    for df_ in (df_train, df_val, df_test):
        df_[cols] = (df_[cols]-means)/stds
    
    return df_train, df_val, df_test

def normalize_longitudinal_df(df_train, df_val, df_test, cols, masked = True):
    """
    If masked = true, we expect a column with _mask suffix to be present in the data.
    """
    df_train = df_train.copy()
    df_val = df_val.copy()
    df_test = df_test.copy()

    means = {}
    stds = {}

    if masked:
        for col in cols:
            means[col] = df_train.loc[df_train[f"{col}_mask"]==1,col].mean()
            # import ipdb; ipdb.set_trace()
            stds[col]  = df_train.loc[df_train[f"{col}_mask"]==1,col].std()
        
            for df_ in (df_train, df_val, df_test):
                df_.loc[df_[f"{col}_mask"]==1,col] = (df_.loc[df_[f"{col}_mask"]==1,col]-means[col]) / stds[col]
    else:
        for col in cols:
            means[col] = df_train[col].mean()
            # import ipdb; ipdb.set_trace()
            stds[col]  = df_train[col].std()
        
            for df_ in (df_train, df_val, df_test):
                df_[col] =(df_[col]-means[col]) / stds[col]
   
    return df_train, df_val, df_test


def transform_time_since(x):
    """
    transform the time_since variables such that
    it is continuous and does not contain NaNs
    assumes input time is in days
    """
    if np.isnan(x):
        return 0
    elif x <= 0:
        return 1
    else:
        return 1. / (1. + (x / 365.))


def transform_time_since_relapse(x):
    """
    transform the time_since variables such that
    it is continuous and does not contain NaNs
    assumes input time is in days
    """
    if x == 9999 or x == -9999:
        return 0
    elif x <= 0:
        return 1
    else:
        return 1. / (1. + (x / 365.))


def get_msbase_static_dataset(no_dmt = False, include_country = False, include_clinic = False, minimal_example = False, num_visits = 3):

    directory = DATA_DIR+f"/MSBase2020/Cleaned/{num_visits}_visits/Static/"
    df = pd.read_pickle(directory + "df.pkl")
    with open(directory+"cov_cols.pkl", "rb") as fp:   # Unpickling
        cov_cols = pickle.load(fp)
    with open(directory+"continuous_cols.pkl", "rb") as fp:   # Unpickling
        continuous_cols = pickle.load(fp)
   
    if not no_dmt :
        dmt_groups = ["Mild","Moderate","High"]
        dmt_cols = ["DMT_AT_0_" + dmt for dmt in dmt_groups]

        dmt_groups_ind = ["High_Induction"]
        dmt_cols_ind = ["DMT_IND_AT_0_" + dmt for dmt in dmt_groups_ind]
    
    else:
        cov_cols = [c for c in cov_cols if c not in dmt_cols]
        cov_cols = [c for c in cov_cols if c not in dmt_cols_ind]

    if not include_country:
        cov_cols = [c for c in cov_cols if c != "country"]
        cov_cols = [c for c in cov_cols if c != "country_i"]
    if not include_clinic:
        cov_cols = [c for c in cov_cols if c != "clinic"]
        cov_cols = [c for c in cov_cols if c != "clinic_i"]

    if minimal_example:
        cov_cols = ["EDSS_at_0"]
        continuous_cols = ["EDSS_at_0"]
    
    cols = cov_cols + ["PATIENT_ID","slice_id","label"]
    return df[cols], cov_cols, continuous_cols


def prepare_msbase_static_dataset(df, df_path = None, no_dmt = False, include_country=False, include_clinic=False):
   
    if df is None:
        df = pd.read_csv(df_path,usecols = ["PATIENT_ID","slice_id","Time","label","EDSS","gender","Education_status","dmt_at_0","MSCOURSE_at_0","MSCOURSE_AT_VISIT","cumulative_relapse","dmt_current_set","start_dmt","end_dmt","disease_duration_at_0_years","age_at_onset_years_computed","age_at_0","last_KFS_1_at_0","last_KFS_2_at_0","last_KFS_3_at_0","last_KFS_4_at_0","last_KFS_5_at_0","last_KFS_6_at_0","last_KFS_7_at_0","last_KFS_AMBULATION_at_0", "current_dmt_ind_unique","date", "onset_date", "FIRST_SYMPTOM_OPTIC_PATHWAYS","FIRST_SYMPTOM_BRAINTSTEM","FIRST_SYMPTOM_SPINAL_CORD","FIRST_SYMPTOM_SUPRATENTORIAL"], parse_dates=["date","onset_date"])

    init_n = df.slice_id.nunique()
    df = df.loc[df.Time==0].copy()
    assert df.slice_id.nunique() == init_n

    # cols: all variables to use in the dataframe
    # cov_cols: all variables used to train the model
    # continuous_cols: all variables used to train the model that are continuous  
    cols = []
    continuous_cols = []
    # Categorical variables.
    # MSCOURSE_AT_VISIT
    dummies_course = pd.get_dummies(df["MSCOURSE_at_0"], prefix = "MSCOURSE_AT_VISIT")
    df = df.join(dummies_course)
    cols += list(dummies_course.columns)
    df.drop(columns = ["MSCOURSE_AT_VISIT","MSCOURSE_at_0"],inplace = True)

    #Education_status
    dummies_education = pd.get_dummies(df["Education_status"], prefix = "Education")
    df = df.join(dummies_education)
    cols += list(dummies_education.columns)
    df.drop(columns = ["Education_status"], inplace = True)
    
    # Gender
    dummies_gender = pd.get_dummies(df["gender"], prefix = "gender")
    df = df.join(dummies_gender)
    cols += list(dummies_gender.columns)
    df.drop(columns = ["gender"],inplace = True)

    # first symptoms
    first_symptom_arr = ["FIRST_SYMPTOM_OPTIC_PATHWAYS","FIRST_SYMPTOM_BRAINTSTEM","FIRST_SYMPTOM_SPINAL_CORD","FIRST_SYMPTOM_SUPRATENTORIAL"]
    for el in first_symptom_arr:
        dummies_symp = pd.get_dummies(df[el], prefix = el)
        df = df.join(dummies_symp)
        cols += list(dummies_symp.columns)
        df.drop(columns = [el],inplace = True)
   
    print(df.columns)
 
    # DMT_AT_0
    if not no_dmt:
        #dmt_kept = np.load(DATA_DIR+"/MSBase2020/Cleaned/kept_dmts.npy")
        dmt_groups = ["Mild","Moderate","High"]
        dmt_cols = ["DMT_AT_0_" + dmt for dmt in dmt_groups]
        df[dmt_cols] = 0
        df.loc[df["dmt_at_0"].isna(),"dmt_at_0"] = ""
        
        for dmt_col, dmt_original in zip(dmt_cols,dmt_groups):
            df.loc[df.dmt_at_0.astype(str).str.contains(dmt_original),dmt_col] = 1   
    
        cols += dmt_cols

    if not no_dmt:
        dmt_groups_ind = ["High_Induction"]
        dmt_cols_ind = ["DMT_IND_AT_0_" + dmt for dmt in dmt_groups_ind]
        df[dmt_cols_ind] = 0
        df.loc[df["dmt_ind_at_0"].isna(), "dmt_ind_at_0"] = ""
        
        for dmt_col, dmt_original in zip(dmt_cols_ind, dmt_groups_ind):
            df.loc[df.dmt_ind_at_0.astype(str).str.contains(dmt_original), dmt_col] = 1

        cols += dmt_cols_ind

    # Continuous variables
    df.rename(columns = {"EDSS":"EDSS_at_0"}, inplace = True)
    cols += ["EDSS_at_0"]
    continuous_cols += ["EDSS_at_0"]

    # date of visit and date of diagnosis, years since 1990
    df_dateref = pd.DataFrame({"year": [1990], "month": [1], "day": [1]})
    dateref = pd.to_datetime(df_dateref)
    ref_time = dateref.iloc[0]
    df["date_ref"] = ref_time
    df["date"] = pd.to_datetime(df["date"])
    df["onset_date"] = pd.to_datetime(df["onset_date"])
    df["date_reference"] = (df["date"] - df["date_ref"]).dt.days / 365.0
    df["onset_date_reference"] = (df["onset_date"] - df["date_ref"]).dt.days / 365.0

    cols += ["date_reference", "onset_date_reference"]
    continuous_cols += ["date_reference", "onset_date_reference"]

    # KFS cols
    kfs_cols = ["last_KFS_1_at_0","last_KFS_2_at_0","last_KFS_3_at_0","last_KFS_4_at_0","last_KFS_5_at_0","last_KFS_6_at_0","last_KFS_7_at_0","last_KFS_AMBULATION_at_0"]
    cols+= kfs_cols
    continuous_cols += kfs_cols
    #deal with missing KFS
    for kfs_col in kfs_cols:
        kfs_col_mask = kfs_col + "_mask"
        df[kfs_col_mask] = 1
        df.loc[df[kfs_col].isna(),kfs_col_mask] = 0
        df.loc[df[kfs_col].isna(),kfs_col] = -1
    
    cols += [kfs_col+"_mask" for kfs_col in kfs_cols]
    # Other pre-computed variables
    # 
    
    # Country_id (for Pieter and Dimitrios)
    if include_country:
        df['country'] = df.apply(isolate_country, axis=1)
        df['country_i'] = df['country'].astype('category').cat.codes
        cols += ["country","country_i"]
    #
    
    # Country_id (for Pieter and Dimitrios)
    if include_clinic:
        df['clinic'] = df.apply(isolate_clinic, axis=1)
        df['clinic_i'] = df['clinic'].astype('category').cat.codes
        cols += ["clinic","clinic_i"]
    #
    
    # Pre-computed variables
    
    cols +=            ["disease_duration_at_0_years","age_at_onset_years_computed","age_at_0"]
    continuous_cols += ["disease_duration_at_0_years","age_at_onset_years_computed","age_at_0"]

    cov_cols = cols.copy()
    cols += ["PATIENT_ID","slice_id","label"]

    #df.to_csv(DATA_DIR+"/MSBase2020/Cleaned/static_df.csv")
    
    return df[cols], cov_cols, continuous_cols

def get_msbase_dynamic_dataset(num_visits = 3):
    directory = DATA_DIR+f"/MSBase2020/Cleaned/{num_visits}_visits/Dynamic/"
    df = pd.read_pickle(directory + "df.pkl")
    with open(directory+"cov_cols.pkl", "rb") as fp:   # Unpickling
        cov_cols = pickle.load(fp)
    with open(directory+"continuous_cols.pkl", "rb") as fp:   # Unpickling
        continuous_cols = pickle.load(fp)

    cols = cov_cols + ["PATIENT_ID","slice_id","label"]

    return df[cols], cov_cols, continuous_cols

def prepare_msbase_dynamic_dataset(df, df_path = None, no_dmt = False):
    """
    if df is None and df_path is not None, loads the dataset from the path directly
    """

    if df is None:
        df = pd.read_csv(df_path,usecols = ["PATIENT_ID","slice_id","Time","label","EDSS","cumulative_relapse","dmt_current_set","start_dmt","end_dmt","KFS_1","KFS_2","KFS_3","KFS_4","KFS_5","KFS_6","KFS_7","KFS_AMBULATION","time_since_relapse","time_on_treatment","time_until_treatment","time_until_treatment_high","days_since_fampridine","days_between_onset_and_first_high_treatment","days_between_onset_and_first_treatment","ratio_on_treat"])

    init_n = df.slice_id.nunique()

    df = df.loc[df.Time<=0].copy()
    assert df.slice_id.nunique() == init_n

    # cols: all variables to use in the dataframe
    # cov_cols: all variables used to train the model
    # continuous_cols: all variables used to train the model that are continuous  

    cols = []
    continuous_cols = []

    #Maximum EDSS in the patient history
    df["Max_EDSS"] = df["slice_id"].map(df.groupby("slice_id")["EDSS"].max().to_dict())
    cols+= ["Max_EDSS"]
    continuous_cols += ["Max_EDSS"]

    df["Min_EDSS"] = df["slice_id"].map(df.groupby("slice_id")["EDSS"].min().to_dict())
    cols += ["Min_EDSS"]
    continuous_cols += ["Min_EDSS"]

    #Average EDSS in the last 3.25 years
    df_last_3y = df.loc[df.Time>=-3.25*365].copy()
    assert df_last_3y.slice_id.nunique() == init_n
    df["Average_EDSS_last3y"] = df["slice_id"].map(df_last_3y.groupby("slice_id")["EDSS"].mean().to_dict())
    cols+= ["Average_EDSS_last3y"]
    continuous_cols += ["Average_EDSS_last3y"]
    
    #First EDSS in the last 3.25 years
    df["First_EDSS_last3y"] = df["slice_id"].map(df_last_3y.sort_values(by=["slice_id","Time"]).groupby("slice_id")["EDSS"].first().to_dict())
    cols+= ["First_EDSS_last3y"]
    continuous_cols += ["First_EDSS_last3y"]

    #STD EDSS in the last 3 years
    assert df_last_3y.slice_id.nunique() == init_n
    df["Std_EDSS_last3y"] = df["slice_id"].map(df_last_3y.groupby("slice_id")["EDSS"].std().to_dict())
    cols += ["Std_EDSS_last3y"]
    continuous_cols += ["Std_EDSS_last3y"]

   
    kfs_var_post = ["1", "2", "3", "4", "5", "6", "7", "AMBULATION"]
    for el in kfs_var_post:
        #Maximum KFS in the patient history
        kfs_var = "KFS_" + el

        kfs_var_add = "Max_" + kfs_var
        df[kfs_var_add] = df["slice_id"].map(df.groupby("slice_id")[kfs_var].max())
        df[kfs_var_add] = df[kfs_var_add].fillna(-1)
        cols+= [kfs_var_add]
        continuous_cols += [kfs_var_add]

        kfs_var_add = "Min_" + kfs_var
        df[kfs_var_add] = df["slice_id"].map(df.groupby("slice_id")[kfs_var].min())
        cols += [kfs_var_add]
        continuous_cols += [kfs_var_add]
        df[kfs_var_add] = df[kfs_var_add].fillna(-1)

        #Average KFS in the last 3.25 years
        df_last_3y = df.loc[df.Time>=-3.25*365].copy()
        assert df_last_3y.slice_id.nunique() == init_n
        kfs_var_add = "Average_" + kfs_var + "_last3y"
        df[kfs_var_add] = df["slice_id"].map(df_last_3y.groupby("slice_id")[kfs_var].mean())
        df[kfs_var_add] = df[kfs_var_add].fillna(-1)
        cols+= [kfs_var_add]
        continuous_cols += [kfs_var_add]
    
        #First KFS in the last 3.25 years
        kfs_var_add = "First_" + kfs_var + "_last3y"
        df[kfs_var_add] = df["slice_id"].map(df_last_3y.sort_values(by=["slice_id","Time"]).groupby("slice_id")[kfs_var].first())
        df[kfs_var_add] = df[kfs_var_add].fillna(-1)
        cols+= [kfs_var_add]
        continuous_cols += [kfs_var_add]

        # STD KFS in the last 3 years
        assert df_last_3y.slice_id.nunique() == init_n
        kfs_var_add = "Std_" + kfs_var + "_last3y"
        df[kfs_var_add] = df["slice_id"].map(df_last_3y.groupby("slice_id")[kfs_var].std())
        df[kfs_var_add] = df[kfs_var_add].fillna(-1)
        cols += [kfs_var_add]
        continuous_cols += [kfs_var_add]


    # Cumulative number of relapses at t= 0
    df["cumulative_relapse_at_0"] = df["slice_id"].map(df.loc[df.Time==0].groupby("slice_id")["cumulative_relapse"].first())
    
    assert (df.loc[(df.Time==0)&(df.first_visit_date.isna())].shape[0]==0)
    df["relapse_rate"] = df["cumulative_relapse"] / (df["date"]-df["first_visit_date"]).dt.days
    df["relapse_rate_at_0"] = df["slice_id"].map(df.loc[df.Time==0].groupby("slice_id")["relapse_rate"].first())

    cols += ["relapse_rate_at_0"]
    continuous_cols += ["relapse_rate_at_0"]

    #Presence of a High DMT in the past
    df["High_present_tmp"] = df["dmt_current_set"].fillna("").apply(lambda x : "High" in x if str(x) else False)
    df["High_DMT_in_past"] = df["slice_id"].map(df.groupby("slice_id")["High_present_tmp"].any().to_dict()).astype(int) 
    df.drop(columns = ["High_present_tmp"], inplace = True)

    cols += ["High_DMT_in_past"]

    #Number of visits in the last 3 years
    df["Number_visits_last3y"] = df["slice_id"].map(df_last_3y.groupby("slice_id")["PATIENT_ID"].count().to_dict())
    cols += ["Number_visits_last3y"]
    continuous_cols += ["Number_visits_last3y"]

    # time_since_relapse
    df["time_since_relapse_transform"] = df["time_since_relapse"].apply(lambda x: transform_time_since_relapse(x))
    df["t_since_relapse"] = df["slice_id"].map(df.loc[df.Time==0].groupby("slice_id")["time_since_relapse_transform"].first())

    # time since fampridine
    df["time_since_fampridine_transform"] = df["days_since_fampridine"].apply(lambda x: transform_time_since(x))
    df["t_since_fampridine"] = df["slice_id"].map(df.loc[df.Time==0].groupby("slice_id")["time_since_fampridine_transform"].first())
    # time since treatment
    df["time_until_treatment"] = df["days_between_onset_and_first_treatment"].apply(lambda x: transform_time_since(x))
    df["t_until_treat"] = df["slice_id"].map(df.loc[df.Time==0].groupby("slice_id")["time_until_treatment"].first())
    # time since treatment
    df["time_until_treatment_high"] = df["days_between_onset_and_first_high_treatment"].apply(lambda x: transform_time_since(x))
    df["t_until_treat_high"] = df["slice_id"].map(df.loc[df.Time==0].groupby("slice_id")["time_until_treatment_high"].first())
    
    cols += ["t_since_relapse", "t_since_fampridine", "t_until_treat", "t_until_treat_high", "ratio_on_treat"]
    continuous_cols += ["t_since_relapse", "t_since_fampridine", "t_until_treat", "t_until_treat_high", "ratio_on_treat"]
 
    cov_cols = cols.copy()
    cols += ["PATIENT_ID","slice_id","label"]
    
    df = df.loc[df.Time==0].copy()
    assert df.slice_id.nunique() == init_n

    return df[cols], cov_cols, continuous_cols



def get_msbase_longitudinal_dataset(minimal_example = False, num_visits = 3):
    directory = DATA_DIR+f"/MSBase2020/Cleaned/{num_visits}_visits/Longitudinal/"
    df = pd.read_pickle(directory + "df_longitudinal.pkl")
    with open(directory+"continuous_longitudinal_columns.pkl", "rb") as fp:   # Unpickling
        continuous_longitudinal_cols = pickle.load(fp)
    with open(directory+"all_longitudinal_columns.pkl", "rb") as fp:   # Unpickling
        all_longitudinal_cols = pickle.load(fp)
    with open(directory+"mask_longitudinal_columns.pkl", "rb") as fp:   # Unpickling
        mask_longitudinal_cols = pickle.load(fp)
    if minimal_example:
        all_longitudinal_cols = ["EDSS"]
        continuous_longitudinal_cols = ["EDSS"]
        mask_longitudinal_cols = ["EDSS_mask"]
    cols = all_longitudinal_cols + mask_longitudinal_cols + ["slice_id","Time","Delta_Time", "label"] 
    return df[cols], all_longitudinal_cols, continuous_longitudinal_cols, mask_longitudinal_cols

def prepare_msbase_longitudinal_dataset(df):

    print("Preparing Longitudinal DataFrame...")
    # longitudinal_columns = ["EDSS","KFS_1","KFS_2","KFS_3","KFS_4","KFS_5","KFS_6","KFS_7","MSCOURSE_AT_VISIT","Relapse","T1_RESULT","T2_RESULT","T1_GADOLINIUM_RESULT","start_dmt","end_dmt","dmt_current_set","cumulative_relapse"]
    longitudinal_columns = ["EDSS","KFS_1","KFS_2","KFS_3","KFS_4","KFS_5","KFS_6","KFS_7","KFS_AMBULATION","MSCOURSE_AT_VISIT","Relapse","dmt_current_set","cumulative_relapse","BOWEL_BLADDER", "cerebellum", "VISUAL_FUNCTION", "SENSORY_FUNCTION", "PYRAMIDAL_TRACT", "brainstem", "FAMPRIDINE_START_DATE","dmt_current_set_ind"]
    out_columns = longitudinal_columns.copy()
    categorical_cols = ["Relapse"]
    continuous_cols = ["EDSS","KFS_1","KFS_2","KFS_3","KFS_4","KFS_5","KFS_6","KFS_7","KFS_AMBULATION","cumulative_relapse"]
    to_mask_cols = ["EDSS","KFS_1","KFS_2","KFS_3","KFS_4","KFS_5","KFS_6","KFS_7","KFS_AMBULATION"]
    always_observed_cols = ["cumulative_relapse"]
    
    #Consistency check with Fampridine
    df_fampridine =  df.loc[~df.FAMPRIDINE_START_DATE.isna()]
    assert (df_fampridine["FAMPRIDINE_START_DATE"] == df_fampridine["date"]).all()

    init_n = df.slice_id.nunique()
    
    df = df.loc[df.Time<=0].copy()
    df = df[["slice_id","Time","label"]+longitudinal_columns]
    assert df.slice_id.nunique() == init_n


    #Create a delta time
    df["Delta_Time"] = df.groupby("slice_id")["Time"].transform(lambda x: x.diff())
    df.loc[df.Delta_Time.isna(),"Delta_Time"] = 0.
    
    #Process the cumulative relapses
    df["cumulative_relapse"] = df.groupby("slice_id")["cumulative_relapse"].transform(lambda x: x.ffill())
    df.loc[df.cumulative_relapse.isna(),"cumulative_relapse"] = 0.

    #Process MSCOURSE_AT_VISIT
    df = pd.get_dummies(df, columns = ["MSCOURSE_AT_VISIT"], prefix = "MSCOURSE",dummy_na = True)
    ms_course_cols = [x for x in df.columns if ("MSCOURSE" in x) ]
    out_columns += ms_course_cols
    categorical_cols +=  ms_course_cols
    always_observed_cols += ms_course_cols
    out_columns.remove("MSCOURSE_AT_VISIT")

    #DMT Current set
    df["dmt_current_set_True"] = df.dmt_current_set.apply(lambda x : len(x) if isinstance(x,list) else 0)
    df.loc[df.dmt_current_set_True==0,"dmt_current_set"] = np.nan
    df["dmt_current_set"] = df.dmt_current_set.apply(lambda x : x[0] if isinstance(x,list) else np.nan)
    
    df = pd.get_dummies(df,columns = ["dmt_current_set"], prefix = "dmt_current_set",dummy_na = False)
    dmt_current_cols = [x for x in df.columns if ("dmt_current_set" in x) and ("nan" not in x) and ("ind" not in x) and ("True" not in x)]
    out_columns += dmt_current_cols
    categorical_cols +=  dmt_current_cols
    always_observed_cols += dmt_current_cols
    out_columns.remove("dmt_current_set")

    #DMT Current set ind
    df["dmt_current_set_ind_True"] = df.dmt_current_set_ind.apply(lambda x : len(x) if isinstance(x,set) else 0)
    df["dmt_current_set_ind_True"] = df["dmt_current_set_ind_True"].fillna(0)
    
    out_columns += ["dmt_current_set_ind_True"]
    categorical_cols +=  ["dmt_current_set_ind_True"]
    always_observed_cols += ["dmt_current_ind_True"]
    out_columns.remove("dmt_current_set_ind")


    #Relapse + position
    relapse_pos_cols = ["BOWEL_BLADDER", "cerebellum", "VISUAL_FUNCTION", "SENSORY_FUNCTION", "PYRAMIDAL_TRACT", "brainstem"]
    categorical_cols += relapse_pos_cols
    df[relapse_pos_cols].fillna(False)
    #FAMPRIDINE_START_DATE
    df["Fampridine"] = 0
    df.loc[~df.FAMPRIDINE_START_DATE.isna(),"Fampridine"] = 1
    out_columns += ["Fampridine"]
    categorical_cols += ["Fampridine"]
    always_observed_cols += ["Fampridine"]
    out_columns.remove("FAMPRIDINE_START_DATE")

    #Create Masks
    mask_cols = [f"{value}_mask" for value in to_mask_cols]
    df[mask_cols] = ~df[to_mask_cols].isna()

    total_cols = ["slice_id","Time","Delta_Time", "label"] + out_columns + mask_cols
   
    df_out = df[total_cols].copy()

    #convert booleans to 0/1
    df_out[categorical_cols] = df_out[categorical_cols].astype(float)
    df_out[mask_cols] = df_out[mask_cols].astype(float)

    # missing values are casted to 0
    df_out[out_columns] = df_out[out_columns].fillna(0.)
    
    #assert df_out.loc[(df_out[mask_cols]==0).all(1)].shape[0]==0

    assert df_out.slice_id.nunique() == init_n

    print("Longitudinal DataFrame ready.")

    return df_out, out_columns, mask_cols, continuous_cols, always_observed_cols

def load_longitudinal_data(longitudinal_data_path, select_cols = None):
    """
    Load a subset of the longitudinal dataset.
    Input : 
    * path
    * cols : list of columns to use. Only keywoard should be provided. All available columns in the original dataset matching the cols keywords will be selected
    Output:
    * subset of the dataset (includes slice_id, Time, Delta_Time and label)
    * list of all data columns used (excludes slice_id, Time, Delta_Time and label)
    * continuous columns used
    """

    continuous_cols_path = "/".join(longitudinal_data_path.split("/")[:-1]) + "/continuous_longitudinal_columns.pkl"
    all_cols_path = "/".join(longitudinal_data_path.split("/")[:-1]) + "/all_longitudinal_columns.pkl"
    always_observed_cols_path =  "/".join(longitudinal_data_path.split("/")[:-1]) + "/always_observed_columns.pkl"

    
    with open(continuous_cols_path, 'rb') as f:
        continuous_cols = pickle.load(f)

    with open(all_cols_path, 'rb') as f:
        all_cols = pickle.load(f)
    
    with open(always_observed_cols_path, 'rb') as f:
        always_observed_cols = pickle.load(f)
    
    def in_col(c,col_):
        return any(c_ in c for c_ in col_)

    if select_cols is not None:
        cols = [c for c in all_cols if in_col(c,select_cols)]
        mask_cols = [f"{col}_mask" for col in cols]
        full_cols = ["slice_id","Time","Delta_Time","label"] + cols + mask_cols
        continuous_cols = [col for col in cols if col in continuous_cols]

        non_always_observed_cols = np.setdiff1d(np.array(cols),np.array(always_observed_cols)).tolist()
        non_always_observed_cols_mask = [f"{c}_mask" for c in non_always_observed_cols]

        df = pd.read_csv(longitudinal_data_path, usecols = full_cols).sort_values(by=["slice_id","Time"])

        #We now remove the time steps where the non always observed columns are missing and where the always observed columns remain unchanged from past occurence
        df = df.drop(df.loc[((df[non_always_observed_cols_mask]==0).all(1) & (df.duplicated(subset=["slice_id"]+always_observed_cols, keep = "first")))].index).copy()
        
        #Checking if some columns are always observed, in which case, we dont need a mask for them.
        
        number_of_obs_per_feature = (df[mask_cols]==0).sum()
        necessary_mask_cols = number_of_obs_per_feature[number_of_obs_per_feature>0].index.tolist()


        return df, cols, necessary_mask_cols, continuous_cols

    else:
        raise("Not Implemented Error")
        df = pd.read_csv(longitudinal_data_path)
        return df, all_cols, continuous_cols

