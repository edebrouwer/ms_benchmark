import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions

@tf.function
def aleatoric_loss(y_true, logits, T=20):
    n = tfd.Normal(loc=0., scale=1.)
    y_true = tf.cast(y_true, tf.float32)
    logits = tf.cast(logits, tf.float32)
    
    n_out = 1
    
    mu = logits[:,:n_out]
    logvar = logits[:,n_out:]

    def monte_carlo_integration_step():
        x_hat = mu + (tf.math.exp(0.5*logvar)*n.sample(tf.shape(logvar)))
        return tf.math.exp(-tf.nn.sigmoid_cross_entropy_with_logits(labels=y_true, logits=x_hat))

    T_range = tf.range(T, dtype=tf.float32)
    res = tf.vectorized_map(lambda i: monte_carlo_integration_step(), T_range)
    return -tf.math.log(tf.reduce_mean(res, axis=0))

class MCDropoutModel(tf.keras.models.Model):
    def test_step(self, data):
        # Unpack the data
        x, y = data
        # Compute predictions
        y_pred = self(x, training=True)
        # Updates the metrics tracking the loss
        self.compiled_loss(y, y_pred, regularization_losses=self.losses)
        # Update the metrics.
        self.compiled_metrics.update_state(y, y_pred)
        # Return a dict mapping metric names to current value.
        # Note that it will include the loss (tracked in self.metrics).
        return {m.name: m.result() for m in self.metrics}


def make_model(input_features, depth=5, hidden_units_per_layer=256, dropout_rate=0.4, l2_reg=1e-4, residual=False, activation='relu', lr=0.001):
    kl_divergence_function = (lambda q, p, _: tfd.kl_divergence(q, p) /  # pylint: disable=g-long-lambda
                            tf.cast(data_train.cardinality(), dtype=tf.float32))

    inp = tf.keras.layers.Input((input_features,))
    x = inp
    for i in range(depth):
        input_x = x
        x = tf.keras.layers.Dense(hidden_units_per_layer, activation=activation, kernel_regularizer=tf.keras.regularizers.l2(l2_reg), bias_regularizer=tf.keras.regularizers.l2(l2_reg))(x)
        if residual and i > 0:
            x = input_x + x
        x = tf.keras.layers.Dropout(dropout_rate)(x)

    outp_mu_sigma = tf.keras.layers.Dense(1*2, activation=None, kernel_regularizer=tf.keras.regularizers.l2(l2_reg), bias_regularizer=tf.keras.regularizers.l2(l2_reg))(x)
    outp_pos = tf.keras.layers.Activation('sigmoid', name='mean_prediction')(outp_mu_sigma[...,:1])

    model = MCDropoutModel(inp, {'outp_mu_sigma': outp_mu_sigma, 'outp_pos': outp_pos})

    # Model compilation.
    optimizer = tf.keras.optimizers.SGD(
        learning_rate=lr, momentum=0.9, nesterov=False, name='SGD'
    )


    model.compile(
        optimizer,
        loss={'outp_mu_sigma': aleatoric_loss},
        metrics={'outp_pos': ['binary_crossentropy', 'accuracy', tf.keras.metrics.AUC(curve='ROC', name='auc_roc'), tf.keras.metrics.AUC(curve='PR', name='auc_pr')]}
    )
    return model
