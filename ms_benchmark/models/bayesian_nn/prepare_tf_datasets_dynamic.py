from ms_benchmark import DATA_DIR
import pandas as pd
import numpy as np
from ms_benchmark.utils import str2bool
import sys
#from ms_benchmark.data_utils import normalize_df, prepare_msbase_static_dataset, prepare_msbase_dynamic_dataset
from ms_benchmark.data_utils import normalize_df, get_msbase_static_dataset, get_msbase_dynamic_dataset
from imblearn.over_sampling import SMOTENC, RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler


DATATYPE_DICT = ("EP","MSBase2020")

# Load all parameters as tracked by dvc
import yaml

with open("params.yaml", 'r') as fd:
    PARAMS = yaml.safe_load(fd)

num_visits = PARAMS['num_visits']
DATASET = 'dynamic'

class TFPointWiseDataset():
    def __init__(self, datatype, fold, no_dmt, **kwargs):
        super().__init__()
        self.datatype = datatype #EP or MSBase2020

        self.fold = fold
        self.input_dim = 0

        self.no_dmt = no_dmt

    def prepare_data(self):
        import tensorflow as tf

        # Important for SUMO-* servers
        gpus = tf.config.experimental.list_physical_devices('GPU')
        print(gpus)
        if gpus:
            try:
                gpu = gpus[PARAMS['gpu_num']]
                tf.config.experimental.set_visible_devices(gpu, 'GPU')
                tf.config.experimental.set_memory_growth(gpu, True)
                logical_gpus = tf.config.experimental.list_logical_devices('GPU')
                print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
            except RuntimeError as e:
                # Visible devices must be set before GPUs have been initialized
                print(e)

        df_processed_static, cov_cols_static, continuous_cols_static = get_msbase_static_dataset(no_dmt = self.no_dmt, num_visits=num_visits)
        
        df_processed_dynamic, cov_cols_dynamic, continuous_cols_dynamic = get_msbase_dynamic_dataset(num_visits=num_visits)
       
        df_processed = pd.merge(df_processed_static, df_processed_dynamic, on = ["PATIENT_ID","slice_id","label"], how = "outer")
       
        assert(df_processed.slice_id.nunique()==df_processed.shape[0])
        assert(df_processed.shape[0]==df_processed_static.shape[0])
        assert(df_processed.shape[0]==df_processed_dynamic.shape[0])

        cov_cols = cov_cols_static + cov_cols_dynamic
        continuous_cols = continuous_cols_static + continuous_cols_dynamic

        self.label_train = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{num_visits}_visits/folds/fold_{self.fold}/train_idx.csv")
        self.label_val = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{num_visits}_visits/folds/fold_{self.fold}/val_idx.csv")
        self.label_test = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{num_visits}_visits/folds/fold_{self.fold}/test_idx.csv")

        df_train = df_processed.loc[df_processed.slice_id.isin(self.label_train.slice_id.unique())].reset_index().sort_values(by="slice_id")
        df_val = df_processed.loc[df_processed.slice_id.isin(self.label_val.slice_id.unique())].reset_index().sort_values(by="slice_id")
        df_test = df_processed.loc[df_processed.slice_id.isin(self.label_test.slice_id.unique())].reset_index().sort_values(by="slice_id")

        df_test.to_pickle(f"artifacts/{DATASET}/test_df_{self.fold}")
        df_train, df_val, df_test = normalize_df(df_train,df_val, df_test, continuous_cols)
        #categorical_columns_mask = ~np.isin(df_train[cov_cols].columns.values, continuous_cols)
        sm = RandomUnderSampler(random_state=PARAMS['seed'])

        X_train_orig = df_train[cov_cols].values
        y_train_orig = df_train.label.values.astype(int)
        
        if PARAMS['train_test_split']['resample_train']:
            X_train, y_train = sm.fit_resample(X_train_orig, y_train_orig)
        else:
            X_train, y_train = X_train_orig, y_train_orig
        
        X_val = df_val[cov_cols].values
        y_val = df_val.label.values.astype(int)

        X_test = df_test[cov_cols].values
        y_test = df_test.label.values.astype(int)
        assert y_test.shape[0] == X_test.shape[0]
        
        self.data_train = tf.data.Dataset.from_tensor_slices((X_train,y_train))
        self.data_train_orig = tf.data.Dataset.from_tensor_slices((X_train_orig,y_train_orig))
        self.data_val = tf.data.Dataset.from_tensor_slices((X_val,y_val))
        self.data_test = tf.data.Dataset.from_tensor_slices((X_test,y_test))
        assert y_test.shape[0] == self.data_test.cardinality()


        self.input_dim = X_train.shape[1]
        
        tf.data.experimental.save(self.data_train, f"artifacts/{DATASET}/train_dataset_{self.fold}")
        tf.data.experimental.save(self.data_train_orig, f"artifacts/{DATASET}/train_dataset_orig_{self.fold}")
        tf.data.experimental.save(self.data_val, f"artifacts/{DATASET}/val_dataset_{self.fold}")
        tf.data.experimental.save(self.data_test, f"artifacts/{DATASET}/test_dataset_{self.fold}")


    
if __name__ == "__main__":
    ds = TFPointWiseDataset('MSBase2020', int(sys.argv[1]), False)
    ds.prepare_data()
