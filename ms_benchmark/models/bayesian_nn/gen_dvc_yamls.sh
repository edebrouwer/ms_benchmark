#!/usr/bin/env bash

sed 's/%dataset%/static/g' dvc_base.yaml > static/dvc.yaml
sed 's/%dataset%/dynamic/g' dvc_base.yaml > dynamic/dvc.yaml

cp params.yaml static/params.yaml
cp params.yaml dynamic/params.yaml