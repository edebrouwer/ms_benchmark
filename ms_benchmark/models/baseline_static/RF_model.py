from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score, roc_curve, precision_recall_curve

from sklearn.metrics import auc
import argparse

class RF_model:
    def __init__(self,n_estimators, max_depth, min_samples_split, **kwargs):
        self.n_estimators = n_estimators
        self.max_depth = max_depth
        self.max_features = "auto"
        self.min_samples_split = min_samples_split

    def train_model(self,X_train,y_train):
        self.clf=RandomForestClassifier(n_estimators=int(self.n_estimators),max_depth=int(self.max_depth),
                                    max_features = self.max_features, min_samples_split = int(self.min_samples_split), class_weight="balanced")
        self.clf.fit(X_train,y_train)
        return

    def evaluate_model(self,X,y, suffix = ""):
        y_pred = self.clf.predict_proba(X)[:,1]
        roc_auc = roc_auc_score(y,y_pred)
        return {"roc_auc"+suffix : roc_auc}

    @classmethod
    def add_model_specific_args(cls, parent):
        parser = argparse.ArgumentParser(parents=[parent])
        parser.add_argument("--n_estimators", type=int, default=100)
        parser.add_argument("--max_depth", type=int, default=10)
        parser.add_argument('--min_samples_split',type=int,default=5)
        return parser


