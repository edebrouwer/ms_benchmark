# Static Model

The static model only uses information available at t=0 (hence no past information).

## List of Static Variables used :

- MSCourse at visit (t=0)
- Gender
- DMT at t=0 (Mild, Moderate, High, Immunosuppressor)
- disease duration at t=0
- age at onset in years
- age at time = 0 

