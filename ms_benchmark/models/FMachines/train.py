#!/usr/bin/env python


import pickle
import wandb
import numpy as np
import scipy
from factorizationmachine_utils import CustomDataset
from factorizationmachine_utils import FM
from ms_benchmark.utils import str2bool
import argparse


def main(args):
    
    run = wandb.init(project="msbase_21", entity='ms_prognosis_2021', config = args, name = "FMachine")
    config = args

    ds = CustomDataset(datatype=config.dataset, fold=config.fold, no_dmt=config.no_dmt, data_version=config.dataset_version)#, **kwargs)
    df_train, df_val, df_test = ds.prepare_data(num_visits = config.num_visits, no_dmt=config.no_dmt)
    ytrain, yval, ytest = np.array(df_train.pop('label')), np.array(df_val.pop('label')), np.array(df_test.pop('label'))
    Xtrain, Xval, Xtest = scipy.sparse.csr_matrix(np.array(df_train,float)), scipy.sparse.csr_matrix(np.array(df_val,float)), scipy.sparse.csr_matrix(np.array(df_test,float))
    
    fm = FM(num_factors=config.num_factors, num_iter=config.max_iter, task=config.task, initial_learning_rate=config.initial_learning_rate, learning_rate_schedule=config.learning_rate_schedule) #, initial_learning_rate=0.001, learning_rate_schedule="optimal")
    print("fit fm")
    print(Xtrain.shape, Xval.shape, ytrain.shape, yval.shape)
    best_model = fm.fit(Xtrain,ytrain, Xval, yval, Xtest, ytest, log=True)
    # Save the returned best model
    with open('SavedModels/'+str(run.id), 'wb') as filename:
        pickle.dump(best_model, filename)

  

if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--fold', type=int)
    parser.add_argument('--num_factors', type=int)
    parser.add_argument('--initial_learning_rate', type=float)
    parser.add_argument('--max_iter', type=int)
    parser.add_argument('--no_dmt', type=str2bool, default = False)
    parser.add_argument('--learning_rate_schedule', type=str, default = "optimal")
    parser.add_argument('--task', type=str, default = "classification")
    parser.add_argument('--dataset', type=str, default = "MSBase2020")
    parser.add_argument('--dataset_version', type=str)
    parser.add_argument('--num_visits', type=int)
    
    args = parser.parse_args()
    
    main(args)
