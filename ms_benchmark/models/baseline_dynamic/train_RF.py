#!/usr/bin/env python
import argparse
import sys

from pytorch_lightning.loggers import WandbLogger
import pytorch_lightning as pl
import ms_benchmark.models.baseline_dynamic.RF_model as RF_model
import ms_benchmark.models.baseline_dynamic.data_utils as data_utils

from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from pytorch_lightning.callbacks import EarlyStopping
import wandb

DATA_MAP = {"PointWise": data_utils.PointWiseDataset}
MODEL_MAP = {"RF": RF_model.RF_model}


def main(model_cls, dataset_cls,args):

    wandb.init(name = "RF_dynamic", project = "msbase_21", entity = "ms_prognosis_2021", config = args)
    dataset = dataset_cls(**vars(args))
    (X_train, y_train), (X_val,y_val), (X_test,y_test) = dataset.prepare_data(return_data = True)
   

    model = model_cls(**vars(args))

    model.train_model(X_train,y_train)
    results_dict = model.evaluate_model(X_train,y_train,"_train")
    results_dict.update(model.evaluate_model(X_val,y_val,"_val"))
    results_dict.update(model.evaluate_model(X_test,y_test,"_test"))
    wandb.log(results_dict)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--model', type=str, choices=MODEL_MAP.keys())
    parser.add_argument('--dataset', type=str,
                        choices=DATA_MAP.keys())
    parser.add_argument('--gpu', default=None, type=str)

    partial_args, _ = parser.parse_known_args()
    
    model_cls = MODEL_MAP[partial_args.model]
    dataset_cls = DATA_MAP[partial_args.dataset]

    parser = model_cls.add_model_specific_args(parser)
    parser = dataset_cls.add_dataset_specific_args(parser)
    args = parser.parse_args()

    main(model_cls, dataset_cls, args)
