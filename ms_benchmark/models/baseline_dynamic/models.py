import pytorch_lightning as pl
import argparse
import torch
from sklearn.metrics import roc_auc_score, average_precision_score
from ms_benchmark.utils import str2bool

class PointWiseModel(pl.LightningModule):
    def __init__(self,input_dim, hidden_dim, dropout_p, n_layers, weight_decay, weighted_loss, prop_pos, **kwargs):
        super().__init__()
        self.save_hyperparameters()

        if n_layers == 0 :
            self.pre_layers = torch.nn.Linear(input_dim,hidden_dim)
        else:
            self.pre_layers = torch.nn.Sequential(torch.nn.Linear(input_dim,hidden_dim),
                torch.nn.ReLU(),
                torch.nn.Dropout(dropout_p))

        self.mid_layers = torch.nn.ModuleList()
        for layer in range(n_layers-1):
            self.mid_layers.append(torch.nn.Sequential(torch.nn.Linear(hidden_dim,hidden_dim),
                                                        torch.nn.ReLU(),
                                                        torch.nn.Dropout(dropout_p)))
        
        self.out_layer = torch.nn.Sequential(torch.nn.Linear(hidden_dim,2))
        #self.mod = torch.nn.Sequential(torch.nn.Linear(input_dim,hidden_dim),
        #                                torch.nn.ReLU(),
        #                                torch.nn.Dropout(dropout_p),
        #                                torch.nn.Linear(hidden_dim, hidden_dim),
        #                                torch.nn.ReLU(),
        #                                torch.Dropout(dropout_p)
        #                                torch.nn.Linear(hidden_dim,2))

        weights = torch.ones(2)
        if weighted_loss:
            weights[1] = (1-prop_pos)/prop_pos
        
        self.loss = torch.nn.CrossEntropyLoss(weight = weights )
            
        self.accuracy = pl.metrics.Accuracy()
        self.accuracy_val = pl.metrics.Accuracy()
        self.accuracy_test = pl.metrics.Accuracy()

        self.weight_decay = weight_decay

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.hparams.lr, weight_decay = self.weight_decay)

        return optimizer

    def forward(self,x):

        x = self.pre_layers(x)
        for mod in self.mid_layers:
            x = mod(x)
        x = self.out_layer(x)

        return x

    def training_step(self, batch, batch_idx):
        x,y = batch
        y_pred = self(x)
        loss = self.loss(y_pred,y)

        self.accuracy(y_pred,y)

        self.log("train_loss", loss, on_step=True, on_epoch=True)
        self.log("train_acc", self.accuracy, on_step=True, on_epoch=True)
        return loss

    def validation_step(self, batch, batch_idx):

        x,y = batch
        y_pred = self(x)
        loss = self.loss(y_pred,y)

        self.accuracy_val(y_pred, y)

        self.log("val_loss", loss, on_epoch=True)

        self.log("val_acc", self.accuracy_val, on_epoch=True)

        return {"y":y, "y_pred":y_pred}

    def validation_epoch_end(self,data):
        y_full = torch.cat([el["y"] for el in data]).cpu().numpy()
        y_pred_full = torch.softmax(torch.cat([el["y_pred"] for el in data]),1).cpu().numpy()
        auc = roc_auc_score(y_full, y_pred_full[:,1])
        self.log("val_auc", auc)

    def test_step(self, batch, batch_idx):
        x,y = batch
        y_pred = self(x)
        loss = self.loss(y_pred,y)

        self.accuracy_test(y_pred, y)
        
        self.log("test_loss", loss, on_epoch=True)
        self.log("test_acc", self.accuracy_test, on_epoch=True)
        return {"y":y, "y_pred":y_pred}

    def test_epoch_end(self,data):
        y_full = torch.cat([el["y"] for el in data]).cpu().numpy()
        y_pred_full = torch.softmax(torch.cat([el["y_pred"] for el in data]),1).cpu().numpy()
        auc = roc_auc_score(y_full, y_pred_full[:,1])
        self.log("test_auc", auc)

    def predict_step(self,batch, batch_idx):
        x,y = batch
        y_pred = self(x)

        return {"y":y, "y_pred":y_pred, "x":x}

        

    @classmethod
    def add_model_specific_args(cls, parent):
        parser = argparse.ArgumentParser(parents=[parent])
        parser.add_argument("--hidden_dim", type=int, default=32)
        parser.add_argument("--lr", type=float, default=0.001)
        parser.add_argument('--n_layers', type=int, default=2)
        parser.add_argument('--dropout_p',type=float,default=0.)
        parser.add_argument('--weight_decay',type=float,default=0.)
        parser.add_argument('--weighted_loss',type=str2bool,default=True)
        return parser





