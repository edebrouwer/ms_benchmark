import pytorch_lightning as pl
from ms_benchmark.models.baseline_dynamic import models, data_utils
from ms_benchmark import predict_utils
DATA_MAP = {"PointWise": data_utils.PointWiseDataset}
MODEL_MAP = {"PointWise": models.PointWiseModel}


if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--model', type=str, choices=MODEL_MAP.keys(), default = "PointWise")
    parser.add_argument('--dataset', type=str,
                        choices=DATA_MAP.keys(), default = "PointWise")
    parser.add_argument('--run_name', type=str, default = "4iilhs78")
    parser.add_argument('--gpu', default=None, type=str)

    partial_args, _ = parser.parse_known_args()

    model_cls = MODEL_MAP[partial_args.model]
    data_cls = DATA_MAP[partial_args.dataset]

    model, dataset = predict_utils.load_run(partial_args.run_name, model_cls, data_cls)
    trainer = pl.Trainer(logger = False,gpus = gpu)
    out = trainer.predict(model,dataset.test_dataloader())
    
    y_true = torch.cat([o["y"] for o in out]).cpu()
    y_pred = torch.cat([o["y_pred"] for o in out]).cpu()

    return ()
