from ms_benchmark import DATA_DIR
import torch
import pytorch_lightning as pl
import pandas as pd
import numpy as np
from ms_benchmark.utils import str2bool

from ms_benchmark.data_utils import normalize_df, get_msbase_static_dataset, get_msbase_dynamic_dataset

from torch.utils.data import DataLoader

DATATYPE_DICT = ("EP","MSBase2020")


class PointWiseDataset(pl.LightningDataModule):
    def __init__(self, datatype, fold, batch_size, no_dmt, num_workers = 4, num_visits = 3, **kwargs):
        super().__init__()
        self.num_workers = num_workers
        self.datatype = datatype #EP or MSBase2020

        self.fold = fold
        self.batch_size = batch_size
        self.input_dim = 0

        self.no_dmt = no_dmt
        self.num_visits = num_visits
        

    def prepare_data(self, return_data = False, return_df = False, shuffle_pos = -1):

        #df_full = pd.read_csv(DATA_DIR+"/"+self.datatype+"/Cleaned/df_processed.csv").sort_values(by=["slice_id","Time"])

        # This changed (number of visits)
        df_processed_static, cov_cols_static, continuous_cols_static = get_msbase_static_dataset(no_dmt = self.no_dmt, num_visits = self.num_visits)
        
        df_processed_dynamic, cov_cols_dynamic, continuous_cols_dynamic = get_msbase_dynamic_dataset(num_visits = self.num_visits)
       
        df_processed = pd.merge(df_processed_static, df_processed_dynamic, on = ["PATIENT_ID","slice_id","label"], how = "outer")
       
        assert(df_processed.slice_id.nunique()==df_processed.shape[0])
        assert(df_processed.shape[0]==df_processed_static.shape[0])
        assert(df_processed.shape[0]==df_processed_dynamic.shape[0])

        cov_cols = cov_cols_static + cov_cols_dynamic
        continuous_cols = continuous_cols_static + continuous_cols_dynamic
        self.cov_cols = cov_cols

        # This changed.
        self.label_train = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/train_idx.csv")
        self.label_val = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/val_idx.csv")
        self.label_test = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/test_idx.csv")

        df_train = df_processed.loc[df_processed.slice_id.isin(self.label_train.slice_id.unique())].reset_index().sort_values(by="slice_id")
        df_val = df_processed.loc[df_processed.slice_id.isin(self.label_val.slice_id.unique())].reset_index().sort_values(by="slice_id")
        df_test = df_processed.loc[df_processed.slice_id.isin(self.label_test.slice_id.unique())].reset_index().sort_values(by="slice_id")

        self.df_train = df_train
        self.df_val = df_val
        self.df_test = df_test

        df_train, df_val, df_test = normalize_df(df_train,df_val, df_test, continuous_cols)

    
        X_train = df_train[cov_cols].values
        y_train = df_train.label.values.astype(int)
        
        X_val = df_val[cov_cols].values
        y_val = df_val.label.values.astype(int)

        X_test = df_test[cov_cols].values
        y_test = df_test.label.values.astype(int)

        #if shuffle_pos>=0:
        #    X_train[:,shuffle_pos] = np.random.shuffle(X_train[:,shuffle_pos])
        #    X_val[:,shuffle_pos] = np.random.shuffle(X_val[:,shuffle_pos])
        #    X_test[:,shuffle_pos] = np.random.shuffle(X_test[:,shuffle_pos])

        self.data_train = torch.utils.data.TensorDataset(torch.Tensor(X_train),torch.LongTensor(y_train))
        self.data_val = torch.utils.data.TensorDataset(torch.Tensor(X_val),torch.LongTensor(y_val))
        self.data_test = torch.utils.data.TensorDataset(torch.Tensor(X_test),torch.LongTensor(y_test))

        self.input_dim = X_train.shape[1]
        self.cov_cols = cov_cols
        
        self.prop_pos = y_train.mean()

    def get_df(self):
        return self.df_train, self.df_val, self.df_test

    def train_dataloader(self, shuffle_pos = -1):
        return DataLoader(
            self.data_train,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
            drop_last=True,
            pin_memory=True
        )

    def val_dataloader(self):
        return DataLoader(
            self.data_val,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            drop_last=False,
            pin_memory=True
        )

    def test_dataloader(self):
        return DataLoader(
            self.data_test,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            drop_last=False,
            pin_memory=True
        )

    def test_dataloader_shuffle(self,shuffle_pos):
        if shuffle_pos>=0:
            data_test_, data_test_y_ = self.data_test.tensors
            data_test_ = data_test_.clone()
            data_test_[:,shuffle_pos] = data_test_[torch.randperm(data_test_.shape[0]),shuffle_pos]
        else:
            return self.test_dataloader()
        
        return DataLoader(
            torch.utils.data.TensorDataset(data_test_,data_test_y_),
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            drop_last=False,
            pin_memory=True
        )

    def val_dataloader_shuffle(self,shuffle_pos):
        if shuffle_pos>=0:
            data_val_, data_val_y_ = self.data_val.tensors
            data_val_ = data_val_.clone()
            data_val_[:,shuffle_pos] = data_val_[torch.randperm(data_val_.shape[0]),shuffle_pos]
        else:
            return self.val_dataloader()
        return DataLoader(
            torch.utils.data.TensorDataset(data_val_,data_val_y_),
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            drop_last=False,
            pin_memory=True
        )
        

    @classmethod
    def add_dataset_specific_args(cls, parent):
        import argparse
        parser = argparse.ArgumentParser(parents=[parent], add_help=False)
        parser.add_argument('--fold', type=int, default=0)
        parser.add_argument('--batch_size', type=int, default=128)
        parser.add_argument('--datatype', type=str, default="MSBase2020", choices = DATATYPE_DICT)
        parser.add_argument('--no_dmt',type=str2bool, default = False, help =  "dmts are removed from the variables")
        parser.add_argument('--num_visits',type=int, default = 3, help =  "Number of workers to use")
        return parser


