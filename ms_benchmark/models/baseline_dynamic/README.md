# Dynamic Model

The dynamic model only uses information available at t=0 and summary statistics of past information.

## List of Static Variables used :

- MSCourse at visit (t=0)
- Gender
- DMT at t=0 (Mild, Moderate, High, Immunosuppressor)
- disease duration at t=0
- age at onset in years
- age at time = 0 

## List of Dynamic Variables used : 

- Maximum EDSS in the history (Max_EDSS)
- Average EDSS in the last 3 years (Average_EDSS_last3y)
- STD in EDSS in the last 3 years (Std_EDSS_last3y)
- Cumulative number of relapses at t = 0 (cumulative_relapse_at_0)
- Presence of High active DMT in the past (High_DMT_in_past)
- Number of visits in the last 3 years (Number_visits_last3y)


