#!/usr/bin/env python
import argparse
import sys

from pytorch_lightning.loggers import WandbLogger
import pytorch_lightning as pl
import ms_benchmark.models.AutoMTP_light.model as models
import ms_benchmark.models.AutoMTP_light.data_utils as data_utils

from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from pytorch_lightning.callbacks import EarlyStopping

DATA_MAP = {"MultiTask": data_utils.MultiTargetDataset}
MODEL_MAP = {"MultiTask": models.DeepMTPModel}

def get_logger(args):
    return WandbLogger(
            name=f"deepMTP_static",
            project="msbase_21",
            entity="ms_prognosis_2021",
            log_model=True,
            tags=[args.model, args.dataset]
        )

def main(model_cls, dataset_cls,args):
    
    dataset = dataset_cls(**vars(args))
    dataset.prepare_data()
    gpu = args.gpu
    

    model = model_cls( instance_features_dim=dataset.instance_features_dim, target_features_dim=dataset.target_features_dim,
        **vars(args),
     )

    # Loggers and callbacks
    logger = get_logger(args)
    log_dir = getattr(logger, 'log_dir', False) or logger.experiment.dir
    
    #stop_on_min_lr_cb = StopOnMinLR(args.min_lr)
    #lr_monitor = LearningRateMonitor('epoch')
    early_stopping = EarlyStopping('val_auc', patience = 20, mode = "max")

    checkpoint_cb = ModelCheckpoint(
        dirpath=log_dir,
        monitor='val_auc',
        mode='max',
        verbose=True
    )

    trainer = pl.Trainer(
        gpus=gpu,
        logger=logger,
        log_every_n_steps=5,
        max_epochs=args.max_epochs,
        callbacks=[early_stopping, checkpoint_cb]
    )


    trainer.fit(model, datamodule=dataset)
    checkpoint_path = checkpoint_cb.best_model_path
    trainer_test = pl.Trainer(logger = False)
    model = model_cls.load_from_checkpoint(checkpoint_path)
    val_results = trainer_test.test(model, test_dataloaders = dataset.val_dataloader())[0]
    val_results = {name.replace("test", "best_val"):value for name, value in val_results.items()}
    test_results = trainer_test.test(model,test_dataloaders = dataset.test_dataloader())[0]
    for name, value in {**val_results, **test_results}.items():
        logger.experiment.summary[name] = value
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--model', type=str, choices=MODEL_MAP.keys())
    parser.add_argument('--dataset', type=str,
                        choices=DATA_MAP.keys())
    parser.add_argument('--max_epochs', type=int, default=1000)
    parser.add_argument('--gpu', default=None, type=str)

    partial_args, _ = parser.parse_known_args()
    
    model_cls = MODEL_MAP[partial_args.model]
    dataset_cls = DATA_MAP[partial_args.dataset]

    parser = model_cls.add_model_specific_args(parser)
    parser = dataset_cls.add_dataset_specific_args(parser)
    args = parser.parse_args()

    main(model_cls, dataset_cls, args)
