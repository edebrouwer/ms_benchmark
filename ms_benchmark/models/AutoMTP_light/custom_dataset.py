import numpy as np
import random
from torch.utils.data import Dataset

class CustomDataset(Dataset):

    def __init__(self, targets, num_rows, num_targets, instance_features=None, target_features=None, dyadic_features=None, class_weights_per_target=None, embedding=False, instance_input_is_id=False, target_input_is_id=False):
        self.instance_features = instance_features
        self.target_features = target_features
        self.dyadic_features = dyadic_features
        self.targets = targets
        self.embedding = embedding
        self.num_rows = num_rows
        self.num_targets = num_targets
        
        self.instance_input_is_id = instance_input_is_id
        self.target_input_is_id = target_input_is_id
        
        self.are_targets_scaled = False if len(targets[0])==3 else True
    
        print('num_targets: '+str(self.num_targets))

        self.class_weights_per_target = class_weights_per_target

    def __len__(self):
        total_samples = 0
        return len(self.targets)

    def __getitem__(self, idx):
        dict_to_return = {}
        scaled_target_value = np.nan
        
        if self.are_targets_scaled:
            r_index, c_index, target_value, scaled_target_value = self.targets[idx]
            dict_to_return['scaled_target_value'] = scaled_target_value
        else:
            r_index, c_index, target_value = self.targets[idx]
        
        dict_to_return['row_index'] = r_index
        dict_to_return['column_index'] = c_index
        dict_to_return['target_value'] = int(target_value)
        
        sample_weight = 1.0
        if self.class_weights_per_target is not None:
            sample_weight = self.class_weights_per_target[c_index][int(target_value)]
        dict_to_return['sample_weight'] = sample_weight

        if self.embedding:
            dict_to_return['instance_features'] = r_index
            dict_to_return['target_features'] = c_index

        else:
            # check if features for instances are available. If not generate one-hot encoded vectors
            if self.instance_features is None:
                instance_features_vector = np.zeros(self.num_rows)
                instance_features_vector[r_index] = 1
            else:
                if isinstance(self.instance_features, list):
                    instance_features_vector = self.instance_features[r_index]
                else:
                    instance_features_vector = self.instance_features[r_index, :]

            # check if features for targets are available. If not generate one-hot encoded vectors
            if self.target_features is None:
                target_features_vector = np.zeros(self.num_targets)
                target_features_vector[c_index] = 1
            else:
                target_features_vector = self.target_features[c_index, :]                
            
            # check if the dyadic features are available. If not don't return anything at all
            dyadic_features_vector = None
            if self.dyadic_features is not None:
                dyadic_features_vector = self.dyadic_features[r_index, c_index, :]
            
            
            if self.instance_input_is_id:
                instance_features_vector = r_index
            if self.target_input_is_id:
                target_features_vector = c_index
            
            if dyadic_features_vector is not None:
                dict_to_return['dyadic_features'] = dyadic_features_vector

                
            dict_to_return['instance_features'] = instance_features_vector
            dict_to_return['target_features'] = target_features_vector
        
        return dict_to_return