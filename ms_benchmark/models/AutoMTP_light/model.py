import pytorch_lightning as pl
import argparse
import torch
from sklearn.metrics import roc_auc_score, average_precision_score, confusion_matrix, precision_recall_curve, auc
import numpy as np 
    

class DeepMTPModel(pl.LightningModule):
    
    def __init__(self,instance_features_dim, target_features_dim, nodes_per_layer_per_branch_1, nodes_per_layer_per_branch_2, layers_per_branch_1, layers_per_branch_2, embedding_size, dropout_p, batch_norm, weight_decay, **kwargs):
        super().__init__()
        self.save_hyperparameters()
        
        
        in_out_sizes_per_layer_for_first_branch = []
        in_out_sizes_per_layer_for_second_branch = []
        in_out_sizes_per_layer_for_combined_network = []
        
        self.first_branch_layers = torch.nn.ModuleList()                    
        self.second_branch_layers = torch.nn.ModuleList()
        
        
        # initialize the input layer of the two branches
        in_out_sizes_per_layer_for_first_branch.append((instance_features_dim, nodes_per_layer_per_branch_1))
        # calculate the the number of input and output nodes for all the intermediate layers of the first branch
        temp_num_nodes = nodes_per_layer_per_branch_1
        for i in range(layers_per_branch_1-1):
            in_out_sizes_per_layer_for_first_branch.append((temp_num_nodes, int(temp_num_nodes/2)))
            temp_num_nodes = int(temp_num_nodes/2)
        in_out_sizes_per_layer_for_first_branch[-1] = (in_out_sizes_per_layer_for_first_branch[-1][0], embedding_size)
        final_layer_size_for_first_branch = in_out_sizes_per_layer_for_first_branch[-1][1]     
        
        
        
        in_out_sizes_per_layer_for_second_branch.append((target_features_dim, nodes_per_layer_per_branch_2))
        # calculate the  number of input and output nodes for all the intermediate layers of the second branch
        temp_num_nodes = nodes_per_layer_per_branch_2
        for i in range(layers_per_branch_2-1):
            in_out_sizes_per_layer_for_second_branch.append((temp_num_nodes, int(temp_num_nodes/2)))
            temp_num_nodes = int(temp_num_nodes/2)
        in_out_sizes_per_layer_for_second_branch[-1] = (in_out_sizes_per_layer_for_second_branch[-1][0], embedding_size)
        final_layer_size_for_second_branch = in_out_sizes_per_layer_for_second_branch[-1][1]
        

        
        for idx, (input_size, output_size) in enumerate(in_out_sizes_per_layer_for_first_branch):
            self.first_branch_layers.append(torch.nn.Linear(input_size, output_size))

            if idx != (len(in_out_sizes_per_layer_for_first_branch)-1):
                self.first_branch_layers.append(torch.nn.LeakyReLU())
                if batch_norm:
                    # if self.config['batch_norm'] == 'yes':
                    self.first_branch_layers.append(torch.nn.BatchNorm1d(output_size))    

                    # if self.config['dropout_rate'] != 0:
                    self.first_branch_layers.append(torch.nn.Dropout(dropout_p))
                    
                    
        for idx, (input_size, output_size) in enumerate(in_out_sizes_per_layer_for_second_branch):
            self.second_branch_layers.append(torch.nn.Linear(input_size, output_size))
            
            if idx != (len(in_out_sizes_per_layer_for_second_branch)-1):
                self.second_branch_layers.append(torch.nn.LeakyReLU())
                if batch_norm:
                    # if self.config['batch_norm'] == 'yes':
                    self.second_branch_layers.append(torch.nn.BatchNorm1d(output_size))

                    # if self.config['dropout_rate'] != 0:
                    self.second_branch_layers.append(torch.nn.Dropout(dropout_p))
        

        self.act = torch.nn.Sigmoid()

        self.loss = torch.nn.BCELoss()
            
        self.accuracy = pl.metrics.Accuracy()
        self.accuracy_val = pl.metrics.Accuracy()
        self.accuracy_test = pl.metrics.Accuracy()
        self.weight_decay = weight_decay

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.hparams.lr, weight_decay = self.weight_decay)

        return optimizer

    def forward(self, x):
                
        x1 = x[0].float()
        x2 = x[1].float()

        # x1 = self.first_branch_layers(x1)
        for idx, _ in enumerate(range(len(self.first_branch_layers))):
            x1 = self.first_branch_layers[idx](x1)
        
        # x2 = self.second_branch_layers(x2)
        for idx, _ in enumerate(range(len(self.second_branch_layers))):
            x2 = self.second_branch_layers[idx](x2)
            
        x_comb = torch.unsqueeze((x1*x2).sum(1),1)
        x_comb = self.act(x_comb)

        return x_comb

    def training_step(self, batch, batch_idx):
        x1, x2, y = batch['instance_features'], batch['target_features'], torch.unsqueeze(batch['target_value'], 1)

        y_pred = self([x1, x2])
        loss = self.loss(y_pred,y.float())

        self.accuracy(y_pred,y)

        self.log("train_loss", loss, on_step=True, on_epoch=True)
        self.log("train_acc", self.accuracy, on_step=True, on_epoch=True)
        # return loss
        return {"loss":loss, "y_pred":y_pred.detach(), "y_true":y}

    def training_epoch_end(self,data):
        y_full = torch.cat([el["y_true"] for el in data]).cpu().numpy()
        y_pred_full = torch.cat([el["y_pred"] for el in data]).cpu().numpy()
        # print('validating on '+str(len(y_full))+' samples')
        # print('train: '+str(confusion_matrix(y_full, np.where(y_pred_full > 0.5, 1, 0))))
        auc_score = roc_auc_score(y_full, y_pred_full)
        precision, recall, thresholds = precision_recall_curve(y_full, y_pred_full)
        aupr_score = auc(recall, precision)
        self.log("train_auc", auc_score)
        self.log("train_aupr", aupr_score)
        
    def validation_step(self, batch, batch_idx):
        x1, x2, y = batch['instance_features'], batch['target_features'], torch.unsqueeze(batch['target_value'], 1)
        y_pred = self([x1, x2])
        loss = self.loss(y_pred,y.float())

        self.accuracy_val(y_pred, y)

        self.log("val_loss", loss, on_epoch=True)
        self.log("val_acc", self.accuracy_val, on_epoch=True)

        return {"y":y, "y_pred":y_pred}

    def validation_epoch_end(self,data):
        y_full = torch.cat([el["y"] for el in data]).cpu().numpy()
        y_pred_full = torch.cat([el["y_pred"] for el in data]).cpu().numpy()
        # print('validating on '+str(len(y_full))+' samples')
        # print('val: '+str(confusion_matrix(y_full, np.where(y_pred_full > 0.5, 1, 0))))
        auc_score = roc_auc_score(y_full, y_pred_full)
        precision, recall, thresholds = precision_recall_curve(y_full, y_pred_full)
        aupr_score = auc(recall, precision)
        self.log("val_auc", auc_score)
        self.log("val_aupr", aupr_score)

    def test_step(self, batch, batch_idx):
        x1, x2, y = batch['instance_features'], batch['target_features'], torch.unsqueeze(batch['target_value'], 1)
        y_pred = self([x1, x2])
        loss = self.loss(y_pred,y.float())
        self.accuracy_test(y_pred, y)
        
        self.log("test_loss", loss, on_epoch=True)
        self.log("test_acc", self.accuracy_test, on_epoch=True)
        return {"y":y, "y_pred":y_pred}

    def test_epoch_end(self,data):
        y_full = torch.cat([el["y"] for el in data]).cpu().numpy()
        y_pred_full = torch.cat([el["y_pred"] for el in data]).cpu().numpy()
        print('testing on '+str(len(y_full))+' samples')
        print('test: '+str(confusion_matrix(y_full, np.where(y_pred_full > 0.5, 1, 0))))
        auc_score = roc_auc_score(y_full, y_pred_full)
        precision, recall, thresholds = precision_recall_curve(y_full, y_pred_full)
        aupr_score = auc(recall, precision)
        self.log("test_auc", auc_score)
        self.log("test_aupr", aupr_score)

    def predict_step(self, batch, batch_idx):
        x1, x2, y = batch['instance_features'], batch['target_features'], torch.unsqueeze(batch['target_value'], 1)        
        y_pred = self([x1, x2])

        return {"y":y, "y_pred":y_pred, "x":x1, "x1":x1, "x2":x2}


    @classmethod
    def add_model_specific_args(cls, parent):
        parser = argparse.ArgumentParser(parents=[parent])
        
        parser.add_argument("--nodes_per_layer_per_branch_1", type=int, default=64)
        parser.add_argument("--nodes_per_layer_per_branch_2", type=int, default=64)
        parser.add_argument("--layers_per_branch_1", type=int, default=1)
        parser.add_argument("--layers_per_branch_2", type=int, default=1)
        parser.add_argument("--embedding_size", type=int, default=64)
        parser.add_argument("--layers_per_final_mlp", type=int, default=2)

        parser.add_argument("--lr", type=float, default=0.0001)
        parser.add_argument('--dropout_p',type=float,default=0.1)
        parser.add_argument('--batch_norm',type=bool,default=False)
        parser.add_argument('--weight_decay',type=float,default=0.)
        return parser