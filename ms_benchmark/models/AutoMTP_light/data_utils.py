from ms_benchmark import DATA_DIR
import torch
import pytorch_lightning as pl
import pandas as pd
import numpy as np
from ms_benchmark.utils import str2bool

from ms_benchmark.data_utils import normalize_df, get_msbase_static_dataset, get_msbase_dynamic_dataset

from torch.utils.data import DataLoader

from .custom_dataset import CustomDataset

DATATYPE_DICT = ("EP","MSBase2020")

class MultiTargetDataset(pl.LightningDataModule):
    def __init__(self, datatype, fold, batch_size, no_dmt=False, num_workers = 4, data_version="dynamic", minimal_example= False, num_visits = 3,**kwargs):
        super().__init__()
        self.num_workers = num_workers
        self.datatype = datatype #EP or MSBase2020

        self.fold = fold
        self.batch_size = batch_size
        self.input_dim = 0
        
        self.instance_features_dim = 0
        self.target_features_dim = 0
        self.class_weights_per_target = None
        self.no_dmt = no_dmt
        self.data_version = data_version

        self.minimal_example = minimal_example
        self.num_visits = num_visits


    def prepare_data(self, return_data = False, shuffle_pos = -1):
        # df_full = pd.read_csv(cleaned_dir+"df_processed.csv").sort_values(by=["slice_id","Time"])
        # df_full = pd.read_csv(DATA_DIR+"/"+self.datatype+"/Cleaned/df_processed.csv").sort_values(by=["slice_id","Time"])

        if self.data_version == 'static':
            df_processed, cov_cols, continuous_cols = get_msbase_static_dataset(no_dmt=self.no_dmt, minimal_example=self.minimal_example, num_visits=self.num_visits, include_country=True, include_clinic=False)
            print('Finished preparing the static dataset...')
            
        elif self.data_version == 'dynamic':
            df_processed_static, cov_cols_static, continuous_cols_static = get_msbase_static_dataset(no_dmt = self.no_dmt, num_visits = self.num_visits, include_country=True, include_clinic=False)
            print('Finished preparing the static dataset...')

            df_processed_dynamic, cov_cols_dynamic, continuous_cols_dynamic = get_msbase_dynamic_dataset(num_visits = self.num_visits)
            print('Finished preparing the dynamic dataset...')

            df_processed = pd.merge(df_processed_static, df_processed_dynamic, on = ["PATIENT_ID","slice_id","label"], how = "outer")
            
            assert(df_processed.slice_id.nunique()==df_processed.shape[0])
            assert(df_processed.shape[0]==df_processed_static.shape[0])
            assert(df_processed.shape[0]==df_processed_dynamic.shape[0])

            
            cov_cols = cov_cols_static + cov_cols_dynamic
            continuous_cols = continuous_cols_static + continuous_cols_dynamic
            
        self.label_train = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/train_idx.csv")
        self.label_val = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/val_idx.csv")
        self.label_test = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/test_idx.csv")
        
        df_train = df_processed.loc[df_processed.slice_id.isin(self.label_train.slice_id.unique())].reset_index().sort_values(by="slice_id")
        df_val = df_processed.loc[df_processed.slice_id.isin(self.label_val.slice_id.unique())].reset_index().sort_values(by="slice_id")
        df_test = df_processed.loc[df_processed.slice_id.isin(self.label_test.slice_id.unique())].reset_index().sort_values(by="slice_id")
    
        self.df_train = df_train
        self.df_val = df_val
        self.df_test = df_test
    
        df_train, df_val, df_test = normalize_df(df_train,df_val, df_test, continuous_cols)

        # df_processed[cov_cols].to_csv('multi-task_features_per_patient.csv', index=True)
        # df_train[['PATIENT_ID', 'slice_id', 'country', 'country_i', 'label']].to_csv(output_dir+'train_interactions_'+str(fold_id)+'.csv', index=True)
        # df_val[['PATIENT_ID', 'slice_id', 'country', 'country_i', 'label']].to_csv(output_dir+'val_interactions_'+str(fold_id)+'.csv', index=True)
        # df_test[['PATIENT_ID', 'slice_id', 'country', 'country_i', 'label']].to_csv(output_dir+'test_interactions_'+str(fold_id)+'.csv', index=True)
        cov_cols.append('PATIENT_ID')
        cov_cols.append('slice_id')
        
        # remove country from the columns
        cov_cols = [col for col in cov_cols if ((col != 'clinic') and (col != 'country'))]
        
        features = df_processed[cov_cols]
        # initial_train = df_train[['PATIENT_ID', 'slice_id', 'country', 'country_i', 'label']]
        # initial_val = df_val[['PATIENT_ID', 'slice_id', 'country', 'country_i', 'label']]
        # initial_test = df_test[['PATIENT_ID', 'slice_id', 'country', 'country_i', 'label']]
        
        initial_train = df_train[['PATIENT_ID', 'slice_id', 'label']]
        initial_val = df_val[['PATIENT_ID', 'slice_id', 'label']]
        initial_test = df_test[['PATIENT_ID', 'slice_id', 'label']]
        
        train = pd.merge(initial_train, features, on=['PATIENT_ID','slice_id'], how='left')
        val = pd.merge(initial_val, features, on=['PATIENT_ID','slice_id'], how='left')
        test = pd.merge(initial_test, features, on=['PATIENT_ID','slice_id'], how='left')
        
        self.num_targets = len(set(train['country_i'].unique()).union(set(test['country_i'].unique())).union(set(val['country_i'].unique())))
        
        self.target_features_dim = self.num_targets

        train['patient_slice_combo'] = train['PATIENT_ID'] +'-'+train['slice_id'].astype(str)
        val['patient_slice_combo'] = val['PATIENT_ID'] +'-'+val['slice_id'].astype(str)
        test['patient_slice_combo'] = test['PATIENT_ID'] +'-'+test['slice_id'].astype(str)

        train['instance_i'] = train['patient_slice_combo'].astype('category').cat.codes
        val['instance_i'] = val['patient_slice_combo'].astype('category').cat.codes
        test['instance_i'] = test['patient_slice_combo'].astype('category').cat.codes
        
        features_names = features.columns[:-2]
        
        if 'country_i' in features.columns:
            features = features.drop(['country_i'], axis=1)
        if 'clinic_i' in features.columns:
            features = features.drop(['clinic_i'], axis=1)
        
        self.X_train = train[features_names].values
        self.X_test = test[features_names].values
        self.X_val = val[features_names].values
        
        print('The size of the training dataset: '+str(self.X_train.shape))
        print('The size of the test dataset: '+str(self.X_test.shape))
        print('The size of the validation dataset: '+str(self.X_val.shape))

        self.instance_features_dim = self.X_train.shape[1]
        
        train['label'] = train['label'].astype(int) 
        test['label'] = test['label'].astype(int) 
        val['label'] = val['label'].astype(int)

        
        self.train_samples_array = [tuple(x) for x in train[['instance_i', 'country_i', 'label']].values]
        self.val_samples_array = [tuple(x) for x in val[['instance_i', 'country_i', 'label']].values]
        self.test_samples_array = [tuple(x) for x in test[['instance_i', 'country_i', 'label']].values]
        '''
        self.class_weights_per_target = {}
        for s in self.train_samples_array:
            if s[1] not in self.class_weights_per_target:
                self.class_weights_per_target[s[1]] = {0:0, 1:0}
            self.class_weights_per_target[s[1]][s[2]] += 1
        
        for s in set(train['country_i'].unique()).union(set(test['country_i'].unique())).union(set(val['country_i'].unique())):
            if s not in self.class_weights_per_target:
                self.class_weights_per_target[s] = {0:1, 1:1}
        print('Class weights per target: '+str(self.class_weights_per_target))
        '''
        
    def get_df(self):
        return self.df_train, self.df_val, self.df_test
        
    def train_dataloader(self):
        train_dataset = CustomDataset(self.train_samples_array, self.X_train.shape[0], self.num_targets, instance_features=self.X_train, class_weights_per_target=self.class_weights_per_target)

        return DataLoader(
            train_dataset,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
            drop_last=True,
            pin_memory=True
        )

    def val_dataloader(self):
        val_dataset = CustomDataset(self.val_samples_array, self.X_val.shape[0], self.num_targets, instance_features=self.X_val, class_weights_per_target=self.class_weights_per_target)    

        return DataLoader(
            val_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            drop_last=False,
            pin_memory=True
        )

    def test_dataloader(self):
        test_dataset = CustomDataset(self.test_samples_array, self.X_test.shape[0], self.num_targets, instance_features=self.X_test, class_weights_per_target=self.class_weights_per_target)    
        return DataLoader(
            test_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            drop_last=False,
            pin_memory=True
        )

    @classmethod
    def add_dataset_specific_args(cls, parent):
        import argparse
        parser = argparse.ArgumentParser(parents=[parent], add_help=False)
        parser.add_argument('--fold', type=int, default=0)
        parser.add_argument('--batch_size', type=int, default=512)
        parser.add_argument('--datatype', type=str, default="MSBase2020", choices = DATATYPE_DICT)
        parser.add_argument('--no_dmt',type=str2bool, default = False, help =  "dmts are removed from the variables")
        parser.add_argument('--data_version', type=str, default="static")
        
        parser.add_argument('--minimal_example',type=str2bool, default = False, help =  "if true, only uses the edss at t=0")
        parser.add_argument('--num_visits', type=int, default=3, help = "Minimum number of visits in the observation window")
        
        return parser