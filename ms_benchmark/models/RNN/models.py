import pytorch_lightning as pl
import argparse
import torch
import torch.nn as nn
from sklearn.metrics import roc_auc_score, average_precision_score
import time
from torch.nn import TransformerEncoder, TransformerEncoderLayer
import math
from ms_benchmark.utils import str2bool
import numpy as np

class RNNModel(pl.LightningModule):
    def __init__(self,input_dim_fixed, input_dim_longitudinal, d_hidden, dropout_p, n_layers, weight_decay, prop_pos, weighted_loss = True, no_static_data = False, **kwargs):
        super().__init__()
        self.save_hyperparameters()

        self.RNN = nn.GRU(input_size = input_dim_longitudinal, hidden_size = d_hidden, num_layers = n_layers, dropout = dropout_p ) 
        self.classifier_pre = nn.Sequential(torch.nn.Linear(input_dim_fixed,d_hidden),nn.ReLU(), torch.nn.Dropout(dropout_p), nn.Linear(d_hidden,d_hidden))
        self.classifier_h0 = nn.Sequential(torch.nn.Linear(input_dim_fixed,d_hidden),nn.ReLU(), torch.nn.Dropout(dropout_p), nn.Linear(d_hidden,d_hidden * n_layers))
        if no_static_data:
            self.classifier_end = nn.Sequential(torch.nn.Linear(d_hidden,d_hidden),nn.Tanh(), nn.Linear(d_hidden,2))
        else:
            self.classifier_end = nn.Sequential(torch.nn.Linear(d_hidden*2,d_hidden),nn.Tanh(), nn.Linear(d_hidden,2))

        weights = torch.ones(2)
        if weighted_loss:
            weights[1] = (1-prop_pos)/prop_pos
        
        self.loss = torch.nn.CrossEntropyLoss(weight = weights )
            
        self.accuracy = pl.metrics.Accuracy()
        self.accuracy_val = pl.metrics.Accuracy()
        self.accuracy_test = pl.metrics.Accuracy()

        self.weight_decay = weight_decay

        self.n_layers = n_layers

        self.no_static_data = no_static_data
        self.d_hidden = d_hidden

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.hparams.lr, weight_decay = self.weight_decay)

        return optimizer

    def forward(self,data):

        longitudinal_X = data["longitudinal_X"]
        fixed_X = data["fixed_X"]

        if self.no_static_data:
            h0 = torch.zeros((fixed_X.shape[0],self.d_hidden*self.n_layers),device = fixed_X.device)
        else:
            h0 = self.classifier_h0(fixed_X) 
        
        h0 = torch.stack(torch.chunk(h0,self.n_layers,dim = -1))
        output, h_n = self.RNN(longitudinal_X, h0)
        
        if self.no_static_data:
            out_cat = h_n[-1]
        else:
            out_cat = torch.cat((h_n[-1], self.classifier_pre(fixed_X)),-1)

        y_hat = self.classifier_end(out_cat)
        
        return y_hat

    def training_step(self, batch, batch_idx):
        y_pred = self(batch)
        y = batch["label"]
        loss = self.loss(y_pred,y)

        self.accuracy(y_pred,y)

        self.log("train_loss", loss, on_step=True, on_epoch=True)
        self.log("train_acc", self.accuracy, on_step=True, on_epoch=True)
        return loss

    def validation_step(self, batch, batch_idx):
        
        y_pred = self(batch)
        y = batch["label"]
        loss = self.loss(y_pred,y)

        self.accuracy_val(y_pred, y)

        self.log("val_loss", loss, on_epoch=True)

        self.log("val_acc", self.accuracy_val, on_epoch=True)

        return {"y":y, "y_pred":y_pred}

    def validation_epoch_end(self,data):
        y_full = torch.cat([el["y"] for el in data]).cpu().numpy()
        y_pred_full = torch.softmax(torch.cat([el["y_pred"] for el in data]),1).cpu().numpy()
        auc = roc_auc_score(y_full, y_pred_full[:,1])
        self.log("val_auc", auc)

    def test_step(self, batch, batch_idx):
        y_pred = self(batch)
        y = batch["label"]
        loss = self.loss(y_pred,y)

        self.accuracy_test(y_pred, y)
        
        self.log("test_loss", loss, on_epoch=True)
        self.log("test_acc", self.accuracy_test, on_epoch=True)
        return {"y":y, "y_pred":y_pred}

    def test_epoch_end(self,data):
        y_full = torch.cat([el["y"] for el in data]).cpu().numpy()
        y_pred_full = torch.softmax(torch.cat([el["y_pred"] for el in data]),1).cpu().numpy()
        auc = roc_auc_score(y_full, y_pred_full[:,1])
        self.log("test_auc", auc)

    def predict_step(self,batch, batch_idx):
        y_pred = self(batch)
        y = batch["label"]
        sorted_idx = batch["sorted_idx"]
        resort_idx = np.argsort(sorted_idx)
        return {"y":y[resort_idx], "y_pred":y_pred[resort_idx]}
    @classmethod
    def add_model_specific_args(cls, parent):
        parser = argparse.ArgumentParser(parents=[parent])
        parser.add_argument("--d_hidden", type=int, default=64)
        parser.add_argument("--lr", type=float, default=0.001)
        parser.add_argument('--n_layers', type=int, default=2)
        parser.add_argument('--dropout_p',type=float,default=0.)
        parser.add_argument('--weight_decay',type=float,default=0.)
        parser.add_argument('--weighted_loss',type=str2bool,default=True)
        return parser




