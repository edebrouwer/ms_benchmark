from ms_benchmark import DATA_DIR
import torch
import pytorch_lightning as pl
import pandas as pd
import numpy as np
from ms_benchmark.utils import str2bool
from torch.nn.utils.rnn import pad_sequence, pack_padded_sequence
import time
from ms_benchmark.data_utils import normalize_df, normalize_longitudinal_df, get_msbase_static_dataset, get_msbase_dynamic_dataset, get_msbase_longitudinal_dataset

from torch.utils.data import DataLoader, Dataset

DATATYPE_DICT = ("EP","MSBase2020")

class BaseLongitudinalDataset(Dataset):
    def __init__(self,df_fixed, df_longitudinal, fixed_cols, longitudinal_cols, mask_cols_longitudinal, no_static_data = False, minimal_example = False):
        super().__init__()

        self.df_fixed = df_fixed
        self.df_longitudinal = df_longitudinal
        self.mask_cols_longitudinal = mask_cols_longitudinal

        self.fixed_cols = fixed_cols
        self.longitudinal_cols = longitudinal_cols

        assert self.df_fixed.slice_id.nunique() == self.df_longitudinal.slice_id.nunique()
        slice_id_dict = dict(zip(self.df_fixed.slice_id.unique(),np.arange(self.df_fixed.slice_id.nunique())))
        self.df_fixed["slice_id"] = self.df_fixed["slice_id"].map(slice_id_dict)
        self.df_longitudinal["slice_id"] = self.df_longitudinal["slice_id"].map(slice_id_dict)

        self.minimal_example = minimal_example
        #self.minimal_example = False
        if self.minimal_example:
            self.df_longitudinal = self.df_longitudinal.groupby("slice_id").nth([-6,-5,-4,-3,-2,-1])
        else:
            self.df_longitudinal.set_index("slice_id",inplace = True)
       
        self.df_fixed.set_index("slice_id",inplace = True)
        self.df_fixed.sort_index(inplace = True)

        self.prop_pos = self.df_fixed["label"].mean()

        self.tensors = (torch.Tensor(self.df_fixed[fixed_cols].values),(torch.Tensor(self.df_fixed.label.values.astype(int))))

        self.no_static_data = no_static_data
    def __getitem__(self,idx):
        sub_df = self.df_fixed.loc[idx]
        sub_df_longitudinal = self.df_longitudinal.loc[idx]

        if self.no_static_data:
            fixed_X = np.zeros(len(self.fixed_cols))
        else:
            fixed_X = sub_df[self.fixed_cols].values
        
        if self.minimal_example:
            longitudinal_X = sub_df_longitudinal[self.longitudinal_cols].values.astype(np.float)#[None,...]
            longitudinal_mask = sub_df_longitudinal[self.mask_cols_longitudinal].values.astype(np.float)#[None,...]
            times = sub_df_longitudinal[["Time"]].values.astype(np.float)#[None,...]
        else:
            longitudinal_X = sub_df_longitudinal[self.longitudinal_cols].values
            longitudinal_mask = sub_df_longitudinal[self.mask_cols_longitudinal].values
            times = sub_df_longitudinal["Time"].values

        label = sub_df["label"].astype(int)
        return {"fixed_X" : torch.Tensor(fixed_X.astype(np.float32)), "longitudinal_X": longitudinal_X, "longitudinal_mask": longitudinal_mask,"longitudinal_times": times ,"label" : torch.LongTensor([label])} 

    def __len__(self):
        return self.df_fixed.index.nunique()

def longitudinal_fixed_collate_fn(batch):
    
    longitudinal_X = [torch.cat((torch.Tensor(b["longitudinal_X"]),torch.Tensor(b["longitudinal_mask"])),1)  for b in batch]
    lengths = [l.shape[0] for l in longitudinal_X]
    sorted_idx = np.array(sorted(range(len(lengths)), key = lambda k : -lengths[k])).astype(int)

    longitudinal_X = pad_sequence(longitudinal_X ) #Shape is T x B x d
    pack_padded_sequence(longitudinal_X[:,sorted_idx], np.array(lengths)[sorted_idx])
    
    fixed_X = torch.stack([b["fixed_X"] for b in batch],0)[sorted_idx] # B x d
    label = torch.stack([b["label"] for b in batch],0)[sorted_idx] #Shape is B x 1
    times = pad_sequence([torch.Tensor(b["longitudinal_times"]) for b in batch])[:,sorted_idx] #Shape is T x B
    return {"longitudinal_X" : longitudinal_X, "label" : label[:,0], "times" : times, "fixed_X" : fixed_X, "sorted_idx": sorted_idx}

class LongitudinalDataset(pl.LightningDataModule):
    def __init__(self, datatype, fold, batch_size, no_dmt, num_workers = 10, use_delta_t = True, no_static_data = False, scramble_time = False, minimal_example = False,  num_visits = 3, **kwargs):
        super().__init__()
        self.num_workers = num_workers
        self.datatype = datatype #EP or MSBase2020

        self.fold = fold
        self.batch_size = batch_size
        self.input_dim = 0

        self.no_dmt = no_dmt

        self.cols = ["EDSS","MSCOURSE","DMT_current","cumulative_relapse"]

        self.use_delta_t = use_delta_t # uses delta as a longitudinal feature
        self.no_static_data = no_static_data
        self.scramble_time = scramble_time
        self.minimal_example = minimal_example

        self.num_visits = num_visits

    def prepare_data(self):

        df_processed_static, cov_cols_static, continuous_cols_static = get_msbase_static_dataset(no_dmt = self.no_dmt, num_visits = self.num_visits)
        
        df_processed_dynamic, cov_cols_dynamic, continuous_cols_dynamic = get_msbase_dynamic_dataset(num_visits = self.num_visits)
        
        print("Loading longitudinal data...")
        df_processed_longitudinal, cov_cols_longitudinal, continuous_cols_longitudinal, mask_cols_longitudinal = get_msbase_longitudinal_dataset(self.minimal_example, num_visits = self.num_visits)
        if self.scramble_time:
            df_processed_longitudinal = df_processed_longitudinal.groupby("slice_id").sample(frac = 1)
        print("Done.")
       
        df_processed_fixed = pd.merge(df_processed_static, df_processed_dynamic, on = ["PATIENT_ID","slice_id","label"], how = "outer")

        assert(df_processed_fixed.slice_id.nunique()==df_processed_fixed.shape[0])
        assert(df_processed_fixed.shape[0]==df_processed_static.shape[0])
        assert(df_processed_fixed.shape[0]==df_processed_dynamic.shape[0])

        cov_cols_fixed = cov_cols_static + cov_cols_dynamic
        continuous_cols_fixed = continuous_cols_static + continuous_cols_dynamic

        self.label_train = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/train_idx.csv")
        self.label_val = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/val_idx.csv")
        self.label_test = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/test_idx.csv")

        self.df_fixed_train = df_processed_fixed.loc[df_processed_fixed.slice_id.isin(self.label_train.slice_id.unique())].reset_index().sort_values(by="slice_id")
        self.df_fixed_val = df_processed_fixed.loc[df_processed_fixed.slice_id.isin(self.label_val.slice_id.unique())].reset_index().sort_values(by="slice_id")
        self.df_fixed_test = df_processed_fixed.loc[df_processed_fixed.slice_id.isin(self.label_test.slice_id.unique())].reset_index().sort_values(by="slice_id")

        df_longitudinal_train = df_processed_longitudinal.loc[df_processed_longitudinal.slice_id.isin(self.label_train.slice_id.unique())].reset_index().sort_values(by=["slice_id", "Time"])
        df_longitudinal_val = df_processed_longitudinal.loc[df_processed_longitudinal.slice_id.isin(self.label_val.slice_id.unique())].reset_index().sort_values(by=["slice_id", "Time"])
        df_longitudinal_test = df_processed_longitudinal.loc[df_processed_longitudinal.slice_id.isin(self.label_test.slice_id.unique())].reset_index().sort_values(by=["slice_id", "Time"])

        df_fixed_train, df_fixed_val, df_fixed_test = normalize_df(self.df_fixed_train,self.df_fixed_val, self.df_fixed_test, continuous_cols_fixed)
        
        continuous_cols_with_mask = [c for c in continuous_cols_longitudinal if c+"_mask" in mask_cols_longitudinal]
        continuous_cols_without_mask = [c for c in continuous_cols_longitudinal if c+"_mask" not in mask_cols_longitudinal]
        
        df_longitudinal_train, df_longitudinal_val, df_longitudinal_test = normalize_longitudinal_df(df_longitudinal_train,df_longitudinal_val, df_longitudinal_test, continuous_cols_with_mask, masked = True)
        df_longitudinal_train, df_longitudinal_val, df_longitudinal_test = normalize_longitudinal_df(df_longitudinal_train,df_longitudinal_val, df_longitudinal_test, continuous_cols_without_mask, masked = False)

        if self.use_delta_t:
            cov_cols_longitudinal.append("Delta_Time")
        
        self.data_train = BaseLongitudinalDataset(df_fixed_train, df_longitudinal_train, cov_cols_fixed, cov_cols_longitudinal, mask_cols_longitudinal, no_static_data = self.no_static_data, minimal_example = self.minimal_example)
        self.data_val = BaseLongitudinalDataset(df_fixed_val, df_longitudinal_val, cov_cols_fixed, cov_cols_longitudinal, mask_cols_longitudinal,  no_static_data = self.no_static_data, minimal_example = self.minimal_example)
        self.data_test = BaseLongitudinalDataset(df_fixed_test, df_longitudinal_test, cov_cols_fixed, cov_cols_longitudinal, mask_cols_longitudinal,  no_static_data = self.no_static_data, minimal_example = self.minimal_example)

        self.prop_pos = self.data_train.prop_pos
       
        self.input_dim_fixed = len(cov_cols_fixed)
        self.input_dim_longitudinal = len(cov_cols_longitudinal) + len(mask_cols_longitudinal)

    def get_df(self):
        return self.df_fixed_train, self.df_fixed_val, self.df_fixed_test
    
    def train_dataloader(self):
        return DataLoader(
            self.data_train,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
            drop_last=True,
            pin_memory=True,
            collate_fn = longitudinal_fixed_collate_fn
        )

    def val_dataloader(self):
        return DataLoader(
            self.data_val,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            drop_last=False,
            pin_memory=True,
            collate_fn  = longitudinal_fixed_collate_fn
        )

    def test_dataloader(self):
        return DataLoader(
            self.data_test,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            drop_last=False,
            pin_memory=True,
            collate_fn = longitudinal_fixed_collate_fn
        )

    @classmethod
    def add_dataset_specific_args(cls, parent):
        import argparse
        parser = argparse.ArgumentParser(parents=[parent], add_help=False)
        parser.add_argument('--fold', type=int, default=0)
        parser.add_argument('--batch_size', type=int, default=512)
        parser.add_argument('--datatype', type=str, default="MSBase2020", choices = DATATYPE_DICT)
        parser.add_argument('--no_dmt',type=str2bool, default = False, help =  "dmts are removed from the (fixed) variables.")
        parser.add_argument('--scramble_time',type=str2bool, default = False, help =  "The Longitudinal data is scrambled.")
        parser.add_argument('--use_delta_t',type=str2bool, default = True, help =  "The Longitudinal data is scrambled.")
        parser.add_argument('--no_static_data',type=str2bool, default = False, help =  "The static data is set to all 0.")
        parser.add_argument('--minimal_example',type=str2bool, default = False, help =  "Only EDSS is used in the trajectory")
        parser.add_argument('--num_workers', type=int, default=10)
        parser.add_argument('--num_visits', type=int, default=3)
        return parser


