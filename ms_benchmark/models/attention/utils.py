import math
import torch
import numpy as np

class PositionalEncoding():
    """Apply positional encoding to instances."""

    def __init__(self, min_timescale, max_timescale, n_channels):
        """PositionalEncoding.
        Args:
            min_timescale: minimal scale of values
            max_timescale: maximal scale of values
            n_channels: number of channels to use to encode position
        """
        self.min_timescale = min_timescale
        self.max_timescale = max_timescale
        self.n_channels = n_channels

        self._num_timescales = self.n_channels // 2
        self._inv_timescales = self._compute_inv_timescales()

    def _compute_inv_timescales(self):
        log_timescale_increment = (
            math.log(float(self.max_timescale) / float(self.min_timescale))
            / (float(self._num_timescales) - 1)
        )
        inv_timescales = (
            self.min_timescale
            * np.exp(
                np.arange(self._num_timescales)
                * -log_timescale_increment
            )
        )
        return torch.Tensor(inv_timescales)

    def __call__(self, times):
        """Apply positional encoding to instances."""
        # instance = instance.copy()  # We only want a shallow copy
        positions = times
        scaled_time = (
            positions[...,None] *
            self._inv_timescales[None, :].to(times.device)
        )
        signal = torch.cat(
            (torch.sin(scaled_time), torch.cos(scaled_time)),
            axis=-1
        )
        return signal
        #positional_encoding = np.reshape(signal, (-1, self.n_channels))
        #instance[self.positions_key+'_embedded'] = positional_encoding
        #return instance
