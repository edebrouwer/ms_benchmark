import pytorch_lightning as pl
import argparse
import torch
import torch.nn as nn
from sklearn.metrics import roc_auc_score, average_precision_score
import time
from torch.nn import TransformerEncoder, TransformerEncoderLayer
import math

class ClinicalEmbedder(nn.Module):
    def __init__(self, d_embed, input_dim_longitudinal, d_hidden_embed,n_layers_embed, **kwargs):
        super().__init__() 
        self.mod_list = nn.ModuleList([nn.Linear(input_dim_longitudinal,d_hidden_embed), nn.ReLU()])
        for _ in range(n_layers_embed):
            self.mod_list.append(nn.Linear(d_hidden_embed,d_hidden_embed))
            self.mod_list.append(nn.ReLU())
        self.mod_list.append(nn.Linear(d_hidden_embed,d_embed))

    def forward(self,x):
        for mod in self.mod_list:
            x = mod(x)
        return x

class TransformerModel(nn.Module):

    def __init__(self, d_embed, nhead, d_hidden, n_layers, dropout_p, input_dim_fixed, input_dim_longitudinal, max_pos_encoding, **kwargs):
        """
        d_embed = dimension of the embeddings
        n_head = number of heads to use
        d_hidden = dimension of the hidden vectors
        nlayers = number of encoder layers
        """
        super(TransformerModel, self).__init__()
        
        self.pos_encoder = PositionalEncoding(d_embed, dropout_p, max_pos_encoding)

        encoder_layers = TransformerEncoderLayer(d_embed, nhead, d_hidden, dropout_p)
        self.transformer_encoder = TransformerEncoder(encoder_layers, n_layers)
        #self.encoder = nn.Embedding(ntoken, ninp)
        self.embedder = ClinicalEmbedder(d_embed, input_dim_longitudinal, **kwargs)
        self.fixed_embedder = ClinicalEmbedder(d_embed, input_dim_fixed, **kwargs)
        self.d_hidden = d_hidden

        self.d_embed = d_embed
        
        #self.decoder = nn.Linear(d_embed, 2)

        #self.init_weights()

    def generate_square_subsequent_mask(self, sz):
        mask = (torch.triu(torch.ones(sz, sz)) == 1).transpose(0, 1)
        mask = mask.float().masked_fill(mask == 0, float('-inf')).masked_fill(mask == 1, float(0.0))
        return mask

    #def init_weights(self):
    #    initrange = 0.1
    #    self.encoder.weight.data.uniform_(-initrange, initrange)
    #    self.decoder.bias.data.zero_()
    #    self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, longitudinal_X, attention_mask, times, fixed_X):

        src = self.embedder(longitudinal_X) * math.sqrt(self.d_embed)
        
        src = self.pos_encoder(src, times)

        fixed_token = self.fixed_embedder(fixed_X) * math.sqrt(self.d_embed)
        
        src_sequence = torch.cat((fixed_token[None,...],src),0)
        attention_mask_sequence = torch.cat((torch.ones(1,src.shape[1], device =attention_mask.device),attention_mask),0)

        output = self.transformer_encoder(src_sequence, src_key_padding_mask = (~(attention_mask_sequence.T.bool())))

        return output # Shape : T x B x D

class PositionalEncoding(nn.Module):

    def __init__(self, d_model, dropout_p, max_pos_encoding):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout_p)

        pe = torch.zeros(max_pos_encoding, d_model)
        position = torch.arange(0, max_pos_encoding, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

        self.max_pos_encoding = max_pos_encoding

    def forward(self, x, times):
        x = x + self.pe[times.long()+self.max_pos_encoding-1, :][:,:,0,:]
        return self.dropout(x)

class AttentionModel(pl.LightningModule):
    def __init__(self,input_dim_fixed, input_dim_longitudinal, d_hidden, d_embed, nhead, dropout_p, n_layers, weight_decay, max_pos_encoding, **kwargs):
        super().__init__()
        self.save_hyperparameters()

        self.Transformer = TransformerModel(d_embed, nhead, d_hidden, n_layers, dropout_p, input_dim_fixed, input_dim_longitudinal, max_pos_encoding, **kwargs)
        self.classifier = nn.Sequential(torch.nn.Linear(d_embed,d_embed),nn.ReLU(), nn.Linear(d_embed,2))

        self.loss = torch.nn.CrossEntropyLoss()
            
        self.accuracy = pl.metrics.Accuracy()
        self.accuracy_val = pl.metrics.Accuracy()
        self.accuracy_test = pl.metrics.Accuracy()

        self.weight_decay = weight_decay

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.hparams.lr, weight_decay = self.weight_decay)

        return optimizer

    def forward(self,data):

        longitudinal_X = data["longitudinal_X"]
        attention_mask = data["attention_mask"]
        times = data["times"]
        fixed_X = data["fixed_X"]

        transformer_output = self.Transformer(longitudinal_X, attention_mask, times, fixed_X)

        y_hat = self.classifier(transformer_output[0,:,:])
        
        return y_hat

    def training_step(self, batch, batch_idx):
        y_pred = self(batch)
        y = batch["label"]
        loss = self.loss(y_pred,y)

        self.accuracy(y_pred,y)

        self.log("train_loss", loss, on_step=True, on_epoch=True)
        self.log("train_acc", self.accuracy, on_step=True, on_epoch=True)
        return loss

    def validation_step(self, batch, batch_idx):

        y_pred = self(batch)
        y = batch["label"]
        loss = self.loss(y_pred,y)

        self.accuracy_val(y_pred, y)

        self.log("val_loss", loss, on_epoch=True)

        self.log("val_acc", self.accuracy_val, on_epoch=True)

        return {"y":y, "y_pred":y_pred}

    def validation_epoch_end(self,data):
        y_full = torch.cat([el["y"] for el in data]).cpu().numpy()
        y_pred_full = torch.softmax(torch.cat([el["y_pred"] for el in data]),1).cpu().numpy()
        auc = roc_auc_score(y_full, y_pred_full[:,1])
        self.log("val_auc", auc)

    def test_step(self, batch, batch_idx):
        y_pred = self(batch)
        y = batch["label"]
        loss = self.loss(y_pred,y)

        self.accuracy_test(y_pred, y)
        
        self.log("test_loss", loss, on_epoch=True)
        self.log("test_acc", self.accuracy_test, on_epoch=True)
        return {"y":y, "y_pred":y_pred}

    def test_epoch_end(self,data):
        y_full = torch.cat([el["y"] for el in data]).cpu().numpy()
        y_pred_full = torch.softmax(torch.cat([el["y_pred"] for el in data]),1).cpu().numpy()
        auc = roc_auc_score(y_full, y_pred_full[:,1])
        self.log("test_auc", auc)

    @classmethod
    def add_model_specific_args(cls, parent):
        parser = argparse.ArgumentParser(parents=[parent])
        parser.add_argument("--d_hidden", type=int, default=16)
        parser.add_argument("--lr", type=float, default=0.001)
        parser.add_argument('--n_layers', type=int, default=2)
        parser.add_argument('--dropout_p',type=float,default=0.)
        parser.add_argument('--weight_decay',type=float,default=0.)
        parser.add_argument('--d_embed',type=int,default=32)
        parser.add_argument('--nhead',type=int,default=4)
        parser.add_argument('--d_hidden_embed',type = int, default = 32)
        parser.add_argument('--n_layers_embed',type = int, default = 2)
        parser.add_argument('--max_pos_encoding', type = int, default = 15000) 
        return parser




