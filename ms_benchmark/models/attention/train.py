#!/usr/bin/env python
import argparse
import sys

from pytorch_lightning.loggers import WandbLogger
import pytorch_lightning as pl
import ms_benchmark.models.attention.models_new as models
import ms_benchmark.models.attention.data_utils as data_utils

from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from pytorch_lightning.callbacks import EarlyStopping

DATA_MAP = {"Longitudinal": data_utils.LongitudinalDataset}
MODEL_MAP = {"Attention": models.AttentionModel}

def get_logger(args):
    return WandbLogger(
            name=f"Attention_model",
            project="msbase_21",
            entity="ms_prognosis_2021",
            log_model=True,
            tags=[args.model, args.dataset]
        )

def main(model_cls, dataset_cls,args):
    
    dataset = dataset_cls(**vars(args))
    dataset.prepare_data()
    prop_pos = dataset.prop_pos
    gpu = args.gpu

    model = model_cls( input_dim_fixed = dataset.input_dim_fixed, input_dim_longitudinal = dataset.input_dim_longitudinal, prop_pos = prop_pos,
        **vars(args),
     )

    # Loggers and callbacks
    logger = get_logger(args)
    log_dir = getattr(logger, 'log_dir', False) or logger.experiment.dir
    
    #stop_on_min_lr_cb = StopOnMinLR(args.min_lr)
    #lr_monitor = LearningRateMonitor('epoch')
    early_stopping = EarlyStopping('val_auc', patience = 10, mode = "max")

    checkpoint_cb = ModelCheckpoint(
        dirpath=log_dir,
        monitor='val_auc',
        mode='max',
        verbose=True
    )

    trainer = pl.Trainer(
        gpus=gpu,
        logger=logger,
        log_every_n_steps=5,
        max_epochs=args.max_epochs,
        callbacks=[early_stopping, checkpoint_cb]
    )


    trainer.fit(model, datamodule=dataset)
    checkpoint_path = checkpoint_cb.best_model_path
    trainer_test = pl.Trainer(logger = False, gpus = gpu)
    model = model_cls.load_from_checkpoint(checkpoint_path)
    val_results = trainer_test.test(model, test_dataloaders = dataset.val_dataloader())[0]
    val_results = {name.replace("test", "best_val"):value for name, value in val_results.items()}
    test_results = trainer_test.test(model,test_dataloaders = dataset.test_dataloader())[0]
    for name, value in {**val_results, **test_results}.items():
        logger.experiment.summary[name] = value


if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--model', type=str, choices=MODEL_MAP.keys(), default = "Attention")
    parser.add_argument('--dataset', type=str,
                        choices=DATA_MAP.keys(),default = "Longitudinal")
    parser.add_argument('--max_epochs', type=int, default=1000)
    parser.add_argument('--gpu', default=1, type=int)

    partial_args, _ = parser.parse_known_args()
    
    model_cls = MODEL_MAP[partial_args.model]
    dataset_cls = DATA_MAP[partial_args.dataset]

    parser = model_cls.add_model_specific_args(parser)
    parser = dataset_cls.add_dataset_specific_args(parser)
    args = parser.parse_args()

    main(model_cls, dataset_cls, args)
