import pytorch_lightning as pl
import argparse
import torch
import torch.nn as nn
from sklearn.metrics import roc_auc_score, average_precision_score
import time
from torch.nn import TransformerEncoder, TransformerEncoderLayer
import math
from ms_benchmark.models.attention.utils import PositionalEncoding
import torch.nn.functional as F
from ms_benchmark.utils import str2bool


class ReZero(nn.Module):
    def __init__(self):
        super().__init__()
        self.resweight = nn.Parameter(torch.Tensor([0.]))

    def forward(self, x1, x2):
        return x1 + self.resweight * x2

class TransformerEncoderLayer(nn.Module):
    """TransformerEncoderLayer is made up of self-attn and feedforward network.
    This standard encoder layer is based on the paper "Attention Is All You
    Need".  Ashish Vaswani, Noam Shazeer, Niki Parmar, Jakob Uszkoreit, Llion
    Jones, Aidan N Gomez, Lukasz Kaiser, and Illia Polosukhin. 2017. Attention
    is all you need. In Advances in Neural Information Processing Systems,
    pages 6000-6010. Users may modify or implement in a different way during
    application.
    This class is adapted from the pytorch source code.
    Args:
        d_model: the number of expected features in the input (required).
        nhead: the number of heads in the multiheadattention models (required).
        dim_feedforward: the dimension of the feedforward network model
            (default=2048).
        dropout: the dropout value (default=0.1).
        norm: Normalization to apply, one of 'layer' or 'rezero'.
    Examples::
        >>> encoder_layer = nn.TransformerEncoderLayer(d_model=512, nhead=8)
        >>> src = torch.rand(10, 32, 512)
        >>> out = encoder_layer(src)
    """

    def __init__(self, d_model, nhead, dim_feedforward=2048, dropout=0.1,
                 norm='layer'):
        super(TransformerEncoderLayer, self).__init__()
        if norm == 'layer':
            def get_residual():
                def residual(x1, x2):
                    return x1 + x2
                return residual

            def get_norm():
                return nn.LayerNorm(d_model)
        elif norm == 'rezero':
            def get_residual():
                return ReZero()

            def get_norm():
                return nn.Identity()
        else:
            raise ValueError('Invalid normalization: {}'.format(norm))

        self.self_attn = nn.MultiheadAttention(d_model, nhead, dropout=dropout)
        # Implementation of Feedforward model
        self.linear1 = nn.Linear(d_model, dim_feedforward)
        self.dropout = nn.Dropout(dropout)
        self.linear2 = nn.Linear(dim_feedforward, d_model)

        self.norm1 = get_norm()
        self.norm2 = get_norm()
        self.residual1 = get_residual()
        self.residual2 = get_residual()
        self.dropout1 = nn.Dropout(dropout)
        self.dropout2 = nn.Dropout(dropout)

        self.activation = F.relu

    def __setstate__(self, state):
        if 'activation' not in state:
            state['activation'] = F.relu
        super(TransformerEncoderLayer, self).__setstate__(state)

    def forward(self, src, src_mask=None, src_key_padding_mask=None):
        """Pass the input through the encoder layer.
        Args:
            src: the sequence to the encoder layer (required).
            src_mask: the mask for the src sequence (optional).
            src_key_padding_mask: the mask for the src keys per batch (optional).
        Shape:
            see the docs in Transformer class.
        """
        src2 = self.self_attn(
            src, src, src,
            attn_mask=src_mask,
            key_padding_mask=src_key_padding_mask
        )[0]
        src = self.residual1(src, self.dropout1(src2))
        src = self.norm1(src)

        src2 = self.linear2(self.dropout(self.activation(self.linear1(src))))
        src = self.residual2(src, self.dropout2(src2))
        src = self.norm2(src)
        return src


class AttentionModel(pl.LightningModule):
    def __init__(self,input_dim_fixed, input_dim_longitudinal, d_model, d_pos_embed, nhead, dropout_p, n_layers, weight_decay, use_statics, max_pos_encoding, prop_pos, weighted_loss = True, **kwargs):
        super().__init__()
        self.save_hyperparameters()

        #self.Transformer = TransformerModel(d_embed, nhead, d_hidden, n_layers, dropout_p, input_dim_fixed, input_dim_longitudinal, max_pos_encoding, **kwargs)
        #self.classifier = nn.Sequential(torch.nn.Linear(d_embed,d_embed),nn.ReLU(), nn.Linear(d_embed,2))
        weights = torch.ones(2)
        if weighted_loss:
            weights[1] = (1-prop_pos)/prop_pos

        self.loss = torch.nn.CrossEntropyLoss(weight = weights)
            
        self.accuracy = pl.metrics.Accuracy()
        self.accuracy_val = pl.metrics.Accuracy()
        self.accuracy_test = pl.metrics.Accuracy()

        self.weight_decay = weight_decay

        self.PositionalEncoding = PositionalEncoding(1,max_pos_encoding,d_pos_embed)

        ff_dim = 4*d_model # dimensionality of ff layers: hard-coded default
        #self.to_observation_tuples = to_observation_tuples if indicators else to_observation_tuples_without_indicators 
        self.save_hyperparameters()
        d_in = input_dim_longitudinal + d_pos_embed
        
        self.use_statics = use_statics
        if self.use_statics:
            self.embed_statics = nn.Linear(input_dim_fixed,d_model)
            d_in += d_model
        #d_statics, d_in = self._get_input_dims()
        #if not self.hparams.ignore_statics:
        #    self.statics_embedding = nn.Linear(d_statics, d_model)
        self.layers = nn.ModuleList(
            [nn.Linear(d_in, d_model)]
            + [
                TransformerEncoderLayer(
                    d_model, nhead, ff_dim, dropout_p, norm="rezero")
                for n in range(n_layers)
            ]
            + [nn.Linear(d_model, 2)]
        )

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.hparams.lr, weight_decay = self.weight_decay)
        return optimizer

    def forward(self,data):
        
        longitudinal_X = data["longitudinal_X"].permute(1,0,2)
        longitudinal_mask = data["longitudinal_mask"].permute(1,0,2)
        attention_mask = data["attention_mask"].permute(1,0)
        times = data["longitudinal_times"].permute(1,0)
        fixed_X = data["fixed_X"]
        
        times = times * (-1) + 1
        
        pos_encodings = self.PositionalEncoding(times)
        x_full = torch.cat((pos_encodings,longitudinal_X,longitudinal_mask),-1) #(times, values, mask)
        if self.use_statics:
            x_static = self.embed_statics(fixed_X).repeat(x_full.shape[0],1,1)
            x_full = torch.cat((x_full,x_static),-1)

        x = self.layers[0](x_full)
        for layer in self.layers[1:]:
            if isinstance(layer, TransformerEncoderLayer):
                x = layer(
                    x, src_key_padding_mask=(attention_mask==0).T)
            else:
                x = layer(x)
        
        y_hat = x[0]
        return y_hat

    def training_step(self, batch, batch_idx):
        y_pred = self(batch)
        y = batch["label"]
        loss = self.loss(y_pred,y)

        self.accuracy(y_pred,y)

        self.log("train_loss", loss, on_step=True, on_epoch=True)
        self.log("train_acc", self.accuracy, on_step=True, on_epoch=True)
        return loss

    def validation_step(self, batch, batch_idx):

        y_pred = self(batch)
        y = batch["label"]
        loss = self.loss(y_pred,y)

        self.accuracy_val(y_pred, y)

        self.log("val_loss", loss, on_epoch=True)
        self.log("val_acc", self.accuracy_val, on_epoch=True)

        return {"y":y, "y_pred":y_pred}

    def validation_epoch_end(self,data):
        y_full = torch.cat([el["y"] for el in data]).cpu().numpy()
        y_pred_full = torch.softmax(torch.cat([el["y_pred"] for el in data]),1).cpu().numpy()
        auc = roc_auc_score(y_full, y_pred_full[:,1])
        self.log("val_auc", auc)

    def test_step(self, batch, batch_idx):
        y_pred = self(batch)
        y = batch["label"]
        loss = self.loss(y_pred,y)

        self.accuracy_test(y_pred, y)
        
        self.log("test_loss", loss, on_epoch=True)
        self.log("test_acc", self.accuracy_test, on_epoch=True)
        return {"y":y, "y_pred":y_pred}

    def test_epoch_end(self,data):
        y_full = torch.cat([el["y"] for el in data]).cpu().numpy()
        y_pred_full = torch.softmax(torch.cat([el["y_pred"] for el in data]),1).cpu().numpy()
        auc = roc_auc_score(y_full, y_pred_full[:,1])
        self.log("test_auc", auc)

    def predict_step(self,batch, batch_idx):
        y_pred = self(batch)
        y = batch["label"]

        return {"y":y, "y_pred":y_pred}

    @classmethod
    def add_model_specific_args(cls, parent):
        parser = argparse.ArgumentParser(parents=[parent])
        parser.add_argument("--d_model", type=int, default=128)
        parser.add_argument("--lr", type=float, default=0.001)
        parser.add_argument('--n_layers', type=int, default=2)
        parser.add_argument('--dropout_p',type=float,default=0.)
        parser.add_argument('--weight_decay',type=float,default=0.)
        parser.add_argument('--d_pos_embed',type=int,default=10)
        parser.add_argument('--nhead',type=int,default=4)
        parser.add_argument('--d_hidden_embed',type = int, default = 32)
        parser.add_argument('--n_layers_embed',type = int, default = 2)
        parser.add_argument('--max_pos_encoding', type = int, default = 18000) 
        parser.add_argument('--weighted_loss',type=str2bool,default=True)
        parser.add_argument('--use_statics',type=str2bool,default=True, help = "Use static data in the prediction")
        return parser



