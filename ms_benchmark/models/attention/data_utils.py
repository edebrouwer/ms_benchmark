from ms_benchmark import DATA_DIR
import torch
import pytorch_lightning as pl
import pandas as pd
import numpy as np
from ms_benchmark.utils import str2bool
from torch.nn.utils.rnn import pad_sequence
import time
from ms_benchmark.data_utils import normalize_df, normalize_longitudinal_df, get_msbase_static_dataset, get_msbase_dynamic_dataset, get_msbase_longitudinal_dataset
import os
from torch.utils.data import DataLoader, Dataset

DATATYPE_DICT = ("EP","MSBase2020")

class BaseLongitudinalDataset(Dataset):
    def __init__(self,df_fixed, df_longitudinal, fixed_cols, longitudinal_cols, mask_cols_longitudinal):
        super().__init__()

        self.df_fixed = df_fixed
        self.df_longitudinal = df_longitudinal
        self.mask_cols_longitudinal = mask_cols_longitudinal

        self.fixed_cols = fixed_cols
        self.longitudinal_cols = longitudinal_cols
        self.mask_cols_longitudinal = mask_cols_longitudinal # [f"{c}_mask" for c in longitudinal_cols]

        assert self.df_fixed.slice_id.nunique() == self.df_longitudinal.slice_id.nunique()
        slice_id_dict = dict(zip(self.df_fixed.slice_id.unique(),np.arange(self.df_fixed.slice_id.nunique())))
        self.df_fixed["slice_id"] = self.df_fixed["slice_id"].map(slice_id_dict)
        self.df_longitudinal["slice_id"] = self.df_longitudinal["slice_id"].map(slice_id_dict)

        self.df_longitudinal.set_index("slice_id",inplace = True)
        self.df_fixed.set_index("slice_id",inplace = True)
        self.df_fixed.sort_index(inplace = True)
    def __getitem__(self,idx):
        sub_df = self.df_fixed.loc[idx]
        sub_df_longitudinal = self.df_longitudinal.loc[idx]

        fixed_X = sub_df[self.fixed_cols].values
        longitudinal_X = sub_df_longitudinal[self.longitudinal_cols].values
        longitudinal_mask = sub_df_longitudinal[self.mask_cols_longitudinal].values
        times = sub_df_longitudinal["Time"].values
        label = sub_df["label"].astype(int)
        return {"fixed_X" : fixed_X, "longitudinal_X": longitudinal_X, "longitudinal_mask": longitudinal_mask,"longitudinal_times": times ,"label" : label} 

    def __len__(self):
        return self.df_fixed.index.nunique()

def longitudinal_fixed_collate_fn(batch):
    
    fixed_X = torch.Tensor([b["fixed_X"] for b in batch]) # B x d
    longitudinal_X = [torch.cat((torch.Tensor(b["longitudinal_X"]),torch.Tensor(b["longitudinal_mask"])),1)  for b in batch]
    attention_mask = [ torch.ones(b["longitudinal_X"].shape[0]) for b in batch] #Shape is T x B 
    
    longitudinal_X = pad_sequence(longitudinal_X) #Shape is T x B x d
    attention_mask = pad_sequence(attention_mask) #Shape is T x B
    
    label = torch.LongTensor([b["label"] for b in batch]) #Shape is B x 1
    times = pad_sequence([torch.Tensor(b["longitudinal_times"]) for b in batch]) #Shape is T x B

    return {"longitudinal_X" : longitudinal_X, "attention_mask" : attention_mask, "label" : label, "times" : times, "fixed_X" : fixed_X}

class TensorLongitudinalDataset(Dataset):
    def __init__(self,df_fixed = None, df_longitudinal = None, fixed_cols = None, longitudinal_cols = None, mask_cols_longitudinal = None, pre_load = False, source_dir = None, no_static_data = False, scramble_time = False, use_delta_t = True):
        super().__init__()
        
        if pre_load:
            self.tensor_fixed = torch.load(source_dir+"tensor_fixed.pt")
            self.tensor_longitudinal = torch.load(source_dir+"tensor_longitudinal.pt")
            self.longitudinal_mask = torch.load(source_dir+"longitudinal_mask.pt")
            self.attention_mask = torch.load(source_dir+"attention_mask.pt")
            self.times = torch.load(source_dir+"times.pt")
            self.labels = torch.load(source_dir+"labels.pt")
        else:
            self.df_fixed = df_fixed
            self.df_longitudinal = df_longitudinal
            self.mask_cols_longitudinal = mask_cols_longitudinal

            self.fixed_cols = fixed_cols
            self.longitudinal_cols = longitudinal_cols
            self.mask_cols_longitudinal = mask_cols_longitudinal # [f"{c}_mask" for c in longitudinal_cols]

            assert self.df_fixed.slice_id.nunique() == self.df_longitudinal.slice_id.nunique()
            slice_id_dict = dict(zip(self.df_fixed.slice_id.unique(),np.arange(self.df_fixed.slice_id.nunique())))
            self.df_fixed["slice_id"] = self.df_fixed["slice_id"].map(slice_id_dict)
            self.df_longitudinal["slice_id"] = self.df_longitudinal["slice_id"].map(slice_id_dict)
            self.df_fixed.sort_index(inplace = True)
            
            self.tensor_fixed = torch.Tensor(self.df_fixed[self.fixed_cols].values)
            
            max_length = self.df_longitudinal.groupby("slice_id").count().max().max()
            self.tensor_longitudinal  = torch.zeros(self.df_longitudinal.slice_id.nunique(),max_length, len(self.longitudinal_cols))
            self.longitudinal_mask = torch.zeros(self.df_longitudinal.slice_id.nunique(),max_length, len(self.mask_cols_longitudinal))
            self.attention_mask = torch.zeros(self.df_longitudinal.slice_id.nunique(),max_length)
            self.labels = torch.Tensor(self.df_fixed["label"].values)
            self.times = torch.zeros(self.df_longitudinal.slice_id.nunique(),max_length)

            self.df_longitudinal.set_index("slice_id",inplace = True)
            for idx in np.arange(self.tensor_fixed.shape[0]):
                sub_df_longitudinal = self.df_longitudinal.loc[idx]
                self.tensor_longitudinal[idx,:len(sub_df_longitudinal)] = torch.Tensor(sub_df_longitudinal[self.longitudinal_cols].values)
                self.longitudinal_mask[idx,:len(sub_df_longitudinal)] = torch.Tensor(sub_df_longitudinal[self.mask_cols_longitudinal].values)
                self.attention_mask[idx,:len(sub_df_longitudinal)] = 1.
                self.times[idx,:len(sub_df_longitudinal)] = torch.Tensor(sub_df_longitudinal["Time"].values)
            
            os.makedirs(source_dir)
            torch.save(self.tensor_fixed,source_dir+"tensor_fixed.pt")
            torch.save(self.tensor_longitudinal,source_dir+"tensor_longitudinal.pt")
            torch.save(self.longitudinal_mask,source_dir+"longitudinal_mask.pt")
            torch.save(self.attention_mask,source_dir+"attention_mask.pt")
            torch.save(self.times,source_dir+"times.pt")
            torch.save(self.labels,source_dir+"labels.pt")

        self.prop_pos = self.labels.mean()
    
        self.len_cov_cols_fixed = self.tensor_fixed.shape[1]
        self.len_cov_cols_longitudinal = self.tensor_longitudinal.shape[-1]
        self.len_mask_cols_longitudinal = self.longitudinal_mask.shape[-1]

        if no_static_data:
            self.tensor_fixed = torch.zeros(self.tensor_fixed.shape,device = self.tensor_fixed.device)

        if scramble_time:
            self.times = torch.zeros(self.times.shape,device = self.times.device)

    
    def __getitem__(self,idx):
        return {"fixed_X" : self.tensor_fixed[idx], "longitudinal_X": self.tensor_longitudinal[idx], "longitudinal_mask": self.longitudinal_mask[idx],"longitudinal_times": self.times[idx] ,"label" : self.labels[idx].long(), "attention_mask" : self.attention_mask[idx]} 

    def __len__(self):
        return len(self.labels)

class LongitudinalDataset(pl.LightningDataModule):
    def __init__(self, datatype, fold, batch_size, no_dmt, num_workers = 10, no_static_data = False, scramble_time = False, num_visits = 3, **kwargs):
        super().__init__()
        self.num_workers = num_workers
        self.datatype = datatype #EP or MSBase2020

        self.fold = fold
        self.batch_size = batch_size
        self.input_dim = 0

        self.no_dmt = no_dmt

        self.cols = ["EDSS","MSCOURSE","DMT_current","cumulative_relapse"]

        self.base_data_class = TensorLongitudinalDataset #BaseLongitudinalDataset
        self.collate_fn = None # longitudinal_fixed_collate_fn

        self.no_static_data = no_static_data
        self.scramble_time = scramble_time

        self.num_visits = num_visits

    def check_data_available(self,fold):
        if self.base_data_class == BaseLongitudinalDataset:
            return False
        else:
            return os.path.isdir(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/Longitudinal/Attention/fold_{fold}")

    def prepare_data(self):

        if self.check_data_available(fold = self.fold):
            self.data_train = self.base_data_class(pre_load = True, source_dir = DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/Longitudinal/Attention/fold_{self.fold}/train/", no_static_data = self.no_static_data, scramble_time = self.scramble_time )
            self.data_val = self.base_data_class(pre_load = True, source_dir = DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/Longitudinal/Attention/fold_{self.fold}/val/", no_static_data = self.no_static_data, scramble_time = self.scramble_time   )
            self.data_test = self.base_data_class(pre_load = True, source_dir = DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/Longitudinal/Attention/fold_{self.fold}/test/", no_static_data = self.no_static_data, scramble_time = self.scramble_time  )
        else:
            df_processed_static, cov_cols_static, continuous_cols_static = get_msbase_static_dataset(no_dmt = self.no_dmt, num_visits = self.num_visits)
            
            df_processed_dynamic, cov_cols_dynamic, continuous_cols_dynamic = get_msbase_dynamic_dataset(num_visits = self.num_visits)
            
            print("Loading longitudinal data...")
            df_processed_longitudinal, cov_cols_longitudinal, continuous_cols_longitudinal, mask_cols_longitudinal = get_msbase_longitudinal_dataset(num_visits = self.num_visits)
            print("Done.")
            df_processed_fixed = pd.merge(df_processed_static, df_processed_dynamic, on = ["PATIENT_ID","slice_id","label"], how = "outer")

            assert(df_processed_fixed.slice_id.nunique()==df_processed_fixed.shape[0])
            assert(df_processed_fixed.shape[0]==df_processed_static.shape[0])
            assert(df_processed_fixed.shape[0]==df_processed_dynamic.shape[0])

            cov_cols_fixed = cov_cols_static + cov_cols_dynamic
            continuous_cols_fixed = continuous_cols_static + continuous_cols_dynamic

            self.label_train = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/train_idx.csv")
            self.label_val = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/val_idx.csv")
            self.label_test = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/test_idx.csv")
            
            df_fixed_train = df_processed_fixed.loc[df_processed_fixed.slice_id.isin(self.label_train.slice_id.unique())].reset_index().sort_values(by="slice_id")
            df_fixed_val = df_processed_fixed.loc[df_processed_fixed.slice_id.isin(self.label_val.slice_id.unique())].reset_index().sort_values(by="slice_id")
            df_fixed_test = df_processed_fixed.loc[df_processed_fixed.slice_id.isin(self.label_test.slice_id.unique())].reset_index().sort_values(by="slice_id")

            df_longitudinal_train = df_processed_longitudinal.loc[df_processed_longitudinal.slice_id.isin(self.label_train.slice_id.unique())].reset_index().sort_values(by=["slice_id", "Time"])
            df_longitudinal_val = df_processed_longitudinal.loc[df_processed_longitudinal.slice_id.isin(self.label_val.slice_id.unique())].reset_index().sort_values(by=["slice_id", "Time"])
            df_longitudinal_test = df_processed_longitudinal.loc[df_processed_longitudinal.slice_id.isin(self.label_test.slice_id.unique())].reset_index().sort_values(by=["slice_id", "Time"])

            df_fixed_train, df_fixed_val, df_fixed_test = normalize_df(df_fixed_train,df_fixed_val, df_fixed_test, continuous_cols_fixed)
            
            continuous_cols_with_mask = [c for c in continuous_cols_longitudinal if c+"_mask" in mask_cols_longitudinal]
            continuous_cols_without_mask = [c for c in continuous_cols_longitudinal if c+"_mask" not in mask_cols_longitudinal]
            
            df_longitudinal_train, df_longitudinal_val, df_longitudinal_test = normalize_longitudinal_df(df_longitudinal_train,df_longitudinal_val, df_longitudinal_test, continuous_cols_with_mask, masked = True)
            df_longitudinal_train, df_longitudinal_val, df_longitudinal_test = normalize_longitudinal_df(df_longitudinal_train,df_longitudinal_val, df_longitudinal_test, continuous_cols_without_mask, masked = False)

            self.data_train = self.base_data_class(df_fixed_train, df_longitudinal_train, cov_cols_fixed, cov_cols_longitudinal, mask_cols_longitudinal, source_dir= DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/Longitudinal/Attention/fold_{self.fold}/train/", no_static_data = self.no_static_data, scramble_time = self.scramble_time  )
            self.data_val = self.base_data_class(df_fixed_val, df_longitudinal_val, cov_cols_fixed, cov_cols_longitudinal, mask_cols_longitudinal, source_dir = DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/Longitudinal/Attention/fold_{self.fold}/val/", no_static_data = self.no_static_data, scramble_time = self.scramble_time  )
            self.data_test = self.base_data_class(df_fixed_test, df_longitudinal_test, cov_cols_fixed, cov_cols_longitudinal, mask_cols_longitudinal, source_dir = DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/Longitudinal/Attention/fold_{self.fold}/test/", no_static_data = self.no_static_data , scramble_time = self.scramble_time )

        self.prop_pos = self.data_train.prop_pos
       
        self.input_dim_fixed = self.data_train.len_cov_cols_fixed
        self.input_dim_longitudinal = self.data_train.len_cov_cols_longitudinal + self.data_train.len_mask_cols_longitudinal

    def get_df(self):
        df_processed_static, cov_cols_static, continuous_cols_static = get_msbase_static_dataset(no_dmt = self.no_dmt, num_visits = self.num_visits)
        df_processed_dynamic, cov_cols_dynamic, continuous_cols_dynamic = get_msbase_dynamic_dataset(num_visits = self.num_visits)

        df_processed_fixed = pd.merge(df_processed_static, df_processed_dynamic, on = ["PATIENT_ID","slice_id","label"], how = "outer")

        
        self.label_train = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/train_idx.csv")
        self.label_val = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/val_idx.csv")
        self.label_test = pd.read_csv(DATA_DIR+"/"+self.datatype+f"/Cleaned/{self.num_visits}_visits/folds/fold_{self.fold}/test_idx.csv")

        df_fixed_train = df_processed_fixed.loc[df_processed_fixed.slice_id.isin(self.label_train.slice_id.unique())].reset_index().sort_values(by="slice_id")
        df_fixed_val = df_processed_fixed.loc[df_processed_fixed.slice_id.isin(self.label_val.slice_id.unique())].reset_index().sort_values(by="slice_id")
        df_fixed_test = df_processed_fixed.loc[df_processed_fixed.slice_id.isin(self.label_test.slice_id.unique())].reset_index().sort_values(by="slice_id")

        for df in [df_fixed_train, df_fixed_val, df_fixed_test]:
            slice_id_dict = dict(zip(df.slice_id.unique(),np.arange(df.slice_id.nunique())))
            df["slice_id"] = df["slice_id"].map(slice_id_dict)
            df.sort_index(inplace = True)
        return df_fixed_train, df_fixed_val, df_fixed_test

    def train_dataloader(self):
        return DataLoader(
            self.data_train,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
            drop_last=True,
            pin_memory=True,
            collate_fn = self.collate_fn
        )

    def val_dataloader(self):
        return DataLoader(
            self.data_val,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            drop_last=False,
            pin_memory=True,
            collate_fn  = self.collate_fn,
        )

    def test_dataloader(self):
        return DataLoader(
            self.data_test,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
            drop_last=False,
            pin_memory=True,
            collate_fn = self.collate_fn
        )

    @classmethod
    def add_dataset_specific_args(cls, parent):
        import argparse
        parser = argparse.ArgumentParser(parents=[parent], add_help=False)
        parser.add_argument('--fold', type=int, default=0)
        parser.add_argument('--batch_size', type=int, default=512)
        parser.add_argument('--datatype', type=str, default="MSBase2020", choices = DATATYPE_DICT)
        parser.add_argument('--no_dmt',type=str2bool, default = False, help =  "dmts are removed from the (fixed) variables")
        parser.add_argument('--no_static_data',type=str2bool, default = False, help =  "Static data is set to 0.")
        parser.add_argument('--scramble_time',type=str2bool, default = False, help =  "Times are all set to 0.")
        parser.add_argument('--num_workers',type=int, default = 10, help =  "Number of workers to use")
        parser.add_argument('--num_visits',type=int, default = 3, help =  "Number of workers to use")
        return parser


