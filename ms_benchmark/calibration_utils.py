from ms_benchmark import predict_utils
import pytorch_lightning as pl
from sklearn.linear_model import LogisticRegression
from sklearn.calibration import calibration_curve
import torch
import sklearn
from sklearn.calibration import CalibratedClassifierCV
import numpy as np


class ModelSklearn(sklearn.base.BaseEstimator):
    def __init__(self):
        self.is_fitted_ = True
        self.classes_ = [0,1]
    def predict(self,X):
        return X[:,0]>0.5
    def predict_proba(self,X):
        return np.concatenate((1-X,X),-1) 
    def fit(self,X,y):
        return None


def calibration(y_pred_val, y_true_val, y_pred_test, y_true_test, gpu=0, return_calibrator = False):
   
    y_true_platt, y_pred_platt, y_calibrated_platt, clf_platt = platt_scaler(y_pred_val, y_true_val, y_pred_test, y_true_test)
    y_true_iso, y_pred_iso, y_calibrated_iso, clf_iso = isotonic_scaler(y_pred_val, y_true_val, y_pred_test, y_true_test)
    
    if return_calibrator:
        return {"platt":(y_true_platt, y_pred_platt, y_calibrated_platt,clf_platt), "iso": (y_true_iso, y_pred_iso, y_calibrated_iso,clf_iso)}
    else:
        return {"platt":(y_true_platt, y_pred_platt, y_calibrated_platt), "iso": (y_true_iso, y_pred_iso, y_calibrated_iso)}


def platt_scaler(y_pred_val, y_true_val, y_pred_test, y_true_test):

    #Platt scaling
    clf = LogisticRegression(random_state=0).fit(y_pred_val[:,None], y_true_val)

    #Calibrating
    y_calibrated_platt = clf.predict_proba(y_pred_test[:,None])[:,1]
    return y_true_test, y_pred_test, y_calibrated_platt, clf

def isotonic_scaler(y_pred_val, y_true_val, y_pred_test, y_true_test):
    model_sk = ModelSklearn()

    calibrated_clf = CalibratedClassifierCV(base_estimator=model_sk, cv="prefit", method = "isotonic")

    _ = calibrated_clf.fit(y_pred_val[:,None],y_true_val)
    
    y_calibrated_iso = calibrated_clf.predict_proba(y_pred_test[:,None])[:,1]

    return y_true_test, y_pred_test, y_calibrated_iso, calibrated_clf


def expected_calibration_error(y_true,y_calibrated):
    fraction_of_positives, mean_predicted_value = calibration_curve(y_true, y_calibrated, n_bins=20)
    return np.abs(fraction_of_positives-mean_predicted_value).mean()
