import wandb
import pytorch_lightning as pl
import torch
from ms_benchmark.models.baseline_dynamic import models, data_utils
import os

def load_run(run_name, model_cls, data_cls, shuffle_idx = -1):
    api = wandb.Api()
    
    run = api.run(f"ms_prognosis_2021/msbase_21/{run_name}")

    f_name = [f.name for f in run.files() if "ckpt" in f.name][0]
    run.file(f_name).download(replace = True, root = ".")

    model = model_cls
    model = model.load_from_checkpoint(f_name)

    os.remove(f_name)

    hparams = dict(model.hparams)

    dataset = data_cls(**hparams, shuffle_idx = shuffle_idx)
    dataset.prepare_data()

    return model, dataset
