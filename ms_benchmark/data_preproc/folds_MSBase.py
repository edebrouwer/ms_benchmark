from ms_benchmark import DATA_DIR
import pandas as pd
import numpy as np
import os
from ms_benchmark.data_preproc.data_preproc_utils import stratified_group_train_test_split



def create_folds(groupby_key, val_prop, test_prop, indir, outdir):
    df_full = pd.read_pickle(indir+"df_processed.pkl")
    label_df = pd.read_pickle(indir+"label_processed.pkl")

    if groupby_key == "Center":
        df_full["Center"] = df_full["PATIENT_ID"].apply(lambda x : "-".join(x.split("-")[:2])) 
        label_df["Center"] = label_df["PATIENT_ID"].apply(lambda x : "-".join(x.split("-")[:2]))

    seeds = [42,43,44,45,46]
    for fold in range(5):
        label_train, label_test = stratified_group_train_test_split(label_df, group = groupby_key,stratify_by="label",test_size = test_prop, seed = seeds[fold])
        label_train, label_val = stratified_group_train_test_split(label_train, group = groupby_key,stratify_by="label",test_size = val_prop, seed = seeds[fold])

        # Check for no patient leak -------
        df_train = df_full.loc[df_full.slice_id.isin(label_train.slice_id.unique())]
        df_val = df_full.loc[df_full.slice_id.isin(label_val.slice_id.unique())]
        df_test = df_full.loc[df_full.slice_id.isin(label_test.slice_id.unique())]

        assert np.intersect1d(df_train[groupby_key].unique(),(df_test[groupby_key].unique())).shape[0] == 0
        assert np.intersect1d(df_train[groupby_key].unique(),(df_val[groupby_key].unique())).shape[0] == 0
        assert np.intersect1d(df_val[groupby_key].unique(),(df_test[groupby_key].unique())).shape[0] == 0

        # -----------------------------------
        print(f"Created fold {fold} with positive proportions : {(df_train.label==True).sum()/df_train.shape[0]:.2f} (train) -  {(df_val.label==True).sum()/df_val.shape[0]:.2f} (val) -  {(df_test.label==True).sum()/df_test.shape[0]:.2f} (test)")

        os.makedirs(outdir+f"folds/fold_{fold}/",exist_ok=True)    
        label_train.to_csv(outdir+f"folds/fold_{fold}/train_idx.csv",index=False)
        label_val.to_csv(outdir+f"folds/fold_{fold}/val_idx.csv",index=False)
        label_test.to_csv(outdir+f"folds/fold_{fold}/test_idx.csv",index=False)


if __name__ == "__main__":

    groupby_key = "Center" # Center or PATIENT_ID
    test_prop = 0.2
    val_prop = 0.2
    create_folds(groupby_key, val_prop, test_prop)

