import pandas as pd
from datetime import timedelta
import tqdm
import numpy as np
import scipy.signal
import multiprocessing
from ms_benchmark.data_preproc import data_preproc_utils as data_preproc
import os
from multiprocessing import Pool
import itertools
import argparse
from ms_benchmark import DATA_DIR


APB_min = 17
AH_min = 35                             
PEAK_min = 5

# Convenience function to iterate over patients, visits, tests, or measurements
def iterate_over(df, kw):
    VALID = ['patient', 'visit', 'test', 'measurement']

    assert kw in VALID, '%s is not a valid keyword. Should be one of: %s' % (kw, ', '.join(VALID))

    index = VALID.index(kw)

    return df.groupby(['%s_uid' % s for s in VALID[:index+1]], as_index=False)

def get_maximal_peak_to_peak(df):
    '''Returns df containing only the repeated measurements with the maximum peak-to-peak per test
    
    df: Pandas dataframe
    '''
    return df.loc[[max(group.iterrows(), key=lambda k: peak_to_peak(k[1].timeseries))[0] for _, group in iterate_over(df, 'test')]]

def peak_to_peak(ts, offset=100):
    '''Maximum peak-to-peak value for a row
    
    Parameters
    ts: timeseries (numpy array)
    offset: determines how many points to discard at the start of the measurement
    '''
    return np.max(ts[offset:]) - np.min(ts[offset:])

def approximate_entropy(y, mnom, rth):
    """Calculate the approximate entropy of a 1D timeseries
    Code adapted from HCTSA package (https://github.com/benfulcher/hctsa)
    """
    n = y.shape[-1] # Length of the TS
    r = rth * np.std(y, axis=-1, ddof=1)
    phi = np.zeros((2, 1))

    ds = []
    drs = []
    for k in range(1, 3):
        m = mnom + k - 1 # Pattern length
        C = np.zeros((n - m + 1, 1))
        x = np.zeros((n - m + 1, m))

        # Form vector sequences x from the time series y
        for i in range(n - m + 1):
            x[i, :] = y[i:i + m]

        ax = np.ones((n - m + 1, m));
        for i in range(n - m + 1):
            for j in range(m):
                ax[:, j] = x[i,j]; 
            d = abs(x - ax)
            if m > 1:
                d = np.max(d, axis=-1) 
            dr = d <= r
            ds.append(d)
            drs.append(dr)
            C[i] = np.sum(dr) / (n - m + 1)
        phi[k - 1] = np.mean(np.log(C))

    return phi[0] - phi[1]

def normalize_apen(apen_val, nc): 
    '''Normalize the ApEn values by the values used in the paper
    '''
    apen_norm = - (apen_val - nc['median']) / (nc['iqr'] / 1.35)
    apen_norm = 1. / (1 + np.exp(apen_norm))
    apen_norm = (apen_norm - nc['min']) / (nc['max'] - nc['min'])
    return apen_norm

def normalized_approximate_entropy(x, m=None, r=None):
    nc = {
        'median': 0.0660287973,
        'iqr': 0.0629248893,
        'min': 0.230205474,
        'max': 1.0,
        'm': 2,
        'r': 0.2,
        'cutoff': 70
        }
    # nc = yaml.load(open('./normalization_constants.yaml', 'r'), Loader=yaml.FullLoader)
    if len(x) != 1920:
        x = scipy.signal.resample(x, 1920)
    # assert len(x) == 1920, 'Wrong number of samples for the timeseries, consider resampling'
    # ts = scipy.stats.zscore(x[nc['cutoff']:])
    ts = x[nc['cutoff']:]
    if (m is None) and (r is None):
        apen = approximate_entropy(ts, nc['m'], nc['r'])
    else:
        apen = approximate_entropy(ts, m, r)
    norm_apen = normalize_apen(apen, nc)
    return norm_apen

def signal_strength(ts):
    return np.sum(np.abs(ts))

def get_dt(row):
    return float(100.) / row['timeseries'].shape[0]

def sample_thres(dt, thresh):
    return int(thresh / dt)

def facilitated_indices(df):
    vals = []
    inds = []
    # First get the time per sample
    for index, row in tqdm.tqdm(df.iterrows(), total=len(df)):
        tmp_dt = get_dt(row)
        st = signal_strength(row['timeseries'][sample_thres(tmp_dt, PEAK_min): sample_thres(tmp_dt, APB_min)])
        df.loc[index, 'fac'] = st

def mep_cleaning(df):
    ndf = get_maximal_peak_to_peak(df)

    ndf['lat'] = ndf['marker_1_latency(ms)']
    ndf['ppa'] = ndf.timeseries.apply(peak_to_peak)

    # Resample timeseries
    ndf.loc[ndf.timeseries.apply(len) != 1920, 'timeseries'] = ndf[ndf.timeseries.apply(len) != 1920]\
                                                            .timeseries\
                                                            .apply(
                                                                lambda ts: scipy.signal.resample(ts, 1920)
                                                            )

    # Do this in parallel as it takes a while. This will take ~2 minutes, depending on your hardware.
    p = multiprocessing.Pool()
    results = list(tqdm.tqdm(p.imap(normalized_approximate_entropy, ndf.timeseries.values, chunksize=1), total=len(ndf)))
    p.close()

    ndf['apen'] = np.array(results).ravel()

    facilitated_indices(ndf)

    ndf["date"] = pd.to_datetime(pd.to_datetime(ndf.visit_date).dt.date)

    return ndf

def add_first_visit_date(df_in):
    df_ = df_in.copy()
    df_["first_visit_date"] = df_["date"]
    df_ = df_.sort_values(by=["patient_uid","first_visit_date"])
    df_ = df_.drop_duplicates(subset = ["patient_uid"],keep="first")
    df_ =  df_[["patient_uid","first_visit_date"]]
    return df_in.merge(df_,on=["patient_uid"],how = "left")

def add_last_visit_date(df_in):
    df_ = df_in.copy()
    df_["last_visit_date"] = df_["date"]
    df_ = df_.sort_values(by=["patient_uid","last_visit_date"],ascending=False)
    df_ = df_.drop_duplicates(subset = ["patient_uid"],keep="first")
    df_ =  df_[["patient_uid","last_visit_date"]]
    return df_in.merge(df_,on=["patient_uid"], how = "left")

def add_last_edss_date(df_in):
    df_ = df_in.copy()
    df_ = df_.loc[~df_.edss.isna()]
    df_["last_edss_date"] = df_["date"]
    df_ = df_.sort_values(by=["patient_uid","last_edss_date"],ascending=False)
    df_ = df_.drop_duplicates(subset = ["patient_uid"],keep="first")
    df_ =  df_[["patient_uid","last_edss_date"]]
    return df_in.merge(df_,on=["patient_uid"], how = "left")

def merging_clinical_mep(ndf,cdf):
    assert (cdf.duplicated(subset=["edss","date","patient_uid"]).sum()==0)
    df_m = ndf.merge(cdf,on=["patient_uid","date"],how="outer")
    assert ndf.loc[ndf.anatomy.isna()].shape[0]==0

    print("Number of entries with EDSS : ")
    print(cdf.shape[0])
    print("Number of entires with EP OR EDSS :")
    print(df_m.shape[0])
    print("Number of entries with EDSS AND EP : ")
    n_edss_and_ep = df_m.loc[(~df_m.edss.isna())&(~df_m.anatomy.isna())].shape[0]
    print(f"{n_edss_and_ep} ({100*n_edss_and_ep/df_m.shape[0]:.2f} %)")

    print(f"Number of unique patients : ")
    print(df_m.patient_uid.nunique())

    df_m = add_first_visit_date(df_m)
    df_m = add_last_visit_date(df_m)
    df_m = add_last_edss_date(df_m)
    
    df_m["total_followup_time"] = df_m["last_visit_date"]-df_m["first_visit_date"]

    return df_m

if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Cleaning of the MEP dataset and slices generation')
    parser.add_argument('--overwrite', default = False, action = 'store_true',
                    help='to overwrite the cleaning process and directly go to slice generation')

    args = parser.parse_args()

    overwrite = args.overwrite
    num_threads = 10
    # Load the datasets
    mdf = pd.read_pickle(DATA_DIR+'/EP/work_mep_identifiable_keep_clinic_id.p')
    cdf = pd.read_pickle(DATA_DIR+'/EP/work_clinical_identifiable_keep_clinic_id.p')

    
    if (not os.path.isfile(DATA_DIR+"/EP/Cleaned/cleaned_mep.p")) or (overwrite is True):
        print(f"Cleaning the MEP data ....")
        ndf = mep_cleaning(mdf)
        ndf.to_csv(DATA_DIR+"/EP/Cleaned/cleaned_mep.p",index = False)
    else:
        print("Skipping cleaning step ---- run mep_clean.py --overwrite if you want to do the cleaning step !")
        ndf = pd.read_csv(DATA_DIR+"/EP/Cleaned/cleaned_mep.p")
        date_cols = [c for c in ndf.columns if 'date' in c]
        for c in date_cols:
            ndf[c] = pd.to_datetime(ndf[c])
    
    df = merging_clinical_mep(ndf,cdf)
    
    #Compatibility with the procedures 
    #df["DATE_OF_VISIT"] = df["date"]
    df["PATIENT_ID"] = df["patient_uid"]
    df["EDSS"] = df["edss"]
    df["last_relapse_date"] = None
    df["last_relapse_date"] = pd.to_datetime(df["last_relapse_date"],errors="coerce")

    num_years_follow_up = 3
    min_visits_follow_up = 6
    pred_horizon = 2
    window_tol = 0.5
    ref_end = True
    confirmation_window = 180

    def valid_set_fun(df_):
        return data_preproc.count_valid_intervals(df_,num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol, confirmation_window = confirmation_window, ref_end = ref_end, return_valid_set = True)[2]

    print("Computing the slices...")
    df_final, label_final = data_preproc.get_slices(df,num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol, confirmation_window = confirmation_window, ref_end = ref_end, num_threads = num_threads, valid_set_fun = valid_set_fun)

    print("Saving")
    df_final.to_csv(DATA_DIR+"/EP/Cleaned/df_processed.csv",index = False)
    label_final.to_csv(DATA_DIR+"/EP/Cleaned/label_processed.csv",index = False)
    print("Done")
    



    
