import pandas as pd
from datetime import timedelta
import tqdm
import numpy as np
import multiprocessing
import itertools
from multiprocessing import Pool
import multiprocessing
import argparse
import time
import pickle

from ms_benchmark import DATA_DIR
from ms_benchmark.data_preproc import data_preproc_utils as data_preproc
from ms_benchmark.data_preproc import msbase_dmt_clean 
from ms_benchmark.data_preproc import folds_MSBase
import os
from datetime import datetime

import shutil

class printing_class:
    def __init__(self):
        self.prev_total_visits=0
        self.prev_total_patients=0
    def printing_patients_entries(self,df):
        print("Number of patients remaining : {}. Diff = {}".format(df["PATIENT_ID"].nunique(),df["PATIENT_ID"].nunique()-self.prev_total_patients))
        print("Total number of visits : {}. Diff= {}".format(len(df.index),len(df.index)-self.prev_total_visits))
        self.prev_total_visits=len(df.index)
        self.prev_total_patients=df["PATIENT_ID"].nunique()


def process_patients(patients_df = "~/Data/MS/Cleaned_MSBASE/patients.csv",outdir="~/Data/MS/Cleaned_MSBASE/"):
    """
    Processing function for MSBase
    """
    df=pd.read_csv(patients_df,encoding='latin1')


    #Check that every patient has a onset date
    assert(sum(df["MSCOURSE_1_DATE_reformat"].isnull()==False)==len(df.index))
    #assert(sum(df["SYMPTOMS_DATE"].isnull()==False)==len(df.index))
    
    #Check that every patient has an age at first symptoms
    assert(sum(df["age_at_MSCOURSE1_days"].isnull()==False)==len(df.index))
    
    #df["age_at_first_symptoms_days"] = df["age_at_MSCOURSE1_days"]
    #Average age at first symptoms.
    #mean_age_1symptom = df["age_at_first_symptoms_days"].mean()/365
    mean_age_onset = df["age_at_MSCOURSE1_days"].mean()/365
    print(f"Average age at onset : {mean_age_onset}")
    print("Number of patients after the reformat step : {}".format(df["PATIENT_ID"].nunique()))


    #Date time conversions.
    df["birth_date"]=pd.to_datetime(df["BIRTH_DATE"], format='%d.%m.%Y')
    df["onset_date"]=pd.to_datetime(df["SYMPTOMS_DATE"], format='%d.%m.%Y', errors = "coerce")
    df['START_OF_PROGRESSION']=pd.to_datetime(df['START_OF_PROGRESSION'], format='%d.%m.%Y', errors="coerce")
    df["SYMPTOMS_DATE"]=pd.to_datetime(df['SYMPTOMS_DATE'], format='%d.%m.%Y')
    df["MS_DIAGNOSIS_DATE_reformat"]=pd.to_datetime(df['MS_DIAGNOSIS_DATE'],format='%d.%m.%Y', errors="coerce")
    df["CLINIC_ENTRY_DATE"]=pd.to_datetime(df['CLINIC_ENTRY_DATE'], format='%d.%m.%Y', errors="coerce")
    df["DATE_OF_FIRST_RELAPSE"]=pd.to_datetime(df['DATE_OF_FIRST_RELAPSE'], format='%d.%m.%Y', errors="coerce")
   
    #delete patients with invalid birth date
    df.drop(df.loc[df.birth_date.isna()].index, inplace = True)
    print("Number of remaining patients : {}".format(df["PATIENT_ID"].nunique()))
    print("Number of patients after removing invalid birth dates: {}".format(df["PATIENT_ID"].nunique()))
    
    #delete patients with invalid onset date
    df.drop(df.loc[df.onset_date.isna()].index, inplace = True)
    print("Number of remaining patients : {}".format(df["PATIENT_ID"].nunique()))
    print("Number of patients after removing invalid format of onset date: {}".format(df["PATIENT_ID"].nunique()))

    onset_age=df["onset_date"]-df["birth_date"]
    df["age_at_onset_days_computed"]=onset_age.dt.days
            
    #Delete patients with invalid diagnosis date.
    df=df.drop(df.loc[(df["MS_DIAGNOSIS_DATE_reformat"].isnull() & df["MS_DIAGNOSIS_DATE"].notnull())].index).copy()
    print("Number of remaining patients : {}".format(df["PATIENT_ID"].nunique()))
    print("Number of patients after removing invalid format of diagnosis date: {}".format(df["PATIENT_ID"].nunique()))

    #Delete patients with invalid gender.
    df=df.drop(df.loc[df["gender"].isna()].index).copy()
    print("Number of remaining patients : {}".format(df["PATIENT_ID"].nunique()))
    print("Number of patients after removing missing gender: {}".format(df["PATIENT_ID"].nunique()))
  
    # delete patients with invalid age_at_onset 
    df = df.drop(df.loc[df["age_at_onset_days_computed"].isna()].index).copy()    
    print("Number of patients after removing missing age at onset: {}".format(df["PATIENT_ID"].nunique()))
    df["age_at_onset_years_computed"] = df["age_at_onset_days_computed"] / 365
    

    from datetime import datetime
    today=datetime(2020,9,1)

    print("Number of patients with dates > {} : {}".format(today,len(df.loc[df["MS_DIAGNOSIS_DATE_reformat"]>today].index)))
    print("Number of patients with dates > {} : {}".format(today,len(df.loc[df["SYMPTOMS_DATE"]>today].index)))
    print("Number of patients with dates > {} : {}".format(today,len(df.loc[df["birth_date"]>today].index)))
    print("Number of patients with dates > {} : {}".format(today,len(df.loc[df["onset_date"]>today].index)))
    print("Number of patients with dates > {} : {}".format(today,len(df.loc[df["START_OF_PROGRESSION"]>today].index)))
    print("Number of patients with dates > {} : {}".format(today,len(df.loc[df["CLINIC_ENTRY_DATE"]>today].index)))
    print("Number of patients with dates > {} : {}".format(today,len(df.loc[df["DATE_OF_FIRST_RELAPSE"]>today].index)))

    df=df.drop(df.loc[df["MS_DIAGNOSIS_DATE_reformat"]>today].index).copy()
    df=df.drop(df.loc[df["SYMPTOMS_DATE"]>today].index).copy()
    df=df.drop(df.loc[df["birth_date"]>today].index).copy()
    df=df.drop(df.loc[df["onset_date"]>today].index).copy()
    df=df.drop(df.loc[df["START_OF_PROGRESSION"]>today].index).copy()
    df=df.drop(df.loc[df["CLINIC_ENTRY_DATE"]>today].index).copy()
    df=df.drop(df.loc[df["DATE_OF_FIRST_RELAPSE"]>today].index).copy()

    print("Number of patients after removing aberrant dates: {}".format(df["PATIENT_ID"].nunique()))

    df["Education_status"] = df["education"].map({"HIGH_SCHOOL":"lower","UNIVERSITY":"higher", "ELEMENTARY_SCHOOL":"lower","COLLEGE" : "higher", "ELEMENTARY":"lower", "HIGHSCHOOL":"lower"})
    df.Education_status.fillna("unknown", inplace = True)
    df.drop(columns = ["education"],inplace = True)

    first_symptoms_cols = ["FIRST_SYMPTOM_OPTIC_PATHWAYS","FIRST_SYMPTOM_BRAINTSTEM","FIRST_SYMPTOM_SPINAL_CORD","FIRST_SYMPTOM_SUPRATENTORIAL"]
    
    df_subset=df[['PATIENT_ID', 'birth_date', 'gender',"Education_status",  'age_at_onset_years_computed','onset_date']+first_symptoms_cols].copy()
   
    #df_subset.rename({'MS_DIAGNOSIS_DATE_reformat':'MS_DIAGNOSIS_DATE'},inplace=True,axis='columns')
    df_subset.to_pickle(outdir+"patients_clean.pkl")

    print('number of unique patients in patients_clean.pkl: ')
    print(df["PATIENT_ID"].nunique())

    print(f"Patients file processed. Saved in {outdir}patients_clean.pkl")


def process_visits(visits_df = "~/Data/MS/Cleaned_MSBASE/visits.csv",clean_pats_pkl= "/home/edward/Data/MS/Cleaned_MSBASE/patients_clean.pkl"):
    """
    Processing function for MSBase
    """
    print_instance=printing_class()
    cols_to_use = ['PATIENT_ID', 'VISIT_FK', 'DATE_OF_VISIT', 'edss',
                    'KFS_1', 'KFS_2', 'KFS_3', 'KFS_4', 'KFS_5', 'KFS_6',
                    'KFS_7','KFS_AMBULATION', 'DURATION_OF_MS_AT_VISIT',
                    'DURATION_OF_MS_AT_VISIT_ROUNDED','FIRST_MSCOURSE',
                    'MSCOURSE_AT_VISIT']
    df=pd.read_csv(visits_df,usecols = cols_to_use).rename(columns = {"edss":"EDSS"})
    patients_df=pd.read_pickle(clean_pats_pkl)
    print('------------- number of patients in the visits.csv file: ----------------')
    print_instance.printing_patients_entries(df)
    print('-------------------------------------------------------------------------')
    print('')
    

    #Linking with patients table.
    
    #Only select patients present in the cleaned patients pkl.
    patient_clean_idx=patients_df["PATIENT_ID"]
    df=df.loc[df["PATIENT_ID"].isin(patient_clean_idx)].copy()

    df_link=patients_df.merge(df,on="PATIENT_ID")
    df=df_link.copy()

    df["DATE_OF_VISIT"] = pd.to_datetime(df["DATE_OF_VISIT"], format='%d.%m.%Y', errors = "coerce")
    
    print('------------- number of patients after the cleaning steps from process_patients(): ----------------')
    print_instance.printing_patients_entries(df_link)
    print('----------------------------------------------------------------------------------------------------')
    print('')


    #Basic Cleaning of the data.
    #Remove entries with missing EDSS
    df=df.drop(df.loc[df["EDSS"].isnull()].index).copy()
    print('------------- number of patients after removing "no EDSS" visits: ----------------')
    print_instance.printing_patients_entries(df)
    print('-----------------------------------------------------------------------------------')
    print('')

    #Remove inconsistent visits (same day but with different EDSS measurements)
    df1=df.drop_duplicates(subset=["PATIENT_ID","DATE_OF_VISIT"],keep=False) #contains only entries wich are not duplicated

    several_visits=df.duplicated(subset=["PATIENT_ID","DATE_OF_VISIT"],keep=False) 
    df2=df[several_visits] # contains all duplicated entries
    assert((len(df1.index)+len(df2.index))==len(df.index))

    #Remove the entries with same date and patients but different EDSS.
    df3=df2.drop_duplicates(subset=["PATIENT_ID","DATE_OF_VISIT","EDSS"],keep="first")
    df4=df3.drop_duplicates(subset=["PATIENT_ID","DATE_OF_VISIT"],keep=False)

    df=df1.append(df4)
    assert((len(df1.index)+len(df4.index))==len(df.index))
    assert(sum(df.duplicated(subset=["PATIENT_ID","DATE_OF_VISIT"],keep=False))==0)
    print('------------- number of patients after removing duplicate or inconsistend EDSS visits: ----------------')
    print_instance.printing_patients_entries(df)
    print('-------------------------------------------------------------------------------------------------------')
    print('')    

    #Create date_visits_processed (conversion to datetime)
    df["DATE_OF_VISIT"]=pd.to_datetime(df["DATE_OF_VISIT"], format='%d.%m.%Y', errors="coerce")
    df=df.drop(df.loc[df["DATE_OF_VISIT"].isnull()].index).copy() #Drop wrong dates

    #Remove entries whose date > 2020.
    from datetime import datetime
    today=datetime(2020,9,1)
    df=df.drop(df.loc[(df["DATE_OF_VISIT"]>today)].index).copy()

    print('------------- number of patients after removing visits after extraction date: ----------------')
    print_instance.printing_patients_entries(df)
    print('----------------------------------------------------------------------------------------------')
    print('')

    #Remove entries where onset date > visit: 
    # df=df.drop(df.loc[df["onset_date"]>df["DATE_OF_VISIT"]].index).copy()
    # assert(len(df.loc[df["onset_date"]>df["DATE_OF_VISIT"]].index)==0)
    
    df_ascend=df.sort_values(by=["PATIENT_ID","DATE_OF_VISIT"]).copy()

    # print('------------- onset date > DATE OF VISIT: ----------------')
    # print_instance.printing_patients_entries(df)
    # print('----------------------------------------------------------')
    # print('')

    df_first=df_ascend.drop_duplicates(subset=["PATIENT_ID"],keep="first")[["PATIENT_ID","EDSS","DATE_OF_VISIT"]]
    df_first.rename(columns={'EDSS':'first_EDSS',"DATE_OF_VISIT":"first_visit_date"},inplace=True)
    
    df=df.merge(df_first,on="PATIENT_ID").copy()
    
    
    df_ascend=df.sort_values(by=["PATIENT_ID","DATE_OF_VISIT"]).copy()
    df_last=df_ascend.drop_duplicates(subset=["PATIENT_ID"],keep="last")[["PATIENT_ID","EDSS","DATE_OF_VISIT"]]
    df_last.rename(columns={'EDSS':'last_EDSS',"DATE_OF_VISIT":"last_visit_date"},inplace=True)

    assert(sum(df_first.duplicated(subset=["PATIENT_ID"]))==0)
    assert(sum(df_last.duplicated(subset=["PATIENT_ID"]))==0)

    df=df.merge(df_last,on="PATIENT_ID").copy()

    #remove invalid dates
    df = df.loc[~df.DATE_OF_VISIT.isna()]
    df["date"] = df["DATE_OF_VISIT"]
    # df["date"] = pd.to_datetime(df["DATE_OF_VISIT"])


    print('------------- remove invalid dates ------------------')
    print_instance.printing_patients_entries(df)
    print('-----------------------------------------------------')
    print('')
   

    #keep only patient with more than 1 EDSS measurements
    edss_count_s = df.groupby("PATIENT_ID")["EDSS"].count()
    pat_idx = edss_count_s[edss_count_s>0].index
      
    df = df.loc[df.PATIENT_ID.isin(pat_idx)].copy()

    print('------------- only keep patients with more than 1 EDSS measurement ------------------')
    print_instance.printing_patients_entries(df)
    print('-------------------------------------------------------------------------------------')
    print('')


    # drop patients that are still CIS at the end of their follow up
    dfsort = df.sort_values(by=['PATIENT_ID', 'DATE_OF_VISIT']).groupby('PATIENT_ID')[['PATIENT_ID', 'MSCOURSE_AT_VISIT']].last()
    dfsort_cis = dfsort[dfsort.MSCOURSE_AT_VISIT == 'CIS']
    pat_ids_cis = dfsort_cis['PATIENT_ID']
    arr_filter = np.asarray(pat_ids_cis)
    df = df.drop(df.loc[df["PATIENT_ID"].isin(arr_filter)].index).copy()
    del dfsort, dfsort_cis, pat_ids_cis

 
    print("Remaining patients in the database : {}".format(df["PATIENT_ID"].nunique()))

    print('------------- remove patients always CIS ------------------')
    print_instance.printing_patients_entries(df)
    print('----------------------------------------------------------')
    print('')

    
    min_date=datetime(1970,1,1)
    #Remove patients with onset_date <1970
    #df_clean=df.drop(df.loc[df["onset_date"]<min_date].index).copy()
    #print("Remaining patients in the database : {}".format(df_clean["PATIENT_ID"].nunique())) 

    #First remove patients with visits before 1970.
    #df_clean = df_clean.drop(df_clean.loc[df_clean["first_visit_date"]<min_date].index).copy()
    
    # Remove visits before 1970.
    df_clean = df.drop(df.loc[df["DATE_OF_VISIT"]<min_date].index).copy()
    print("Remaining patients in the database : {}".format(df_clean["PATIENT_ID"].nunique()))

    print('------------- remove visits before 1970 ------------------')
    print_instance.printing_patients_entries(df_clean)
    print('----------------------------------------------------------')
    print('')


    # Clean the KFS Variables : remove values = -1
    for kf_id in range(1,8):
        kf_col = f"KFS_{kf_id}"
        df_clean.loc[df_clean[kf_col]<0,kf_col] = np.nan

    # for KFS_ambulation
    kf_col = "KFS_AMBULATION"
    df_clean.loc[df_clean[kf_col]<0,kf_col] = np.nan

    return df_clean


def merge_with_relapses_all_separate(df_in, df_relapse = "~/Data/MS/MSBase2020/RELAPSE.csv"):
    """
    Merge with relapse MSBASE
    """
    df = df_in.copy()
    init_n_pats = df.PATIENT_ID.nunique()
    rel = pd.read_csv(df_relapse)

    positions_cols = ["BOWEL_BLADDER", "cerebellum", "VISUAL_FUNCTION", "SENSORY_FUNCTION", "PYRAMIDAL_TRACT", "brainstem"]
    # rel["location_relapse_other"] = rel[other_positions_cols].any(1)
    # rel.rename(columns = {"PYRAMIDAL_TRACT":"location_relapse_pyramidal","brainstem":"location_relapse_brainstem"},inplace = True)
    # rel.drop(columns = other_positions_cols, inplace = True)

    rel["DATE_OF_ONSET"] = pd.to_datetime(rel["DATE_OF_ONSET"], format='%d.%m.%Y', errors="coerce")
    df["date"] = pd.to_datetime(df["date"], format='%d.%m.%Y', errors="coerce")
    
    # Remove relapses before 1970.
    min_date=datetime(1970,1,1)
    today=datetime(2020,9,1)
    rel = rel.drop(rel.loc[rel["DATE_OF_ONSET"]<min_date].index).copy()
    rel = rel.drop(rel.loc[rel["DATE_OF_ONSET"]>today].index).copy()

    rel["DATE_OF_VISIT"] = rel["DATE_OF_ONSET"]
    rel["Relapse"] = True
    var_keep = ["PATIENT_ID", "DATE_OF_ONSET", "Relapse"] + positions_cols
    rel = rel[var_keep].copy()
    rel.dropna("index", subset = ["DATE_OF_ONSET"], inplace = True)

    df_m = pd.merge(df,rel,on = ["PATIENT_ID"], how = "left")
    df_m["time_since_relapse"] = (df_m["date"]-df_m["DATE_OF_ONSET"]).dt.days
    df_m.loc[df_m.DATE_OF_ONSET.isna(),"time_since_relapse"] = -9999 #no relapse for these patients.
    assert df_m.time_since_relapse.isna().sum() == 0
    df_m.loc[df_m["time_since_relapse"]<0,"time_since_relapse"] = 9999
    df_m = df_m.sort_values(by=["PATIENT_ID","date","time_since_relapse"])
    df_m = df_m.drop_duplicates(subset = ["PATIENT_ID","DATE_OF_VISIT"], keep = "first") 
    df_m.rename(columns = {"DATE_OF_ONSET":"last_relapse_date"},inplace = True)
    df_m.loc[df_m["time_since_relapse"]==9999,"last_relapse_date"] =None
    
    columns_to_drop = ["Relapse"] + positions_cols
    df_m.drop(columns = columns_to_drop, inplace = True)

    # add all relapses as observations
    rel_obs = rel.loc[rel.PATIENT_ID.isin(df_m.PATIENT_ID.unique())].copy()
    rel_obs.rename(columns  = {"DATE_OF_ONSET":"date"}, inplace = True)
    rel_obs = rel_obs.loc[~rel_obs.date.isna()].copy()
    rel_obs.drop_duplicates(subset = ["PATIENT_ID","date"],keep="first",inplace = True)
    
    df_with_rel = pd.merge(df_m, rel_obs, how = "outer", on = ["PATIENT_ID","date"])
    df_with_rel.loc[df_with_rel.Relapse.isna(),"Relapse"] = False
    df_with_rel.sort_values(["PATIENT_ID","date"],inplace = True)

    df_with_rel["Relapse_flag"] = df_with_rel["Relapse"].astype(int)
    df_with_rel["cumulative_relapse"] = df_with_rel.groupby("PATIENT_ID")["Relapse_flag"].cumsum()
    df_with_rel.drop(columns = ["Relapse_flag"], inplace = True) 

    # Time since relapse is 0 at date of relapse.
    df_with_rel.loc[df_with_rel.Relapse,"time_since_relapse"] = 0
    
    assert df_with_rel.time_since_relapse.isna().sum() == 0

    assert df_with_rel.PATIENT_ID.nunique() == init_n_pats
    
    return df_with_rel.copy()


def merge_with_relapses(df_in, df_relapse = "~/Data/MS/MSBase2020/RELAPSE.csv"):
    """
    Merge with relapse MSBASE
    """
    df = df_in.copy()
    init_n_pats = df.PATIENT_ID.nunique()
    rel = pd.read_csv(df_relapse)

    other_positions_cols = ["BOWEL_BLADDER","cerebellum","VISUAL_FUNCTION","NEUROPSYCHO_FUNCTION","SENSORY_FUNCTION"]
    rel["location_relapse_other"] = rel[other_positions_cols].any(1)
    rel.rename(columns = {"PYRAMIDAL_TRACT":"location_relapse_pyramidal","brainstem":"location_relapse_brainstem"},inplace = True)
    rel.drop(columns = other_positions_cols, inplace = True)

    rel["DATE_OF_ONSET"] = pd.to_datetime(rel["DATE_OF_ONSET"], format='%d.%m.%Y', errors="coerce")
    rel["DATE_OF_VISIT"] = rel["DATE_OF_ONSET"]
    rel["Relapse"] = True
    rel = rel[["PATIENT_ID","DATE_OF_ONSET","Relapse","severity", "location_relapse_brainstem", "location_relapse_pyramidal", "location_relapse_other"]].copy()
    rel.dropna("index", subset = ["DATE_OF_ONSET"], inplace = True)

    df_m = pd.merge(df,rel,on = ["PATIENT_ID"], how = "left")
    df_m["time_since_relapse"] = (df_m["date"]-df_m["DATE_OF_ONSET"]).dt.days
    df_m.loc[df_m.DATE_OF_ONSET.isna(),"time_since_relapse"] = -9999 #no relapse for these patients.
    assert df_m.time_since_relapse.isna().sum() == 0
    df_m.loc[df_m["time_since_relapse"]<0,"time_since_relapse"] = 9999
    df_m = df_m.sort_values(by=["PATIENT_ID","date","time_since_relapse"])
    df_m = df_m.drop_duplicates(subset = ["PATIENT_ID","DATE_OF_VISIT"], keep = "first") 
    df_m.rename(columns = {"DATE_OF_ONSET":"last_relapse_date"},inplace = True)
    df_m.loc[df_m["time_since_relapse"]==9999,"last_relapse_date"] =None
    
    df_m.drop(columns = ["severity","Relapse","location_relapse_brainstem","location_relapse_pyramidal","location_relapse_other"],inplace = True)

    #add all relapses as observations
    rel_obs = rel.loc[rel.PATIENT_ID.isin(df_m.PATIENT_ID.unique())].copy()
    rel_obs.rename(columns  = {"DATE_OF_ONSET":"date"}, inplace = True)
    rel_obs = rel_obs.loc[~rel_obs.date.isna()].copy()
    rel_obs.drop_duplicates(subset = ["PATIENT_ID","date"],keep="first",inplace = True)
    
    df_with_rel = pd.merge(df_m,rel_obs, how = "outer", on = ["PATIENT_ID","date"])
    df_with_rel.loc[df_with_rel.Relapse.isna(),"Relapse"] = False
    df_with_rel.sort_values(["PATIENT_ID","date"],inplace = True)

    df_with_rel["Relapse_flag"] = df_with_rel["Relapse"].astype(int)
    df_with_rel["cumulative_relapse"] = df_with_rel.groupby("PATIENT_ID")["Relapse_flag"].cumsum()
    df_with_rel.drop(columns = ["Relapse_flag"], inplace = True) 

    # Time since relapse is 0 at date of relapse.
    df_with_rel.loc[df_with_rel.Relapse,"time_since_relapse"] = 0
    
    assert df_with_rel.time_since_relapse.isna().sum() == 0

    assert df_with_rel.PATIENT_ID.nunique() == init_n_pats
    
    return df_with_rel.copy()


def merge_with_MRI(df_in, df_mri = "~/Data/MS/MSBase2020/MRI.csv"):


    df = df_in.copy()
    init_n_pats = df.PATIENT_ID.nunique()
    mri = pd.read_csv(df_mri)
    
    mri = mri.loc[mri.EXAM_TYPE=="BRAIN"].copy()
    mri_cols = ["T1_LESION","T1_GADOLINIUM_LESION","T2_LESION", "T1_RESULT", "T1_GADOLINIUM_RESULT", "T2_RESULT"]
    mri_cols_lesion =  ["T1_LESION","T1_GADOLINIUM_LESION","T2_LESION"]
    mri_cols_result =  ["T1_RESULT", "T1_GADOLINIUM_RESULT", "T2_RESULT"]

    mri = mri[["PATIENT_ID","EXAM_DATE"]+mri_cols].rename(columns = {"EXAM_DATE":"date"})
    mri = mri.loc[~(mri[mri_cols].isna().all(axis=1))]

    
    mri["date"] = pd.to_datetime(mri["date"], format='%d.%m.%Y', errors = "coerce")
    
    # Remove relapses before 1970.
    min_date=datetime(1970,1,1)
    today = datetime(2020,9,1)
    mri = mri.drop(mri.loc[mri["date"]<min_date].index).copy()
    mri = mri.drop(mri.loc[mri["date"]>today].index).copy()
    
    mri["MRI"] = True

    mri_cols += ["MRI"]

    mri = mri.loc[mri.PATIENT_ID.isin(df.PATIENT_ID.unique())].copy()
    mri = mri.loc[~mri.date.isna()].copy()

    agg_mri_lesions = mri.groupby(["PATIENT_ID","date"])[mri_cols].mean().reset_index() #take average value of lesion fields if repeated dates.
    
    # If duplicates Time and patient wise, we take the values which are not "unkown" (last in alphabetical order)
    mri_results_list = []
    for mri_col in mri_cols_result:
        mri_ = mri.sort_values(by=["PATIENT_ID","date",mri_col])[["PATIENT_ID","date",mri_col]].copy()
        mri_.drop_duplicates(subset=["PATIENT_ID","date"], keep = "first", inplace = True)
        mri_results_list.append(mri_)

    mri_results = mri_results_list[0]
    for df_ in mri_results_list[1:]:
        mri_results = pd.merge(mri_results,df_, on = ["PATIENT_ID","date"], how = "outer").copy()


    agg_mri = pd.merge(agg_mri_lesions,mri_results,how = "outer",on = ["PATIENT_ID","date"]).copy()
    
    #Unknown result is converted to nan.
    for result_col in mri_cols_result:
        agg_mri.loc[agg_mri[result_col]=="UNKNOWN",result_col] = np.nan
    
    df_m = pd.merge(df,agg_mri, how = "outer", on = ["PATIENT_ID","date"])

    assert df_m.duplicated(subset=["PATIENT_ID","date"]).sum() == 0
    assert df_m.PATIENT_ID.nunique() == init_n_pats
    return df_m.copy()


def merge_with_fampridine(df_in, df_treat_name):
    """
    input:
    -- df_in: full processed dataframe
    -- df_treat:  TREATMENTS.CSV
    creates new columns 'treatment' for Fampridine and 'FAMRIDINE_START_DATE'
    output: dataframe with new fampridine column
    """
    df = df_in.copy()

    df_treat = pd.read_csv(df_treat_name)
    df_treatment = df_treat.copy()

    df_treatment["processed_treatment"] = df_treatment["treatment"]

    treatment_names = ['4-aminopyridine', '4-Aminopyridin', '4- Aminopyridine', '4- aminopyridine', '4-Aminopyridine', 
                        '4 aminopiridina', 'Aminopiridina', '4 aminopiridina 5 mg', 'aminopiridina 10mg', '4-Aminopiridina 10mg', 
                        'Fampyra', 'FAMPYRA', 'fampridina  FAMPYRA', 'Famypra', 'Faympra',
                        'Fampridine', 'Fampridine (Fampyra)', 'Fampridin', 'FAMPRIDINA', 'fampridina', 'Fampridina',  
                        'Fampiridine', 'fampridine', 'Fampyridine', 'Dalfampridine',
                        '3-4 Diaminopiridina', 'Diaminopiridina']
    
    for str_el in treatment_names:
        df_treatment.loc[df_treatment.treatment == str_el, "processed_treatment"] = "Fampridine"
    
    df_final_treatment = df_treatment[df_treatment.processed_treatment == "Fampridine"]
    df_final_treatment = df_final_treatment[['PATIENT_ID', 'START_DATE', 'processed_treatment']]
    df_final_treatment.drop_duplicates(inplace=True)
    df_final_treatment["START_DATE"] = pd.to_datetime(df_final_treatment["START_DATE"], format='%d.%m.%Y', errors = "coerce")

    
    # Remove relapses before 1970.
    min_date=datetime(1970,1,1)
    today = datetime(2020,9,1)
    df_final_treatment = df_final_treatment.drop(df_final_treatment.loc[df_final_treatment["START_DATE"]<min_date].index).copy()
    df_final_treatment = df_final_treatment.drop(df_final_treatment.loc[df_final_treatment["START_DATE"]>today].index).copy()

    df_final_treatment.rename(columns={'processed_treatment': 'treatment', 'START_DATE': 'FAMPRIDINE_START_DATE'}, inplace=True)
    df_final_treatment["date"] = df_final_treatment['FAMPRIDINE_START_DATE']
    df_merge = df.merge(df_final_treatment, how="outer", on=['PATIENT_ID', 'date'])

    return df_merge


def process_dmt_path(path):
    unpaired = []
    for dmt in path:
        if dmt[0] == "-":
            #check if this is in the set of open dmts.
            if dmt[1:] in unpaired:
                unpaired.remove(dmt[1:])
            else:
                unpaired.append(dmt)
        else:
            unpaired.append(dmt)

    return unpaired


def current_DMT_fun(df_in):
    df = df_in.sort_values(by=["date","DMT_START"]).copy() #First processing the ending dmts and then continue with the starting ones.
    
    df["processed_dmt_token"] = df["processed_dmt"]
    df.loc[df.DMT_START!=True, "processed_dmt_token"] = "-" + df.loc[df.DMT_START!=True, "processed_dmt_token"] 

    df["processed_dmt_token"] = df["processed_dmt_token"].apply(lambda x : [x])
    df["dmt_story"] = df["processed_dmt_token"].cumsum()

    df["dmt_current"] = df["dmt_story"].apply(process_dmt_path)
    
    return df


def clean_dmt_current_set(df_in, suffix = ""):
    df = df_in.copy()
    df.sort_values(by=["PATIENT_ID","date"],inplace = True)
    df["dmt_current_set"+suffix]  = df.groupby("PATIENT_ID")["dmt_current_set"+suffix].ffill()
    return df

def merge_with_DMT(df_in, df_dmt = "~/Data/MS/MSBase2020/TREATMENT.csv"):

    df = df_in.copy()
    init_n_pats = df.PATIENT_ID.nunique()
    dmt = pd.read_csv(df_dmt)
    
    dmt["START_DATE"] = pd.to_datetime(dmt["START_DATE"], format='%d.%m.%Y', errors = "coerce")
    dmt["END_DATE"] = pd.to_datetime(dmt["END_DATE"], format='%d.%m.%Y', errors = "coerce")

    dmt_clean = msbase_dmt_clean.clean_dmt_df(dmt)
    dmt_clean = dmt_clean.loc[dmt_clean.PATIENT_ID.isin(df.PATIENT_ID.unique())].copy()

    assert dmt_clean[["DMT_START","DMT_END"]].isna().all(1).sum() == 0 #DMT should start or end.
    dmt_clean[["DMT_START","DMT_END"]] = dmt_clean[["DMT_START","DMT_END"]].fillna(False)
    dmt_clean.drop_duplicates(subset = ["PATIENT_ID","date","DMT_START","DMT_END","processed_dmt"], inplace = True)
   
    dmt_dict, _ = get_dmt_group_dict()
    dmt_clean["dmt_group"] = dmt_clean["processed_dmt"].map(dmt_dict)

    dmt_clean_induction = dmt_clean.loc[dmt_clean.dmt_group.str.contains("Induction")].copy()
    dmt_clean_no_induction = dmt_clean.loc[~dmt_clean.dmt_group.str.contains("Induction")].copy()

    # We don't want induction drugs to be started at the same time. Check : 
    assert dmt_clean_induction.loc[(dmt_clean_induction.duplicated(subset = ["PATIENT_ID","date","DMT_START"], keep = False)) & (dmt_clean_induction.DMT_START==True)].shape[0] == 0
   
    # We delete the dmt which are started at the same time and keep only the most powerful one
    dict_effectiveness = {"Mild":0, "Moderate":1, "High":2}
    dmt_clean_no_induction["dmt_strength"] = dmt_clean_no_induction["dmt_group"].map(dict_effectiveness) 
    dmt_clean_no_induction.sort_values(by = ["PATIENT_ID","date","dmt_strength"],inplace = True)
    index_to_drop = dmt_clean_no_induction.loc[(dmt_clean_no_induction.duplicated(subset = ["PATIENT_ID","date","DMT_START"],keep = "last")) & (dmt_clean_no_induction.DMT_START==True)].index
    dmt_clean_no_induction.drop(index_to_drop,inplace = True)

    #Computing the set of active drugs at a given time.
    print("Computing DMT history.... Please stand by....")
    keep_last_valid_current = [True, True] #if true, will drop the previous dmts (only keep the last valid one)
    prefix  = ["_ind", ""]
    for dmt_i, dmt_subset in enumerate([dmt_clean_induction, dmt_clean_no_induction]):
       
        if prefix[dmt_i] =="_ind": #For the induction, only keep the DMT Start as they keep being active forever
            dmt_subset = dmt_subset.loc[dmt_subset.DMT_START==True]

        dmt_clean_hist = data_preproc.applyParallel(dmt_subset.groupby(["PATIENT_ID"]), current_DMT_fun, n_jobs = multiprocessing.cpu_count())        
        dmt_clean_hist = dmt_clean_hist.reset_index(drop=True)

        if keep_last_valid_current[dmt_i]:
            dmt_clean_hist["dmt_current_set"] = dmt_clean_hist["dmt_current"].apply(lambda x: set([x_ for x_ in x if x_[0]!="-"][-1:])) #unmatched DMTs are dropped and only last current one is kept
        else:
            dmt_clean_hist["dmt_current_set"] = dmt_clean_hist["dmt_current"].apply(lambda x: set([x_ for x_ in x if x_[0]!="-"])) #unmatched DMTs are dropped.

        dmt_clean_hist.sort_values(by = ["PATIENT_ID","date"], inplace = True)
        dmt_clean_hist.drop_duplicates(subset = ["PATIENT_ID","date"], keep = "last", inplace = True)

        dmt_clean_agg = dmt_subset.groupby(["PATIENT_ID","date","DMT_START","DMT_END"]).agg({"processed_dmt": lambda x : x.str.cat(sep=";")}).reset_index()

        dmt_start = dmt_clean_agg.loc[dmt_clean_agg.DMT_START==True,["PATIENT_ID","date","processed_dmt"]].copy().rename(columns = {"processed_dmt":"start_dmt"})
        dmt_end = dmt_clean_agg.loc[dmt_clean_agg.DMT_END==True,["PATIENT_ID","date","processed_dmt"]].copy().rename(columns = {"processed_dmt":"end_dmt"})
        dmt_joint = pd.merge(dmt_start,dmt_end,how= "outer",on = ["PATIENT_ID","date"])

        assert dmt_joint.duplicated(["PATIENT_ID","date"]).sum() == 0
       
        dmt_joint.rename(columns = {c:c+prefix[dmt_i] for c in dmt_joint.columns if c not in ["PATIENT_ID","date"]}, inplace = True)

        df = pd.merge(df,dmt_joint, how = "outer", on = ["PATIENT_ID","date"])
    
        #merging the histories.
        dmt_clean_hist = dmt_clean_hist[["PATIENT_ID","date","dmt_current_set"]]
        dmt_clean_hist.rename(columns = {c:c+prefix[dmt_i] for c in dmt_clean_hist.columns if c not in ["PATIENT_ID","date"]}, inplace = True)
        df = pd.merge(df, dmt_clean_hist, how = "outer", on = ["PATIENT_ID","date"])

    df = clean_dmt_current_set(df,suffix = "_ind")
    df = clean_dmt_current_set(df,suffix = "")
    
    df["dmt_current_set_overall"] = df[["dmt_current_set","dmt_current_set_ind"]].apply(create_overall_current_set,axis=1)
   
    assert df.PATIENT_ID.nunique() == init_n_pats
    return df.copy()

def create_overall_current_set(x):
    """
    Merge and regular DMT and Induction current sets
    """
    y = x["dmt_current_set"]
    z = x["dmt_current_set_ind"]
    if type(y)==set:
        if type(z)==set:
            return set.union(y,z)
        else:
            return y
    else:
        if type(z)==set:
            return z
        else:
            return np.nan

def last_visit_computation(x,rel):
    """
    Routine to compute the last visit date of a patient
    """
    df_ = rel.loc[rel.PATIENT_ID==x["PATIENT_ID"]]["DATE_OF_ONSET"].copy()
    delta = (df_ - x["date"]).dt.days
    if delta[delta<=0].shape[0]>0:
        return df_.loc[delta[delta<=0].idxmax()]
    else:
        return None


def clean_mscourse(df_in):
    df = df_in.copy()
    df.sort_values(by=["PATIENT_ID","date"],inplace = True)
    df = df.reset_index().drop(columns = ["index"])

    df["MSCOURSE_AT_VISIT"] = df.groupby("PATIENT_ID")["MSCOURSE_AT_VISIT"].ffill()

    return df


def clean_vars(df_in):
    # same as clean_mscourse, but with bfill as well as ffill
    df = df_in.copy()
    df.sort_values(by=["PATIENT_ID","date"],inplace = True)
    df = df.reset_index().drop(columns = ["index"])

    vars_fill = ['gender', 'age_at_onset_years_computed'] 

    for varstr in vars_fill:
        df[varstr] = df.groupby("PATIENT_ID")[varstr].ffill()
        df[varstr] = df.groupby("PATIENT_ID")[varstr].bfill()

    return df


def clean_static_covariates(df_in, cov_list, no_nans_list = []):
    df = df_in.copy()
    
    for cov in cov_list:
        df_gp = df.loc[~df[cov].isna()].groupby("PATIENT_ID")[cov]
        assert (df_gp.nunique()==1).sum() == len(df_gp.nunique()) # CHeck that no distinct unique values per patient
        cov_dict = dict(df_gp.first())
        df[cov] = df["PATIENT_ID"].map(cov_dict)
    
    for cov in no_nans_list:
        previous_n_pats = df.PATIENT_ID.nunique()
        previous_pat_ids = pd.unique(df.PATIENT_ID)
        df = df.loc[~df[cov].isna()].copy()
        updated_n_pats = df.PATIENT_ID.nunique()
        updated_pat_ids = pd.unique(df.PATIENT_ID)
        
        # print("patient IDs removed because of " + str(cov))
        # print(np.setdiff1d(previous_pat_ids, updated_pat_ids))
        print(f"Removed {updated_n_pats-previous_n_pats} Patients due to Nans in field {cov} ")

    return df

def slice_based_covariates(df_final):

    # KFS variables
    kfs_cols = [c for c in df_final.columns if "KFS" in c]
    for kfs_col in kfs_cols:
        df_ = df_final.loc[(df_final.Time<=0) & (~df_final[kfs_col].isna())] 
        kfs_dict = dict(df_.groupby("slice_id")[kfs_col].last())
        df_final["last_"+kfs_col+"_at_0"] = df_final["slice_id"].map(kfs_dict)

    
    # MSCOURSE AT 0
    mscourse_at_0_dict = dict(df_final.loc[df_final.Time==0].groupby(["slice_id"])["MSCOURSE_AT_VISIT"].first())
    df_final["MSCOURSE_at_0"] = df_final["slice_id"].map(mscourse_at_0_dict)

    print(df_final.shape)    

    # remove slices where the MSCourse is not defined at time of prediction
    df_final_remove = df_final[df_final["MSCOURSE_at_0"].isnull()].copy()
    arr_filter = np.asarray(df_final_remove['slice_id'])
    df_final = df_final.drop(df_final.loc[df_final["slice_id"].isin(arr_filter)].index).copy()
    del df_final_remove

    print(df_final.shape)


    # AGE AT 0
    birth_date_gb = df_final.loc[df_final.Time==0].groupby(["slice_id"])[["date","birth_date"]].first()
    birth_date_gb["age_at_time0_years"] = (birth_date_gb["date"]-birth_date_gb["birth_date"]).dt.days / 365
    age_at_0_dict = dict(birth_date_gb["age_at_time0_years"])
    df_final["age_at_0"] = df_final["slice_id"].map(age_at_0_dict)

    # DISEASE_DURATION AT 0
    duration_gb  = df_final.loc[df_final.Time==0].groupby(["slice_id"])[["date","onset_date"]].first()
    duration_gb["disease_duration_at_0_years"]  = (duration_gb["date"] - duration_gb["onset_date"]).dt.days/365
    duration_dict = dict(duration_gb["disease_duration_at_0_years"])
    df_final["disease_duration_at_0_years"] = df_final["slice_id"].map(duration_dict)

    # Last DMT at 0
    dmt_gb = df_final.loc[df_final.Time==0].groupby("slice_id")[["date","dmt_current_set"]].first()
    dmt_dict = dict(dmt_gb["dmt_current_set"])
    df_final["dmt_at_0"] =  df_final["slice_id"].map(dmt_dict)


    df_final["dmt_at_0"] = df_final["dmt_at_0"].map(group_current_dmt,na_action="ignore") 
    df_final["dmt_current_set"] = df_final["dmt_current_set"].map(group_current_dmt,na_action = "ignore")

    df_final["start_dmt"] = df_final["start_dmt"].map(group_start_end_dmt, na_action = "ignore")
    df_final["end_dmt"] = df_final["end_dmt"].map(group_start_end_dmt, na_action = "ignore")
    
    # Last DMT induction at 0
    df_final["dmt_current_ind"] = df_final["dmt_current_set_ind"].apply(set_to_str_first)
    
    dmt_ind_gb = df_final.loc[df_final.Time==0].groupby("slice_id")[["date", "dmt_current_ind"]].first()
    dmt_ind_dict = dict(dmt_ind_gb["dmt_current_ind"])
    df_final["dmt_ind_at_0"] = df_final["slice_id"].map(dmt_ind_dict)
    df_final["dmt_ind_at_0"] = df_final["dmt_ind_at_0"].map(group_current_dmt,na_action="ignore") 
    
    df_final.drop(columns = ["dmt_current_ind"],inplace = True)
    
    return df_final

def set_to_str_first(x):
    if type(x)==set:
        return list(x)
    else:
        return x
    

def get_dmt_group_dict():

    map_dict = {"Interferon-beta": "Mild", "Teriflunomide": "Mild", "Glatiramer-Acetate": "Mild", "Fingolimod":"Moderate", "Dimethyl-Fumarate":"Moderate", "Cladribine": "Moderate", "Siponimod": "Moderate", "Daclizumab":"Moderate", "Alemtuzumab":"High", "Rituximab": "High", "Ocrelizumab": "High", "Natalizumab":"High", "Mitoxandrone":"High_Induction", "Azathioprine": "Mild", "Methotrexate": "Mild", "Cyclophosphamide": "High_Induction"}
    remove_list = ["Immuno-Globulin", "Gluco-corticoid"]

    return map_dict, remove_list


def group_start_end_dmt(dmt_set_in):

    dmt_set_list = dmt_set_in.split(";")

    map_dict, remove_list = get_dmt_group_dict()

    mapped_list = [map_dict[dmt_element] for dmt_element in dmt_set_list if dmt_element not in remove_list]

    return mapped_list


def group_current_dmt(dmt_set_in):
   
    map_dict, remove_list = get_dmt_group_dict()
    
    mapped_list = [map_dict[dmt_element] for dmt_element in dmt_set_in if dmt_element not in remove_list]

    return mapped_list

def bool_fun_treat(dmt_set_in,valid_values = ["Mild","Moderate","High"]):
    for el in valid_values:
        if el in dmt_set_in:
            return True

def compute_extra_variables(df_in):
    df = df_in.copy()

    df["dmt_start_grouped"]  = df.start_dmt.map(group_start_end_dmt,na_action = "ignore")
    df["is_treatment_no_ind"] = df.dmt_start_grouped.map(lambda x : bool_fun_treat(x,["Mild","Moderate","High"]),na_action = "ignore")
    df["is_high_treatment_no_ind"] = df.dmt_start_grouped.map(lambda x : bool_fun_treat(x,["High"]),na_action = "ignore")
    
    df["dmt_start_grouped_ind"]  = df.start_dmt_ind.map(group_start_end_dmt,na_action = "ignore")
    df["is_high_treatment_ind"] = df.dmt_start_grouped_ind.map(lambda x : bool_fun_treat(x,["High_Induction"]),na_action = "ignore")

    df["is_treatment"] = False
    df["is_high_treatment"] = False
    df.loc[((df.is_treatment_no_ind) | (df.is_high_treatment_ind)),"is_treatment"] = True
    df.loc[((df.is_high_treatment_no_ind) | (df.is_high_treatment_ind)),"is_high_treatment"] = True

    first_treatment_date = df.loc[df.is_treatment==True].groupby("PATIENT_ID").date.min()
    first_high_treatment_date = df.loc[df.is_high_treatment==True].groupby("PATIENT_ID").date.min()
    onset_date = df.groupby("PATIENT_ID").onset_date.first()
   
    df["onset_date"] = df.PATIENT_ID.map(onset_date)
    df["first_treatment_date"] = df.PATIENT_ID.map(first_treatment_date)
    df["first_high_treatment_date"] = df.PATIENT_ID.map(first_high_treatment_date)
    
    df["days_between_onset_and_first_treatment"] = (df["first_treatment_date"]-df["onset_date"]).dt.days
    df["days_between_onset_and_first_high_treatment"] = (df["first_high_treatment_date"]-df["onset_date"]).dt.days

    # The time since onset and first treatment should be 0 before the first treatment is given
    df["time_since_onset"] = (df["date"]-df["onset_date"]).dt.days

    keep_time_since_onset = False
    if keep_time_since_onset : # before the first treatment, the days between onset and first treatment is set to the time since onset:
        df.loc[df.time_since_onset<df.days_between_onset_and_first_treatment,"days_between_onset_and_first_treatment"] = df.loc[df.time_since_onset<df.days_between_onset_and_first_treatment,"time_since_onset"] 
        df.loc[df.time_since_onset<df.days_between_onset_and_first_high_treatment,"days_between_onset_and_first_high_treatment"] = df.loc[df.time_since_onset<df.days_between_onset_and_first_high_treatment,"time_since_onset"] 
    
        # Nans should be filled with time since onset
        df.loc[df.days_between_onset_and_first_treatment.isna(),"days_between_onset_and_first_treatment"] = df.loc[df.days_between_onset_and_first_treatment.isna(),"time_since_onset"]
        df.loc[df.days_between_onset_and_first_high_treatment.isna(),"days_between_onset_and_first_high_treatment"] = df.loc[df.days_between_onset_and_first_high_treatment.isna(),"time_since_onset"]
    else: #we set the latter to nan
        df.loc[df.time_since_onset<df.days_between_onset_and_first_treatment,"days_between_onset_and_first_treatment"] = np.nan
        df.loc[df.time_since_onset<df.days_between_onset_and_first_high_treatment,"days_between_onset_and_first_high_treatment"] = np.nan

    df.drop(columns = ["first_treatment_date","first_high_treatment_date","dmt_start_grouped","dmt_start_grouped_ind","is_treatment","is_high_treatment","is_treatment_no_ind","is_high_treatment_no_ind","is_high_treatment_ind","time_since_onset"],inplace = True)
    return df


def compute_time_on_dmt(df):
    """
    Compute the number of days spent on a DMT (variable = days_treat)
    """
    print("Computing the time under dmt for each patient. Can take several minutes.")
    df["dmt_current_set_group"] = df["dmt_current_set_overall"].map(group_current_dmt,na_action = "ignore").astype(str)
    df["is_treat"] = (df["dmt_current_set_group"].fillna("[]")!="[]").astype(int)
    df.loc[df.dmt_current_set_group=="nan","is_treat"] = 0

    def compute_time_on_dmt_(df_in):
        df = df_in.sort_values(by=["date"]).copy()
        df["days_on_treat"] = (df["date"].diff(-1).dt.days*-1 * df["is_treat"]).cumsum().shift()
        df["days_no_treat"] =  (df["date"].diff(-1).dt.days*-1 * (1-df["is_treat"])).cumsum().shift()
        return df

    #df["days_treat"] = df.groupby("PATIENT_ID")[["date","is_treat"]].apply(lambda x : compute_time_on_dmt_(x))
    
    df = data_preproc.applyParallel(df.groupby(["PATIENT_ID"]), compute_time_on_dmt_, n_jobs = multiprocessing.cpu_count())
    df.drop(columns = ["dmt_current_set_group","is_treat"],inplace = True)
    df["ratio_on_treat"] = df["days_on_treat"] / (df["days_on_treat"] + df["days_no_treat"])
    print("Done")
    return df

def time_since_fampridine(df):
    fampridine_dict = df.groupby("PATIENT_ID")["FAMPRIDINE_START_DATE"].min()
    df["first_fampridine_date"] = df["PATIENT_ID"].map(dict(fampridine_dict))
    df["days_since_fampridine"] = (df["date"]-df["first_fampridine_date"]).dt.days
    df.loc[df.days_since_fampridine<0,"days_since_fampridine"] = np.nan 
    return df

def get_string(el):
    if isinstance(el, list):
        return el[0]
    else:
        return np.NaN


if __name__=="__main__":

    start_time = time.time()

    #Do the cleaning here.
    parser = argparse.ArgumentParser(description='Cleaning of the MSBase2020 dataset and slices generation')
    parser.add_argument('--overwrite', default = False, action = 'store_true',
                    help='to overwrite the cleaning process and directly go to slice generation')
    parser.add_argument('--debug', default = False, action = 'store_true',
                    help='to bypass the slicing and load the previous df')
    parser.add_argument('--num_threads', default = 20, type = int, help = 'number of threads to use for the pre-processing')
    parser.add_argument('--num_visits', default = 3, type = int, help = 'minimum number of visits in the observation window')
    parser.add_argument('--only_longitudinal', default = False, action = 'store_true',
                    help='If true, computes only the longitudinal version of the dataset')
    parser.add_argument('--bypass_slicing',default = False, action = "store_true", help = "directly loads a previous version of the df with slices")

    args = parser.parse_args()

    overwrite = args.overwrite

    # --- Compute the number of valid intervals/slices ---
    num_years_follow_up = 3.25
    min_visits_follow_up = args.num_visits
    pred_horizon = 2
    window_tol = 0.5  # currently not used
    ref_end = True
    confirmation_window = 182
    max_slices_per_patient = 0


    patients_df_name = DATA_DIR+"/MSBase2020/PATIENT.csv"
    visits_df_name = DATA_DIR+"/MSBase2020/VISITS.csv"
    relapse_df_name = DATA_DIR+"/MSBase2020/RELAPSE.csv"
    mri_df_name     = DATA_DIR + "/MSBase2020/MRI.csv"
    dmt_df_name     = DATA_DIR + "/MSBase2020/TREATMENT.csv"
    outdir = DATA_DIR+f"/MSBase2020/Cleaned/"
    outdir_specific = DATA_DIR+f"/MSBase2020/Cleaned/{min_visits_follow_up}_visits/"

    os.makedirs(outdir_specific,exist_ok=True)

    num_threads = args.num_threads

        
    static_cov_list = ["gender", "age_at_onset_years_computed"] + ["FIRST_SYMPTOM_OPTIC_PATHWAYS","FIRST_SYMPTOM_BRAINTSTEM","FIRST_SYMPTOM_SPINAL_CORD","FIRST_SYMPTOM_SUPRATENTORIAL"] #list of static covariates to handle
    # no_nans_list = ["gender", "age_at_onset_years_computed","MSCOURSE_AT_VISIT"] #Remove patients with nans in this static variables list
    no_nans_list = ["gender", "age_at_onset_years_computed"] #Remove patients with nans in this static variables list

    if (not os.path.isfile(outdir+"df_with_relapses.pkl")) or (overwrite):
    
        print(f"Cleaning the MSBase2020 data ....")
        # Read patients csv
        df = pd.read_csv(patients_df_name,encoding='latin1')

        # Process the patient csv
        process_patients(patients_df = patients_df_name, outdir = outdir)

        # Process the visits
        patients_pkl_path = outdir+"patients_clean.pkl"
        df_clean = process_visits(visits_df = visits_df_name, clean_pats_pkl= patients_pkl_path)
        #--------- Merging with relapses -----
        print("Merging with the relapses, MRI and DMT")
         
        df = merge_with_relapses_all_separate(df_clean, relapse_df_name)
        df = merge_with_MRI(df, mri_df_name)
        df = merge_with_fampridine(df, dmt_df_name) 
        df = merge_with_DMT(df, dmt_df_name)

        # print(df[df.PATIENT_ID == 'AR-003-0015'][['Relapse','gender','MSCOURSE_AT_VISIT','age_at_onset_years_computed']]) 
        df.sort_values(by = ["PATIENT_ID","date"], inplace = True)

        # assert df.duplicated(["PATIENT_ID","date"]).sum() == 0

        # Cleaning of the MSCOURSE_AT_VISIT (ffil)
        df = clean_mscourse(df)
        # Clean gender and age_at_onset (ffil and bfil)
        df = clean_vars(df)
        
        # print(df[df.PATIENT_ID == 'AR-003-0015'][['Relapse','gender','MSCOURSE_AT_VISIT','age_at_onset_years_computed']]) 
        # Cleaning of the static variables.    
        # REMOVE patients that do not pass some basic quality standard requirements (gender and age_at_onset)
        # this is necessary because some patients their fampridine or DMT visits are added,
        # even though they do not pass the inclusion criteria
        # this is not an elegant way to deal with this, but it works
        df = clean_static_covariates(df, static_cov_list, no_nans_list)

        # Cleaning of the static variables.
        df = compute_time_on_dmt(df)
      
        print("Computing time since fampridine")
        df = time_since_fampridine(df)
 
        print('------------- number of patients after all preprocessijg  ---------------------------------------------')
        print(df.PATIENT_ID.nunique())
        print("# of EDSS visits")
        print(np.sum(df['EDSS'].count())) 
        print('-------------------------------------------------------------------------------------------------------')
        print('')    

       
        print("Saving the processed df...")
        df.to_pickle(outdir+"df_with_relapses.pkl")
        print("Saved the (cleaned) merging with relapses ! ")
    #-------------------------------------
    else:
        print("Skipping cleaning step ---- run msbase_clean.py --overwrite if you want to re-do the cleaning step !")
        df = pd.read_pickle(outdir+"df_with_relapses.pkl")
        date_cols = [c for c in df.columns if "date" in c]
        for date_col in date_cols:
            df[date_col] = pd.to_datetime(df[date_col], format='%d.%m.%Y')
   
    if not args.bypass_slicing:
        # replace PR with PP for the MSCOURSE
        df["MSCOURSE_AT_VISIT"].replace({"PR": "PP"}, inplace=True) 
        # Use only relevant columns
        df = df[["PATIENT_ID","birth_date","gender","Education_status","age_at_onset_years_computed","onset_date",
                "EDSS","KFS_1", "KFS_2", "KFS_3", "KFS_4", "KFS_5", "KFS_6", "KFS_7", "KFS_AMBULATION","DURATION_OF_MS_AT_VISIT","MSCOURSE_AT_VISIT",
                "first_EDSS","first_visit_date","last_EDSS", "last_visit_date","date","last_relapse_date","time_since_relapse","Relapse", 
                "start_dmt","end_dmt","dmt_current_set","start_dmt_ind","end_dmt_ind","dmt_current_set_ind","dmt_current_set_overall",
                "cumulative_relapse",
                "FIRST_SYMPTOM_OPTIC_PATHWAYS","FIRST_SYMPTOM_BRAINTSTEM","FIRST_SYMPTOM_SPINAL_CORD","FIRST_SYMPTOM_SUPRATENTORIAL",
                "treatment",
                "FAMPRIDINE_START_DATE",
                "BOWEL_BLADDER", "cerebellum", "VISUAL_FUNCTION", "SENSORY_FUNCTION", "PYRAMIDAL_TRACT", "brainstem",
                "days_since_fampridine","days_on_treat","days_no_treat","ratio_on_treat"]]
        
        df = compute_extra_variables(df)

        

        def valid_set_fun(df_):
            return data_preproc.count_valid_intervals(df_,num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol,confirmation_window = confirmation_window, ref_end = ref_end, return_valid_set = True, max_count = max_slices_per_patient)[2]
     
        if args.debug:
            df_final = pd.read_csv(outdir+"df_processed.csv")
            date_cols = [c for c in df_final.columns if "date" in c]
            for date_col in date_cols:
                df_final[date_col] = pd.to_datetime(df_final[date_col], format='%d.%m.%Y')
        else:
            print("Computing the slices...")
            df_final, label_final = data_preproc.get_slices(df,num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol,confirmation_window = confirmation_window, ref_end = ref_end, num_threads = num_threads, valid_set_fun = valid_set_fun, test_mode = False)
            
        df_final.to_pickle(outdir+"checkpoint_after_slices.pkl")
    else:
        print("Loaded sliced dataset from checkpoint")
        df_final = pd.read_pickle(outdir+"checkpoint_after_slices.pkl")
   
    # Add the slice based covariates
    print("Computing the slice based covariates...")
    df_final = slice_based_covariates(df_final) #inplace 0
 
    # remove patients that are CIS at prediction time 
    df_final.drop(df_final.loc[df_final['MSCOURSE_at_0']=='CIS'].index, inplace=True) 
    #df_final.drop(df_final.loc[df_final['MSCOURSE_at_0'].isna()].index, inplace=True) 



    #Dropping the future of the dataset slices (time>0) as it is not used in the modelling
    df_final = df_final.loc[df_final.Time<=0]
    #label_final = df_final.groupby(["slice_id","PATIENT_ID"])["label"].any().reset_index()

    print('final number of samples and patients: ')
    print(df_final['slice_id'].nunique())
    print(df_final['PATIENT_ID'].nunique())
     
    print("Saving...")
    df_final.to_pickle(outdir_specific+"df_processed.pkl")
    label_final.to_pickle(outdir_specific+"label_processed.pkl")
    print("Done")
 
    print("Removing previous datasets...")
    if os.path.isdir(outdir_specific+"Static"):
        shutil.rmtree(outdir_specific+"Static")
    if os.path.isdir(outdir_specific+"Dynamic"):
        shutil.rmtree(outdir_specific+"Dynamic")
    if os.path.isdir(outdir_specific+"Longitudinal"):
        shutil.rmtree(outdir_specific+"Longitudinal")

    print("Generating folds...")
    groupby_key = "Center" # Center or PATIENT_ID
    test_prop = 0.2
    val_prop = 0.2

    folds_MSBase.create_folds(groupby_key, val_prop, test_prop, indir = outdir_specific, outdir = outdir_specific)
   
    end_time = time.time()
 
    print(f"Done. Total time for the pre-processing : {(end_time-start_time)//60} minutes and {((end_time-start_time)%60):.2f} seconds.")
    print(f"You also produced {(end_time-start_time)/10000:.2f} kg of CO2. Go plant a tree.")

