from ms_benchmark.data_utils import prepare_msbase_static_dataset, prepare_msbase_dynamic_dataset, prepare_msbase_longitudinal_dataset
from ms_benchmark import DATA_DIR
import os
import pickle 
import pandas as pd
import argparse


if __name__=="__main__":
     #Do the cleaning here.
    parser = argparse.ArgumentParser(description='Creation of the static, dynamic and longitudinal datasets')
    parser.add_argument('--num_visits', default = 3, type = int, help = 'minimum number of visits in the observation period')
    args = parser.parse_args()

    num_visits = args.num_visits
    indir = DATA_DIR + f"/MSBase2020/Cleaned/{num_visits}_visits/"
    outdir = indir
    df_processed = pd.read_pickle(indir + "df_processed.pkl").sort_values(by = ["slice_id","Time"])


    print("Creating static dataset...")
    df, cov_cols, continuous_cols = prepare_msbase_static_dataset(df = df_processed, no_dmt = False, include_country = True, include_clinic = True)
    os.makedirs(outdir + "Static/",exist_ok = True)
    df.to_pickle(outdir + "Static/df.pkl")

    with open(outdir + "Static/cov_cols.pkl", "wb") as fp:   #Pickling
        pickle.dump(cov_cols, fp)

    with open(outdir + "Static/continuous_cols.pkl", "wb") as fp:   #Pickling
        pickle.dump(continuous_cols, fp)

    print("Creating dynamic dataset...")
    df, cov_cols, continuous_cols = prepare_msbase_dynamic_dataset(df = df_processed, no_dmt = False)
    os.makedirs(outdir + "Dynamic/",exist_ok = True)
    df.to_pickle(outdir + "Dynamic/df.pkl")

    with open(outdir + "Dynamic/cov_cols.pkl", "wb") as fp:   #Pickling
        pickle.dump(cov_cols, fp)

    with open(outdir + "Dynamic/continuous_cols.pkl", "wb") as fp:   #Pickling
        pickle.dump(continuous_cols, fp)


    print("Creating longitudinal dataset...")
    df_longitudinal, out_cols , mask_cols, continuous_cols, always_observed_cols = prepare_msbase_longitudinal_dataset(df_processed)
    os.makedirs(outdir + "Longitudinal/",exist_ok = True)
    print("Saving longitudinal dataframe ...")

    longitudinal_dir = outdir + "Longitudinal/" 
    df_longitudinal.to_pickle(longitudinal_dir + "df_longitudinal.pkl")
    with open(longitudinal_dir +"continuous_longitudinal_columns.pkl", 'wb') as f:
        pickle.dump(continuous_cols, f)
    with open(longitudinal_dir+"all_longitudinal_columns.pkl", 'wb') as f:
        pickle.dump(out_cols, f)
    with open(longitudinal_dir+"always_observed_columns.pkl", 'wb') as f: #longitudinal columns names that are always observed (computed)
        pickle.dump(always_observed_cols, f)
    with open(longitudinal_dir+"mask_longitudinal_columns.pkl", 'wb') as f: #longitudinal columns names that are always observed (computed)
        pickle.dump(mask_cols, f)

    print("Saved.")

