from ms_benchmark import DATA_DIR
import pandas as pd
import numpy as np
import os
from ms_benchmark.data_preproc.data_preproc_utils import stratified_group_train_test_split


if __name__ == "__main__":

    df_full = pd.read_csv(DATA_DIR+"/EP/Cleaned/df_processed.csv")
    label_df = pd.read_csv(DATA_DIR+"/EP/Cleaned/label_processed.csv")


    seeds = [42,43,44,45,46]
    for fold in range(5):
        label_train, label_test = stratified_group_train_test_split(label_df, group = "PATIENT_ID",stratify_by="label",test_size = 0.1, seed = seeds[fold])
        label_train, label_val = stratified_group_train_test_split(label_train, group = "PATIENT_ID",stratify_by="label",test_size = 0.2, seed = seeds[fold])

        # Check for no patient leak -------
        df_train = df_full.loc[df_full.slice_id.isin(label_train.slice_id.unique())]
        df_val = df_full.loc[df_full.slice_id.isin(label_val.slice_id.unique())]
        df_test = df_full.loc[df_full.slice_id.isin(label_test.slice_id.unique())]

        assert np.intersect1d(df_train["PATIENT_ID"].unique(),(df_test["PATIENT_ID"].unique())).shape[0] == 0
        assert np.intersect1d(df_train["PATIENT_ID"].unique(),(df_val["PATIENT_ID"].unique())).shape[0] == 0
        assert np.intersect1d(df_val["PATIENT_ID"].unique(),(df_test["PATIENT_ID"].unique())).shape[0] == 0

        # -----------------------------------

        os.makedirs(DATA_DIR+f"/EP/Cleaned/folds/fold_{fold}/",exist_ok=True)    
        label_train.to_csv(DATA_DIR+f"/EP/Cleaned/folds/fold_{fold}/train_idx.csv",index=False)
        label_val.to_csv(DATA_DIR+f"/EP/Cleaned/folds/fold_{fold}/val_idx.csv",index=False)
        label_test.to_csv(DATA_DIR+f"/EP/Cleaned/folds/fold_{fold}/test_idx.csv",index=False)



