import pandas as pd
from datetime import timedelta
import tqdm
import numpy as np
import multiprocessing
import itertools
from multiprocessing import Pool
from joblib import Parallel, delayed
from datetime import datetime

from sklearn.model_selection import train_test_split

def stratified_group_train_test_split(
    samples: pd.DataFrame, group: str, stratify_by: str, test_size: float,
seed = 421):
    groups = samples[group].drop_duplicates()
    stratify = samples.drop_duplicates(group)[stratify_by].to_numpy()
    groups_train, groups_test = train_test_split(groups, stratify=stratify, test_size=test_size, random_state = seed)

    samples_train = samples.loc[lambda d: d[group].isin(groups_train)]
    samples_test = samples.loc[lambda d: d[group].isin(groups_test)]
    
    assert np.intersect1d(samples_train[group].unique(),samples_test[group].unique()).shape[0] == 0

    return samples_train, samples_test


def applyParallel(dfGrouped, func, n_jobs = 10):
    retLst = Parallel(n_jobs=n_jobs)(delayed(func)(group) for name, group in dfGrouped)
    return pd.concat(retLst)


def is_valid(df_in, ref_date, num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol, confirmation_window, ref_end = False, slice_id = 0):
    """
    Routine to check if a slice is valid.

    ref_date is the start or the end of the follow_up period depending if ref_end is True or False
    ref_end : true if ref_date is the end of the follow-up period.
    """
    if ref_date< datetime(1990,1,1):
        return False, False, None

    if ref_end:
        #Check if the last value (reference) has an edss value (to compute progression)
        if df_in.loc[(df_in.date==ref_date)&(~df_in.EDSS.isna())].shape[0]==0:
            return False, False, None
        
        start_follow_up = ref_date - timedelta(days=365*num_years_follow_up)
        end_follow_up = ref_date
    else:
        start_follow_up = ref_date
        end_follow_up = ref_date + timedelta(days=365*num_years_follow_up)
    
    if df_in.last_visit_date.iloc[0]<end_follow_up:
        return False, True, None
   
    #df = df_in.copy()
    df = df_in.sort_values(by=["PATIENT_ID","date"]).copy()
    df["Time"] = (df.date - ref_date).dt.days
    df["slice_id"] = slice_id
    
    #Number of EDSS visits in the follow_up period.
    
    df_sub = df.loc[(df.date>=start_follow_up)].copy()

    num_visits = df_sub.loc[(df_sub.date<end_follow_up)&(~df_sub.EDSS.isna()),"date"].nunique()
    
    if num_visits<min_visits_follow_up:
        return False, False, None
   
 
    target_date = end_follow_up + timedelta(days=365*pred_horizon)
    
    # if df.last_visit_date.iloc[0]<target_date:
    #     return False, True, None
    
    # NEW CHANGE AUGUST 2021
    # if there is no progression, there is no confirmation of progression needed
    # having one EDSS measurement after 1 year after end_follow_up that indicates no progression is therefore valid
    # therefore, we have to make sure that at least one measurement occurs after target_date
    if df.last_visit_date.iloc[0]<=end_follow_up:
        return False, True, None
    
    #EDSS between end_follow_up and target_date
    valid_edss_pre_target = df_sub.loc[(df_sub.date> end_follow_up)&(df_sub.date<target_date)&(~df_sub.EDSS.isna()),["EDSS","date","last_relapse_date"]].copy()
    #EDSS after target_date
    valid_edss_post_target = df_sub.loc[(df_sub.date>= target_date)&(~df_sub.EDSS.isna()),["EDSS","date","last_relapse_date"]].copy()
    
    # We want to remove all the edss that are too close to relapse (less than 1 month after a relapse) !
    # for the EDSS values between t=0 and t=2, it is allowed to have a relapse before the EDSS measurement.
    # relapses close to EDSS measurements are only relevant for confirming progression
    valid_edss_post_target = valid_edss_post_target.loc[((valid_edss_post_target.date-valid_edss_post_target.last_relapse_date).dt.days>30) | (valid_edss_post_target.last_relapse_date.isna())].copy()
    
    valid_edss_pre_target = valid_edss_pre_target.drop_duplicates(subset = ["date"])
    valid_edss_post_target = valid_edss_post_target.drop_duplicates(subset = ["date"])

    num_edss_pre_target = valid_edss_pre_target.date.nunique()
    if num_edss_pre_target < 1:
        return False, False, None

    valid_edss_pre_target.sort_values(by="date",inplace = True)
    closest_edss_to_target = valid_edss_pre_target.iloc[-1]

    valid_edss_post_target.sort_values(by="date",inplace = True)
    num_trailing_edss = valid_edss_post_target.date.nunique()

    # NEW CHANGE AUGUST 2021
    # we only need trailing edss if we need to confirm progression
    # this needs to be selected upon when there is an unconfirmed progression target
        

    # last edss in the follow-up period, this code is more involved but also valid if t=0 is not the reference time
    # df_last_edss = df_sub.loc[(df_sub.date>=start_follow_up)&(df_sub.date<=end_follow_up)&(~df_sub.EDSS.isna()),["EDSS","date"]].copy()        
    # df_last_edss = df_last_edss.sort_values(by=["date"])
    # df_last_edss = df_last_edss.drop_duplicates(subset = ["date"], keep = "last")
    # last_edss = df_last_edss.iloc[-1]["EDSS"]
    # only valid when t=0 is the reference point. But this is our final choice, so this code makes more sense 
    df_last_edss = df_sub.loc[(df_sub.date==ref_date)&(~df_sub.EDSS.isna())].copy()    
    last_edss = df_last_edss.iloc[0]["EDSS"]    
    df_sub["last_edss"] = last_edss
    df["last_edss"] = last_edss

    if last_edss ==0:
        threshold = 1.5
    elif last_edss<=5.5:
        threshold = 1 + last_edss
    elif last_edss > 5.5:
        threshold = 0.5  + last_edss
    else:
        raise("Error : last edss is not valid.")
        
    # if np.isnan(min_edss_confirmation_window):
    #     raise("Error missing confirmation EDSS")

    if (closest_edss_to_target.EDSS >= threshold):
        if num_trailing_edss == 0:
            return False, False, None
        else:
            # first edss measurement after the target.
            closest_edss_post_target = valid_edss_post_target.iloc[0]
        
            # Window is max(confirmation_window, closest_edss_post_target-closest_edss_to_target)
            valid_edss_confirmation_window = valid_edss_post_target.loc[((valid_edss_post_target.date <= (closest_edss_to_target.date + timedelta(days = int(confirmation_window)))) | (valid_edss_post_target.date<= closest_edss_post_target.date) )]
            min_edss_confirmation_window = valid_edss_confirmation_window["EDSS"].min()
            if min_edss_confirmation_window >= threshold:
                label = True
            else:
                label= False
    else:
        label = False

    df_sub["label"] = label
    df["label"] = label

    return True, False, df


def count_valid_intervals(df_in, num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol,  confirmation_window, return_valid_set = False, ref_end = False, max_count = 0):
    """
    max_count : to limit the number of slices per patient.
    If max_count == 0, there is no limit (so max_count is actually infinite)
    """
    count = 0
    df = df_in.copy()
    df = df.sort_values(by=["PATIENT_ID","date"])
    valid_set = []
    df_sub_list = []

    if max_count == 0:
        max_count = np.inf

    for patient_uid in tqdm.tqdm(df.PATIENT_ID.unique()):
        
        pat_count = 0
        
        df_pat = df.loc[df.PATIENT_ID==patient_uid]
        for ref_date in df.loc[df.PATIENT_ID==patient_uid,"date"].unique():
            ref_date = pd.Timestamp(ref_date)
            valid_interval, stop_current_patient, df_sub = is_valid(df_pat, ref_date, num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol, confirmation_window = confirmation_window, ref_end = ref_end, slice_id = count)
            if valid_interval:
                count += 1
                pat_count += 1
                df_sub_list.append(df_sub)
                if return_valid_set:
                    valid_set += [(patient_uid,ref_date)]
            if stop_current_patient:
                break

            if pat_count>=max_count:
                break

    if len(df_sub_list)>0:
        df_out = pd.concat(df_sub_list)
    else:
        df_out = None

    return count, valid_set, df_out


def get_slices(df, num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol, confirmation_window, ref_end = True, num_threads = 5, valid_set_fun = None, test_mode = False):
    
    ids = df.PATIENT_ID.unique()
    if test_mode: #only select a small subset of patients
        ids = ids[:100]
    ids_split = np.array_split(ids,100)
    df_list = [df.loc[df.PATIENT_ID.isin(split)] for split in ids_split]

    #def valid_set_fun(df_):
    #    return count_valid_intervals(df_, num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol, ref_end = ref_end, return_valid_set = True)[1]

    #valid_set_fun = get_valid_set_fun(num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol, ref_end = ref_end )

    if test_mode:
        sub_df_list = [valid_set_fun(df_) for df_ in df_list]
    else:
        with Pool(num_threads) as p:
            sub_df_list = p.map(valid_set_fun, df_list)

    offset_slice_id = 0
    for ix in range(len(sub_df_list)):
        if sub_df_list[ix] is not None:
            sub_df_list[ix]["slice_id"] = sub_df_list[ix]["slice_id"] + offset_slice_id  
            offset_slice_id = sub_df_list[ix]["slice_id"].max() + 1

    df_final = pd.concat(sub_df_list)

    label_final = df_final.groupby(["slice_id","PATIENT_ID"])["label"].any().reset_index()

    return df_final, label_final



if __name__=="__main__":
    #Do the cleaning here.

    patients_df_name = "~/Data/MS/MSBase2020/PATIENT.csv"
    visits_df_name = "~/Data/MS/MSBase2020/VISITS.csv"
    relapse_df_name = "~/Data/MS/MSBase2020/RELAPSE.csv"
    outdir = "~/Data/MS/MSBase2020/Cleaned/"

    num_threads = 10
    pre_comp = True

    if not pre_comp:
    
        # Read patients csv
        df = pd.read_csv(patients_df_name,encoding='latin1')

        # Process the patient csv
        process_patients(patients_df = patients_df_name, outdir = outdir)

        # Process the visits
        patients_pkl_path = outdir+"patients_clean.pkl"
        df_clean = process_visits(visits_df = visits_df_name, clean_pats_pkl= patients_pkl_path)

        #--------- Merging with relapses -----
        ids = df_clean.PATIENT_ID.unique()
        ids_split = np.array_split(ids,num_threads)
        df_list = [df_clean.loc[df_clean.PATIENT_ID.isin(split)] for split in ids_split]

        def preproc_fun(x):
            return merge_with_relapses(x,relapse_df_name)

        with Pool(num_threads) as p:
            df_list_ = p.map(preproc_fun, df_list)

        df = pd.concat(df_list_)
        df.to_csv(outdir+"df_with_relapses.csv",index=False)
        print("Saved the merging with relapses ! ")
    #-------------------------------------
    else:
        df = pd.read_csv(outdir+"df_with_relapses.csv")
        date_cols = [c for c in df.columns if "date" in c]
        for date_col in date_cols:
            df[date_col] = pd.to_datetime(df[date_col])


    # --- Compute the number of valid intervals/slices ---

    num_years_follow_up = 3
    min_visits_follow_up = 6
    pred_horizon = 2
    window_tol = 0.5
    ref_end = True

    df_final, label_final = get_slices(df, num_years_follow_up, min_visits_follow_up, pred_horizon, window_tol, ref_end = ref_end, num_threads = num_threads)
    
    df_final.to_csv(outdir+"df_processed.csv",index=False)
    label_final.to_csv(outdir+"label_processed.csv",index=False)

