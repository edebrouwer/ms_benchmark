import pandas as pd
import numpy as np
from ms_benchmark import DATA_DIR
from datetime import datetime

def clean_dmt_df(df_in):
    df = df_in.copy()
    df["dmt"] = df["dmt"].str.lower()

    #First remove NaN entries in dmt.

    print("Initial number of different DMTs : {}".format(df["dmt"].nunique()))
    df=df.drop(df.loc[df["dmt"].isnull()].index).copy()
    df["processed_dmt"]=df["dmt"]

    #Copaxone
    df.loc[df["dmt"].str.contains("copaxone",case=False),"processed_dmt"]="Glatiramer-Acetate"
    df.loc[df["dmt"].str.contains("glatiramer",case=False),"processed_dmt"]="Glatiramer-Acetate"
    df.loc[df["dmt"].str.contains("copax",case=False),"processed_dmt"]="Glatiramer-Acetate"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))

    #Lemtrada
    df.loc[df["dmt"].str.contains("Alemtuzumab",case=False),"processed_dmt"]="Alemtuzumab"
    df.loc[df["dmt"].str.contains("Lemtrada",case=False),"processed_dmt"]="Alemtuzumab"
    df.loc[df["dmt"].str.contains("alemtiz",case=False),"processed_dmt"]="Alemtuzumab"

    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Avonex
    df.loc[df["dmt"].str.contains("Avonex",case=False),"processed_dmt"]="Interferon-beta"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    df.loc[df["dmt"].str.contains("interferon",case=False)&df["dmt"].str.contains("1a",case=False),"processed_dmt"]="Interferon-beta"
    df.loc[df["dmt"].str.contains("extavia",case=False),"processed_dmt"]="Interferon-beta"
    df.loc[df["dmt"].str.contains("rebismart",case=False),"processed_dmt"]="Interferon-beta"
    df.loc[df["dmt"].str.contains("serobif",case=False),"processed_dmt"]="Interferon-beta"
    df.loc[df["dmt"].str.contains("betaseron",case=False),"processed_dmt"]="Interferon-beta"
    df.loc[df["dmt"].str.contains("interfer",case=False),"processed_dmt"]="Interferon-beta"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Betaferon
    df.loc[df["dmt"].str.contains("Betaferon",case=False),"processed_dmt"]="Interferon-beta"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    df.loc[df["dmt"].str.contains("interferon",case=False)&df["dmt"].str.contains("1b",case=False),"processed_dmt"]="Interferon-beta"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Cladribine
    df.loc[df["dmt"].str.contains("Cladribine",case=False),"processed_dmt"]="Cladribine"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    df.loc[df["dmt"].str.contains("mavenclad",case=False),"processed_dmt"]="Cladribine"
    df.loc[df["dmt"].str.contains("movectro",case=False),"processed_dmt"]="Cladribine"
    df.loc[df["dmt"].str.contains("cladribin",case=False),"processed_dmt"]="Cladribine"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Daclizumab
    df.loc[df["dmt"].str.contains("Daclizumab",case=False),"processed_dmt"]="Daclizumab"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    df.loc[df["dmt"].str.contains("Zinbryta",case=False),"processed_dmt"]="Daclizumab"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Tecfidera 
    df.loc[df["dmt"].str.contains("Tecfidera",case=False),"processed_dmt"]="Dimethyl-Fumarate"
    df.loc[df["dmt"].str.contains("dimethyl fumarate",case=False),"processed_dmt"]="Dimethyl-Fumarate"
    df.loc[df["dmt"].str.contains("dimetilfum",case=False),"processed_dmt"]="Dimethyl-Fumarate"
    df.loc[df["dmt"].str.contains("dimetil fum",case=False),"processed_dmt"]="Dimethyl-Fumarate"
    df.loc[df["dmt"].str.contains("dimethylfumer",case=False),"processed_dmt"]="Dimethyl-Fumarate"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    df.loc[df["dmt"].str.contains("Dymethyl",case=False)&df["dmt"].str.contains("fumarate",case=False),"processed_dmt"]="Dimethyl-Fumarate"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Fingolimod 
    df.loc[df["dmt"].str.contains("Fingolimod",case=False),"processed_dmt"]="Fingolimod"
    df.loc[df["dmt"].str.contains("finimod",case=False),"processed_dmt"]="Fingolimod"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    df.loc[df["dmt"].str.contains("Gilenya",case=False),"processed_dmt"]="Fingolimod"
    df.loc[df["dmt"].str.contains("fingoline",case=False),"processed_dmt"]="Fingolimod"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #HSCT 
    df.loc[df["dmt"].str.contains("HSCT",case=False),"processed_dmt"]="HSCT"
    #Natalizumab 
    df.loc[df["dmt"].str.contains("Natalizumab",case=False),"processed_dmt"]="Natalizumab"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    df.loc[df["dmt"].str.contains("Tysabri",case=False),"processed_dmt"]="Natalizumab"
    df.loc[df["dmt"].str.contains("Tysabry",case=False),"processed_dmt"]="Natalizumab"
    df.loc[df["dmt"].str.contains("Tyabri",case=False),"processed_dmt"]="Natalizumab"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Ocrelizumab 
    df.loc[df["dmt"].str.contains("Ocrelizumab",case=False),"processed_dmt"]="Ocrelizumab"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    df.loc[df["dmt"].str.contains("Ocrevus",case=False),"processed_dmt"]="Ocrelizumab"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Plegridy 
    df.loc[df["dmt"].str.contains("Plegridy",case=False),"processed_dmt"]="Interferon-beta"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    df.loc[df["dmt"].str.contains("peginterferon",case=False)&df["dmt"].str.contains("1a",case=False),"processed_dmt"]="Interferon-beta"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Rebif 
    df.loc[df["dmt"].str.contains("Rebif",case=False),"processed_dmt"]="Interferon-beta"
    df.loc[df["dmt"].str.contains("cinnovex",case=False),"processed_dmt"]="Interferon-beta"
    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Teriflunomide 
    df.loc[df["dmt"].str.contains("Teriflunomide",case=False),"processed_dmt"]="Teriflunomide"
    df.loc[df["dmt"].str.contains("aubagio",case=False),"processed_dmt"]="Teriflunomide"
    df.loc[df["dmt"].str.contains("teriflu",case=False),"processed_dmt"]="Teriflunomide"
    df.loc[df["dmt"].str.contains("triflunomide",case=False),"processed_dmt"]="Teriflunomide"

    #print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))
    #Mitoxandrone 
    df.loc[df["dmt"].str.contains("novantrone",case=False),"processed_dmt"]="Mitoxandrone"
    df.loc[df["dmt"].str.contains("mitoxandrone",case=False),"processed_dmt"]="Mitoxandrone"
    df.loc[df["dmt"].str.contains("mitoxantrone",case=False),"processed_dmt"]="Mitoxandrone"
    df.loc[df["dmt"].str.contains("mitozantrone",case=False),"processed_dmt"]="Mitoxandrone"
    df.loc[df["dmt"].str.contains("novantron",case=False),"processed_dmt"]="Mitoxandrone"

    #Rituximab 
    df.loc[df["dmt"].str.contains("mabthera",case=False),"processed_dmt"]="Rituximab"
    df.loc[df["dmt"].str.contains("rituximab",case=False),"processed_dmt"]="Rituximab"
    df.loc[df["dmt"].str.contains("rituxan",case=False),"processed_dmt"]="Rituximab"
    df.loc[df["dmt"].str.contains("mabhtera",case=False),"processed_dmt"]="Rituximab"
    df.loc[df["dmt"].str.contains("mabtera",case=False),"processed_dmt"]="Rituximab"
    df.loc[df["dmt"].str.contains("ritux",case=False),"processed_dmt"]="Rituximab"
    df.loc[df["dmt"].str.contains("rituksim",case=False),"processed_dmt"]="Rituximab"


    #GlucoCorticoid
    df.loc[df["dmt"].str.contains("solumedrol",case=False),"processed_dmt"]="Gluco-corticoid"
    df.loc[df["dmt"].str.contains("solumod",case=False),"processed_dmt"]="Gluco-corticoid"
    df.loc[df["dmt"].str.contains("solu-medrol",case=False),"processed_dmt"]="Gluco-corticoid"
    df.loc[df["dmt"].str.contains("methylprednisolon",case=False),"processed_dmt"]="Gluco-corticoid"
    df.loc[df["dmt"].str.contains("metilprednizolon",case=False),"processed_dmt"]="Gluco-corticoid"
    df.loc[df["dmt"].str.contains("decadron",case=False),"processed_dmt"]="Gluco-corticoid"
    df.loc[df["dmt"].str.contains("deltacortene",case=False),"processed_dmt"]="Gluco-corticoid"
    df.loc[df["dmt"].str.contains("medrol",case=False),"processed_dmt"]="Gluco-corticoid"
    df.loc[df["dmt"].str.contains("dexamethason",case=False),"processed_dmt"]="Gluco-corticoid"
    df.loc[df["dmt"].str.contains("prednisolon",case=False),"processed_dmt"]="Gluco-corticoid"


    #Azathioprine
    df.loc[df["dmt"].str.contains("imuran",case=False),"processed_dmt"]="Azathioprine"
    df.loc[df["dmt"].str.contains("azathioprine",case=False),"processed_dmt"]="Azathioprine"
    df.loc[df["dmt"].str.contains("azatioprina",case=False),"processed_dmt"]="Azathioprine"
    df.loc[df["dmt"].str.contains("imurel",case=False),"processed_dmt"]="Azathioprine"
    df.loc[df["dmt"].str.contains("ýmüran",case=False),"processed_dmt"]="Azathioprine"
    df.loc[df["dmt"].str.contains("imurek",case=False),"processed_dmt"]="Azathioprine"


    #cyclophosphamide
    df.loc[df["dmt"].str.contains("cyclophosphamide",case=False),"processed_dmt"]="Cyclophosphamide"
    df.loc[df["dmt"].str.contains("endoxan",case=False),"processed_dmt"]="Cyclophosphamide"
    df.loc[df["dmt"].str.contains("ciclofosfamide",case=False),"processed_dmt"]="Cyclophosphamide"
    df.loc[df["dmt"].str.contains("endoksan",case=False),"processed_dmt"]="Cyclophosphamide"
    df.loc[df["dmt"].str.contains("cytoxan",case=False),"processed_dmt"]="Cyclophosphamide"
    df.loc[df["dmt"].str.contains("genoxal",case=False),"processed_dmt"]="Cyclophosphamide"


    #methotrexate
    df.loc[df["dmt"].str.contains("methotrexate",case=False),"processed_dmt"]="Methotrexate"
    df.loc[df["dmt"].str.contains("methotrexat",case=False),"processed_dmt"]="Methotrexate"
    df.loc[df["dmt"].str.contains("metotrexate",case=False),"processed_dmt"]="Methotrexate"
    df.loc[df["dmt"].str.contains("methotrexaat",case=False),"processed_dmt"]="Methotrexate"
    df.loc[df["dmt"].str.contains("methorexate",case=False),"processed_dmt"]="Methotrexate"

    #Immuno-Globulin
    df.loc[df["dmt"].str.contains("immuunglobulin",case=False),"processed_dmt"]="Immuno-Globulin"
    df.loc[df["dmt"].str.contains("nanogam",case=False),"processed_dmt"]="Immuno-Globulin"
    df.loc[df["dmt"].str.contains("immunoglobul",case=False),"processed_dmt"]="Immuno-Globulin"
    df.loc[df["dmt"].str.contains("gammagard",case=False),"processed_dmt"]="Immuno-Globulin"
    df.loc[df["dmt"].str.contains("octagam",case=False),"processed_dmt"]="Immuno-Globulin"
    df.loc[df["dmt"].str.contains("flebogamma",case=False),"processed_dmt"]="Immuno-Globulin"


    print("Remaining number of different DMTs : {}".format(df["processed_dmt"].nunique()))

    dmt_kept = df.groupby("processed_dmt")["PATIENT_ID"].count().sort_values().reset_index()

    dmt_kept = dmt_kept[dmt_kept.PATIENT_ID>100]
    dmt_kept.drop(dmt_kept.loc[dmt_kept.processed_dmt.str.contains("clini")].index, inplace = True)
    dmt_kept.drop(dmt_kept.loc[dmt_kept.processed_dmt.str.contains("ivig")].index, inplace = True)
    dmt_kept.drop(dmt_kept.loc[dmt_kept.processed_dmt.str.contains("fampyra")].index, inplace = True)
    dmt_kept.drop(dmt_kept.loc[dmt_kept.processed_dmt.str.contains("biotin")].index, inplace = True)
    dmt_kept.drop(dmt_kept.loc[dmt_kept.processed_dmt.str.contains("ivmp")].index, inplace = True)
    dmt_kept.drop(dmt_kept.loc[dmt_kept.processed_dmt.str.contains("vitamin")].index, inplace = True)
    # remove immunoglobulin and gluco-corticoid
    dmt_kept.drop(dmt_kept.loc[dmt_kept.processed_dmt.str.contains("Immuno-Globulin")].index, inplace = True)
    dmt_kept.drop(dmt_kept.loc[dmt_kept.processed_dmt.str.contains("Gluco-corticoid")].index, inplace = True)

    print(f"Number of DMTs kept : {dmt_kept.processed_dmt.nunique()}")
    kept_dmts = np.array(list(dmt_kept.processed_dmt.unique()))
    np.save(DATA_DIR+"/MSBase2020/Cleaned/kept_dmts.npy",kept_dmts)

    df_final = df[["PATIENT_ID","START_DATE","END_DATE","processed_dmt"]].copy()
    df_final = df_final.loc[df_final.processed_dmt.isin(dmt_kept.processed_dmt.unique())]

    df_final_start = df_final[["PATIENT_ID","START_DATE","processed_dmt"]].copy().rename(columns = {"START_DATE":"date"})
    df_final_start["DMT_START"] = True
    df_final_end = df_final[["PATIENT_ID","END_DATE","processed_dmt"]].copy().rename(columns = {"END_DATE":"date"})
    df_final_end["DMT_END"] = True

    df_final = pd.concat((df_final_start, df_final_end))

    df_final["date"] = pd.to_datetime(df_final["date"], format="%d.%m.%Y", errors = "coerce")

    # Remove relapses before 1970.
    min_date=datetime(1970,1,1)
    today = datetime(2020,9,1)
    df_final = df_final.drop(df_final.loc[df_final["date"]<min_date].index).copy()
    df_final = df_final.drop(df_final.loc[df_final["date"]>today].index).copy()
    
    df_final = df_final.loc[~df_final.date.isna()].copy()

    return df_final
