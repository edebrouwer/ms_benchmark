library(CalibrationCurves)

setwd("G:/My Drive/Onderzoek/OMOP_longitudinal/omop-learn-master/my_version")
p <- read.csv('p.csv', header=FALSE)
y <- read.csv('y.csv', header=FALSE)

parr <- p[1:1000, 1]
yarr <- y[1:1000, 1]

# parr <- p[, 1]
# yarr <- y[, 1]

# vp = val.prob(parr, yarr)

vp2 = val.prob.ci.2(parr, yarr, logistic.cal=T, g=15,
                    connect.smooth=TRUE, legendloc=c(0.75, 0.1),
                    d1lab="progressed in disability", d0lab='stable',  
                    col.smooth = 'black', lty.smooth = 1, lwd.smooth = 2, 
                    col.log = 'green', lty.log = 2, lwd.log = 2,
                    lwd.ideal = 2.5,
                    line.bins=-0.2, ylim=c(-0.3, 1.0),
                    length.seg = 1.2, 
                    yaxp = c(0, 1, 5))
