# Machine-learning-based prediction of disability progression in multiple sclerosis

This repository contains the scripts for reproducing the experiments from "Machine-learning-based prediction of disability progression in multiple sclerosis: an observational, international, multi-center study"

It provides all the pre-processing routines to prepare the MSBase data in a machine-learning friendly format.

## Installation

For package management, we are using `poetry`. For information about usage and installation please see : https://python-poetry.org/.

Once you have poetry installed, you can install the package by simply running : 

```
poetry install
```

## Creating clean datasets and folds

You should first put the original (raw) files in the folder `data/EP/` or `data/MSBase2020/`.

Then in `ms_benchmark/data_preproc`, run 

```
poetry run python mep_clean.py
```
or
```
poetry run python msbase_clean.py
```

That will generate "cleaned" MEP or MSBase data `data/EP/Cleaned/` or `data/MSBase2020/Cleaned`

The above will automatically generate folds and save them. But you can also create folds manually : 

```
poetry run python folds_MEP.py
```
or
```
poetry run python folds_MSBase.py
```

The folds are then to be found in `data/EP/Cleaned/folds/` or `data/MSBase2020/Cleaned/folds/`

In order to speed up cleaning and generation of data slices, previous cleaned versions of the data are reloaded when running `_clean.py` and only the slice extraction is redone. If you want to force the recomputation of the cleaned dataset, use 

```
poetry run python mep_clean.py --overwrite
```
or
```
poetry run python msbase_clean.py --overwrite
```

For running the different models (static - dynamic and longitudinal), the datasets have to be created first. For this, run :

```
poetry run python generate_different_datasets.py
```

